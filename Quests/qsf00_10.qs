<Root>
{
	<Quest>
	{
		Desc	=	"Give 5 sharp teeth of Small Black Wolf to Billy Folks."
		GiveUp	=	"1"
		Id	=	"10"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Wolf hunt"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Small Black Wolves live in a pack. They are really vicious and only attack when humans are not around. When they think they have the upper hand, they will attack without a second thought. 





 But lately things changed... They have been attacking humans, and several people have been badly injured. I am getting scared.





 The patrols are trying to get rid of them by putting a bounty on their heads. If you kill them and bring 5 teeth as proof, Billy Folks will give you a nice reward."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20001"
					}
				}
				<Branch0>
				{
					Desc	=	"Reward makes a job worthwhile. I'll do it"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"10"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh, you want to try it? That is a great idea. It's difficult to find brave young people like you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sounds too dangerous..."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Well, I'm sure someone else will enjoy the reward. Goodbye."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Wolf hunt."
			Desc2	=	"Hmm? Bibia told you about the wolf hunt? You brought me the wolf teeth? One, two, three, four and five. Great, you got them all. Thank you for keeping our citizens safe."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20180"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"31"
					Exp	=	"93"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"10"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
