<Root>
{
	<Quest>
	{
		Desc	=	"Find Blanko Luliks at Blue Gorge's Aegis Fortess."
		GiveUp	=	"1"
		Id	=	"24"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Ruin's Pieces (to Blue Gorge)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I think this is pieces of the spirit for sure.  Long ago I met couple of old men hiding and studying the hermit camp. They showed it to me once before. 





 As soon as Alexia found out that this is the spirit pieces, he would immediately tell Aegis' Guard captain.  If someone is planning something, there is no way that strong forces such as the Aegis would not react. 



 I think we should let the Aegis be ready.  I think Alexia gave his writing to you when you went to the Blue Gorge.  This is my signature, take this to Aegis' guard captain."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20207"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"8"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"23;"
					}
				}
				<Branch0>
				{
					Desc	=	"Blue Gorge will be a long journey."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"3"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"20"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"21"
						}
						<YActNode2>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I know it will be a difficult journey, but you are the only one who can do this. Do this for Rog."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I still need to finish my business here."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"When you are strong enough, please come back and see me again."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Ruin's Pieces (to Blue Gorge)"
			Desc2	=	"How did you get here? What? How did a soldier like you obtained Alexia's letter? I must read to find out what's going on. This is Cain? Is this work of Cain? 





 I neglected this important guest.  I must look at this letter further. It must have been long journey coming here, I think you and I will be plenty busy."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"3"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"20"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"21"
				}
				<CondNode2>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20195"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"24"
					Exp	=	"96"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"163002"
					SItem1	=	"163003"
					SItem2	=	"164003"
					SItem3	=	"164004"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"20"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"21"
				}
			}
		}
	}
}
