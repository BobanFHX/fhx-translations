<Root>
{
	<Quest>
	{
		Desc	=	"You must hunt 15 Dust Orc Warriors and 15 Dust Orc Archers."
		GiveUp	=	"1"
		Id	=	"167"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Explore Dust Orc tribe."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I think you need to take this one? I am occupied with this wolf man or Lycanthrope character.  South of Dust field and Dust gorge you can find Dust Orc camps, there you can find out what they do with those stones. 





 I'll post some militia to you.  You must kill 15 Warriors and 15 Archers before you can get near the camp. Once you get rid of them I'll send my militant men to you. 





  Your mission is to kill those 15 Orc Warriors and 15 Orc Archers, and the rest will be taken care of by my men.  Do you understand what I am saying?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20198"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"25"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"166;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm.  SureI will help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then quickly be on your way."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"15"
							KillCnt1	=	"15"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"735"
							NPCIdx1	=	"730"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"This doesn't look like something I should do."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you saying that this has nothing to do with you? If you insist then I'll find someone else."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Explore Dust Orc tribe"
			Desc2	=	"Oh you have returned. Did my men have any success in the infiltration? It was their first time, however they are Dust Gorge's militant army so they should handle it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20198"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"90"
					Exp	=	"4310"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
