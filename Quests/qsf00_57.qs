<Root>
{
	<Quest>
	{
		Desc	=	"You need to receive qualification test from Black Door commander Kevin Dillon"
		GiveUp	=	"1"
		Id	=	"57"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Name called Knight"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"When first king of Rog, Puriken, wanted to stop the devil'd Den forces, he had to prepare attacking  and defense forces.  He found occupational soliders to help him.  This army was very effective and is known for their effectiveness in the battle, so unless there is a special event we will continue this tradition. 





   Lot of the Rog soliders chose their occupation and they continue their goal.  I think you are like them. The equipment that you carry tells me that you can't do any other job.  Knight, do you know that your name is a holy name? 





  Not too many people can obtain this name. it can be obtained by those who have bravery and leadership. From what I can tell you don't know anything about being a Knight. Do you really want to know about the road to being a knight? 





  Before you can become one, I must test you to see if you qualify for it.  Get the test from the Black Door guard Capt. Kevin Dillon.  He is one of the few that qualify as a knight."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20195"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"9"
					}
					<CondNode2>
					{
						Class	=	"2"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I want to learn"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"52"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you pass the test then I'll let you know how to become a true knight."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I never thought my self as a knight"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then you are not qualify as a knight"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Name called knight"
			Desc2	=	"What? A letter from Blanko? I wonder what this is all about? Hmm...I guess he requesting me to give you the test to be a knight."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"52"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20196"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"12"
					Exp	=	"111"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"52"
				}
			}
		}
	}
}
