<Root>
{
	<Quest>
	{
		Desc	=	"Go to Water Spirit Queen"
		GiveUp	=	"1"
		Id	=	"75"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Water Spirit Queen"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Wizard's strength comes from mana. The ones who use mana freely are the spirits. They are born from spirit and go back to them. If you live with wizards you'll eventually run into them. 



 I was lucky to talk with the Water Spirit King. She was a spirit who could control the water. A few days ago I saw her suffering in my dream. I felt that she was asking me for help. 



 I was wondering if you could go there and find out what's going on. She is at Tril fall which is located between Midland and Deadman's Land. Take this symbol and this will tell her that you are not an enemy."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20030"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"12"
					}
					<CondNode2>
					{
						Class	=	"32"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"74;"
					}
				}
				<Branch0>
				{
					Desc	=	"I will go and see her."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"69"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I hope that nothing will happen on your way."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't like spirits."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I don't know what to say..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Water Spirit Queen"
			Desc2	=	"Ah~ I feel friendliness from you. Oh really?  Mary Onet sent you. Yes...





 I sent her a message. It was a very important job. Welcome to the one who has pure spirit!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"69"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20226"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"14"
					Exp	=	"168"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"69"
				}
			}
		}
	}
}
