<Root>
{
	<Quest>
	{
		Desc	=	"Return with Iegees bow merchant Quinton's leather bag"
		GiveUp	=	"1"
		Id	=	"70"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Iegees Old Hunter"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"You look like you're pretty swift. You caught those wolves easily. Hee hee, I think I can make it in time. 





  You are returning to Iegees? I saw you headed there like crazy. I took your catches and separated them by their quality and tied them together. 





 Can you bring this leather bag to the weapon merchant named Paulis Quinton.  I'll give you 10% of what he gives me. If he knows that it was from me he'll give you the 10% anyway. Then I'll leave this up to you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20225"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						Class	=	"4"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"69;"
					}
				}
				<Branch0>
				{
					Desc	=	"I was on my way to Iegees anyway."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"64"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Don't wait - hurry up"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't really want to."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you don't then I have nothing to say.  You'll make some money from this."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Iegees Old Hunter"
			Desc2	=	"Aron sent you to me? You came to the right place. Although I gave him a short time to do it, he managed to make it in time. Anyway all ends well. 



 Thank you. Oh yeah... did he say for me to give you 10%? He is a special guy."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"64"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20009"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"13"
					Exp	=	"128"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"64"
				}
			}
		}
	}
}
