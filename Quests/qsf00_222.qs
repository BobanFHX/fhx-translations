<Root>
{
	<Quest>
	{
		Desc	=	"Töte das Stahlskelett und besorge den Manastein!"
		GiveUp	=	"1"
		Id	=	"222"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Manastein"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Um Dir die Wahrheit zu sagen, diese Lederhandschuhe sind nicht wichtig für mich. Was ich wirklich brauche ist ein Manastein des Stahlskelett. Ich brauche diese Handschuhe nur um den Manastein zu bekommen. 



Wenn Du mir hilfst diesen Manastein zu bekommen, gebe ich Dir einen sehr speziellen Gegenstand. Was meinst Du?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20279"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"221;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"37"
					}
				}
				<Branch0>
				{
					Desc	=	"Hört sich interessant an."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"188"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hahaha, Du wirst es nicht bedauern."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Nee suche jemand anderes."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Willst Du mir erzählen das Du alle Gegenstände der Welt hast...?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Manastein"
			Desc2	=	"Ist das der Manastein...? Hahaha, endlich! Jetzt kann ich einen Golem erschaffen!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20279"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"25"
					Exp	=	"7000"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169007"
					SItem1	=	"169008"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"188"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
