<Root>
{
	<Quest>
	{
		Desc	=	"You must gather 3 Shadow Orc¡¯s remote talismans."
		GiveUp	=	"1"
		Id	=	"97"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Remote Talisman"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm it¡¯s very strange.  I think this is some kind of talisman. I wonder what this is.  Ah sorry, what is it? Ah you mean this thing that I was looking at? Have you seen it before? 





 You seen this on Orc¡¯s heads? Really? Hmm then I think most of the problem has been solved.  I think this was the talisman that Alexis and I talked about. This talisman could control living beings  it¡¯s called a remote talisman. 





 This is made out of gold, but not too many could make this around here. Hmm then somebody is controlling Shadow Orcs around here? I guess this would be a rough guess.  This must have been very difficult, but somebody must be planning something in detail. 





 Still, this won't be enough proof. Hey, I hate to ask you, but can you get some of these from Shadow Orc's heads? Do you think you could do that?  I think 3 would be enough."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20187"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
				}
				<Branch0>
				{
					Desc	=	"Well, I'll try it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"3"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"90"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"For me, I think I need to study this further."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry, but I must go to the Black Bridge."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you don't want to, just be on your way."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Remote Talisman"
			Desc2	=	"You are sure these talismans were on Shadow Orc's heads? Hmm I guess it's true. 





 The blackish-red hairs that are on them are a sure sign that it belong to the Orcs.  Truly somebody is planning something. Who would do this? Who would do such a foolish thing?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20187"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"61"
					Exp	=	"1217"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"3"
					ItemIdx	=	"90"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
