<Root>
{
	<Quest>
	{
		Desc	=	"골짜기 그리즐리를 사냥하고 곰가죽 10개를 구해오시오."
		GiveUp	=	"1"
		Id	=	"329"
		NumOfQUNode	=	"2"
		Repeat	=	"1"
		Title	=	"목책 수리재료 추가조달[반복]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"일전에 구해다준 곰가죽 만으로는 목책수리를 다 끝마치지 못하네..

좀더 구해다 주게나.. 자..이왕 시작한 일이니 웃으면서 즐겁게 해주게.."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20295"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"46"
						MinVal	=	"36"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"292;"
					}
				}
				<Branch0>
				{
					Desc	=	"이왕 도와드린것이니. 이번까지만 더 도와드리죠.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"자네가 도와줄것을 알고있었네..고마우이.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"222"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"수비병도 없는 마당에 혼자서 뭘 할수있다고.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"너저분하게 부서진것을 두고 있을 수는 없지 않나..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"목책 수리재료 추가조달[반복]"
			Desc2	=	"고맙네..고마워.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20295"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"7000"
					Fame	=	"0"
					GP	=	"2"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"222"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
