<Root>
{
	<Quest>
	{
		Desc	=	"Catch vagabonds near the east side of the Ruin's of Rog and provide evidence."
		GiveUp	=	"1"
		Id	=	"20"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Pieces of Ruins (vagabond)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm! I am starting to notice you recently and I am starting to like you.  Not long ago, I've been hearing bad rumours around. 





  Town people have been hearing a rumour that an evil king that has been gone for 1000 years is coming back to life.  According to my subordinate, 





 he noticed that a small Mage camp started to appear.  Can you capture the guards and get some proof for me?  What do think? Can you handle it?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20194"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"3"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I will go and look for it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"17"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I think they might be stronger than you think. My guys are known to be tough, but they were easily defeated. If you can you might want to join with other adventurers."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry, I don't feel up to it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It your choice not to do it."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Pieces of Ruins (vagabond)"
			Desc2	=	"Oh This is a strange piece.  Hmmm I may have to ask the wizard to see what it is.  I can't tell what it is anyway.  This would be enough proof and will help me greatly."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20194"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"35"
					Exp	=	"195"
					Fame	=	"20"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"17"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
