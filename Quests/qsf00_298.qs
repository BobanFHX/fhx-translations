<Root>
{
	<Quest>
	{
		Desc	=	"긴꼬리일족 어미쥐를 처치하라."
		GiveUp	=	"1"
		Id	=	"298"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"근원파괴"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"용사님! 잠시만 기다려 주십시요. 마법통신병을 통해 로그 황실에서 보내온 급한 지시서가 도달했습니다. 그 내용은..

긴꼬리일족 박멸

다자모르 제1기사단 단장의 보고에 의하면, 비단쥐족을 토벌하던중 비단쥐족 어미쥐를 발견했으며, 어미쥐가 수많은 새끼쥐를 낳고 있음이 밝혀졌다. 즉, 긴꼬리쥐족의 완전 박멸을 위해서는 어미쥐를 처치해야만 가능할것이다. 누구를 막론하고 용기있는자는 긴꼬리일족 어미쥐의 처치에 모든 힘을 다하라.
                                   로그황제 : 이니스 로그

라고 공표되었습니다. 무리한 부탁일지는 모르지만, 용사님께서 이번일가지 마무리를 해주시면 이디오스 모든 주민은 물론이고 로그황실에서도 칭송할 것입니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20296"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"45"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"297;"
					}
				}
				<Branch0>
				{
					Desc	=	"맡은일은 끝까지 완수를 해야지요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"1"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1236"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"역시. 용사님은 책임감도 대단하시군요. 기대하겠습니다."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"어미쥐의 처치는 저에게 힘듭니다. 죄송하군요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"황제의 칙명을 모른척 하실것인가요? "
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"근원파괴"
			Desc2	=	"로그 황제를 대신해서 감사드립니다. "
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20296"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"13075"
					Fame	=	"0"
					GP	=	"65"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"102002"
					SItem1	=	"102003"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
