<Root>
{
	<Quest>
	{
		Desc	=	"Collect 8 small slime jellies"
		GiveUp	=	"1"
		Id	=	"73"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Calming Down Mana"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Welcome Mana follower. I think you are the first one from Iegees. I am the one in charge of United Wizards in Iegees.  Looks like you've been through tough training. The flow of your mana seems very peaceful. However, your mana is not yet stable - I guess you are working on that. You must level higher in order to stabilise the mana. 





  Since you're here do you want me to make a medicine that will calm your mana? Hold on... what?... I thought I had two of them but now I can't see it. Do I have to make it again? Here, you know that there is nothing for free. I want to make some more but I need more ingredients. At Mirror Lake small slimes will release slime jelly. I need this, if you can get 8 of them I can make it for you. Then I'll leave it to you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20030"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						Class	=	"32"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"8"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"67"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I'll ask you for this favour. I also need this medicine as well."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have enough."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You're bad."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Calming Down Mana"
			Desc2	=	"Thank you for your hard work.  Can you wait for a sec? All done - here it is."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20030"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"135"
					Exp	=	"738"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"8"
					ItemIdx	=	"67"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
