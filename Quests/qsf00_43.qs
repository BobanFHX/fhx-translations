<Root>
{
	<Quest>
	{
		Desc	=	"Deliever report to Aegis' Blanko Luliks"
		GiveUp	=	"1"
		Id	=	"43"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Curse of the lake"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Are you looking at the lake? How is it? Clear and clean huh? From long ago this mirror lake is known to be clean and clear.  3rd king of Rog Imperial castle named it Mirror Lake after he saw the sunset reflecting on the lake. 



 Lately the island in the middle started to dispense a dark fog. It is causing not only the fish but even the spirit to disappear. I think there's something there.  Sorry, let me introduce myself, I am Blue Raik, I am the guardian of this lake. The lake seems so strange lately. 



 I was about to go to that small island, if you don't mind, can you give this report to Aegis' Guard Captian Blanko Luliks?  I need to go to the island immediately."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20221"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"9"
					}
				}
				<Branch0>
				{
					Desc	=	"I was on my way there."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"40"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It looks like it might be bit cold to swim across, but here I go!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have another job to do."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...really? There's nothing you can do then."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Curse of the lake"
			Desc2	=	"Huh? You are? Guardian sent you? This report to me? Thank you for coming all the way here. 





   Hmm, I guess this is the report that I asked for.  I need to take a look at this."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"40"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20195"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"111"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"40"
				}
			}
		}
	}
}
