<Root>

{

	<Quest>

	{

		Desc	=	"Jage 20 Junge Manticore und 20 Kupfer-Gorgone"

		GiveUp	=	"1"

		Id	=	"275"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Die Kreaturen"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Erwecke den Sandgolem mit Hilfe des böse Welt Kristalls. Kontrolliere dann die Sandgolems und beschützte den Schutzwall. Dann werden sich Monster der bösen Welt am dort sammeln. 



Das stand in dem Brief. Sie nennen sich böse Weltmonster und wollen den Schutzwall zerstören. Am östlichen Schutzwall sammeln sich allerdings schon Monster. Halte sie auf während ich die Nachricht in den Palast bringe."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20288"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"274;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"57"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich nehme die Aufgabe an."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Sei vorsichtig, die Monster aus der bösen Welt sind sehr stark."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10000"

							KillCnt0	=	"20"

							KillCnt1	=	"20"

							KillCnt2	=	"0"

							KillCnt3	=	"0"

							NPCIdx0	=	"1475"

							NPCIdx1	=	"1485"

							NPCIdx2	=	"-1"

							NPCIdx3	=	"-1"

							SvrEventType	=	"0"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Das schaffe ich nicht."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Du bist nicht so stark wie ich dachte."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Die Kreaturen"

			Desc2	=	"Gut gemacht. Die Auditoren vom Rog Palast kommen jetzt."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20288"

				}

				<CondNode1>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"0"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"80"

					Exp	=	"23501"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

