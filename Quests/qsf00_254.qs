<Root>

{

	<Quest>

	{

		Desc	=	"Töte 20 Wilde Troll Eroberer."

		GiveUp	=	"1"

		Id	=	"254"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Kopfgeldjagd (3)"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Wie wäre es mit einer Jagd auf etwas, was sich sonst keiner traut und zwar die Wilden Troll Eroberer? Es geht das Gerücht um, dass es die Anführer derer sind, die Hadun angreifen. 

			

			Wenn du 20 von ihnen töten könntest, wäre die Sicherheit von Hadun wieder hergestellt."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20287"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"253;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"55"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich will es versuchen."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Das sind die stärksten Trolle, also sei vorsichtig."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10000"

							KillCnt0	=	"20"

							KillCnt1	=	"0"

							KillCnt2	=	"0"

							KillCnt3	=	"0"

							NPCIdx0	=	"1430"

							NPCIdx1	=	"-1"

							NPCIdx2	=	"-1"

							NPCIdx3	=	"-1"

							SvrEventType	=	"0"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Das ist zu schwer."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Echt? Na gut.. dann warte ich auf die königliche Armee."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Kopfgeldjagd (3)"

			Desc2	=	"Jetzt sind wir erst einmal sicher. Vielen Dank"

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20287"

				}

				<CondNode1>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"0"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"80"

					Exp	=	"23501"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"101040"

					SItem1	=	"101041"

					SItem2	=	"101042"

					SItem3	=	"101044"

				}

				<YActNode1>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

