<Root>
{
	<Quest>
	{
		Desc	=	"Find out about the Wolfman from Dr. Marcos."
		GiveUp	=	"1"
		Id	=	"162"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Wolfman's Secret"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Even if I tell you the way my cousin told it to me, I don't think you would believe me.  I think I need to hear what other people know as well. 



  There is only one expert in this area in our town. His name is Dr. Marcos and he is around the Midland's mining area, do you think you can go there for me?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20198"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"161;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes I am curious as well."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"139"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I am curious to know more as well."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ask someone else to do this."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Not interested?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Wolfmans Secret."
			Desc2	=	"Mmm, head of a wolf and body of a man? Then I think they are talking about Lycanthrope. 





  I didn't think that they even existed, I always thought they were just legends. I wonder where they came from? I just don't understand it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"139"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20251"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"767"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"139"
				}
			}
		}
	}
}
