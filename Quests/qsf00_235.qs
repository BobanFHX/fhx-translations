<Root>
{
	<Quest>
	{
		Desc	=	"Bringe den Bericht zum Offizier der Akedrav."
		GiveUp	=	"1"
		Id	=	"235"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Im Namen Gottes (1)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ich bin besorgt über das Wachstum der brutalen Stämme. Wir müssen das Rog Imperium darüber informieren und auf weitere Instruktionen warten, aber momentan haben wir keinen magischen Kommunikator.



Wenn Du diesen Bericht zum Offizier der Akedrav in Andora bringst, wird der Oberste des Ordens das Rog Imperium informieren. Dann kannst Du uns die Befehle des Rog Imperiums bringen. Würdest Du das für uns tun?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"45"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20281"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde es tun."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ich werde deine Taten nie vergessen."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"199"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich habe keine Zeit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Gut, ich schätze ich habe meine eigenen Soldaten für diese Aufgabe."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Im Namen Gottes (1)"
			Desc2	=	"Die Situation wird nicht besser.. warte bitte eine Weile. Ich werde versuchen mit Hilfe der magischen Kommunikation vom Rog Imperium Anweisungen zu erhalten."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"199"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20275"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"60"
					Exp	=	"2945"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"199"
				}
			}
		}
	}
}
