<Root>
{
	<Quest>
	{
		Desc	=	"Bring 5 damaged skin clothes of sunrise black croc."
		GiveUp	=	"1"
		Id	=	"158"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"God's knight 3 (Dashmod)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"However, I wanted to prepare a present to him. While I was resting here I saw a strange croc was torturing other crocs.  I know that, that croc is called vicious devil's den's killer croc for sure. 



  If you can bring 5 sunrise black crocs that have been injured I would appreciate it.  I need to you remind that there is something to do here.  Please I ask you this favor."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20265"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"23"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"157;"
					}
				}
				<Branch0>
				{
					Desc	=	"I will do it then."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"136"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Heh heh...then shall we go?"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It looks like it is not a job for me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Really? I feel bit disappointed."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"God's knight 3 (Dashmod)"
			Desc2	=	"What? Andre he asked me to give this to you? This means that he still love his disciple.  He will be here then.  To tell you the truth, my teacher is strong and kind.  Sometimes I can't understand his judgment.  Hew...because of it this happened. 



  Anyway I can only thank you for your troubles"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20247"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"84"
					Exp	=	"2840"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"136"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
