<Root>
{
	<Quest>
	{
		Desc	=	"Bring 20 Shadow Bat Skins to Tessa Welch."
		GiveUp	=	"1"
		Id	=	"190"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Bat Skin Jacket"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm. My family has been selling leather armor for over 150 years.  So we have our own family method of making it.  A jacket made with our secret method is famous among others, and to make that I need Shadow Bat skins.  But recently, they became fiercer and it's hard to get them. 





 If you have time can you get 20 of Shadow Bat Skins?  Then I will give you a Bat's Skin Jacket thats made with our family method.  You can not get it anywhere else."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20047"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"26"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm. Really?  I'll give it a try"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"20"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"163"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Yes, thank you"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I really don't need a leather jacket."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Even if this was just for me, would you still turn me down?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Bat Skin Jacket"
			Desc2	=	"Oh, you brought it.  Right, right, this is it, you have brought it the way it should be so that I could use it. Hee, hee, hee."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20047"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"92"
					Exp	=	"3571"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"161008"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"20"
					ItemIdx	=	"163"
				}
			}
		}
	}
}
