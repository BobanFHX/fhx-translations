<Root>
{
	<Quest>
	{
		Desc	=	"Bringe die Ketten zu Achim Markus in Hadun."
		GiveUp	=	"1"
		Id	=	"249"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Kopfgeld"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Vielen Dank. Ich würde gerne das Kopfgeld für die Goblin-Ketten abholen, aber ich kann nicht nach Hadun gehen, solange meine Beine verletzt sind. Könntest Du zu Achim Markus gehen und das Kopfgeld abholen und mir vielleicht etwas zu Essen mitbringen?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20286"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"248;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"49"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich mach es."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Danke. Ich weiß gar nicht, wie ich das wieder gut machen soll.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"204"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Frag jemand anderes."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du könntest mir schon genauso helfen, wie beim letzten Mal."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Kopfgeld"
			Desc2	=	"Hm.. Hm.. Was ist das denn.. Oh... das sind die Ketten der Felsengoblins."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"204"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20287"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"45"
					Exp	=	"3612"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"204"
				}
			}
		}
	}
}
