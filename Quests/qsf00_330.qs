<Root>
{
	<Quest>
	{
		Desc	=	"Defeat the Rebels in the Great Rebellion and collect 10 Rebel Talismans."
		GiveUp	=	"1"
		Id	=	"330"
		NumOfQUNode	=	"2"
		Repeat	=	"1"
		Title	=	"Rebel Talismans-Seal Exchange [Repeatable]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Rebels and riots are unacceptable, and all orders related to the suppression have been ordered by the prosecutor General Colin.


It is said that all evil forces in the Great Rebellion here carry their own signs of a rebel amulet. 


Defeat them and bring 10 Rebel Talismans.


Bring 10 Rebel Talismans and I'll exchange them for 1 Rebel Seal. "
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20311"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"50"
					}
				}
				<Branch0>
				{
					Desc	=	"I will join in the suppression of the rebels."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"The rebels have amulets, seals, and badges. I heard that if you collect 10 badges, there will be a corresponding reward."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"245"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It's a rebel ... it's hard.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"He said that those who collected 10 rebel badges have a corresponding reward."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Rebel Talismans-Seal Exchange [Repeatable]"
			Desc2	=	"You've had a lot of trouble with the rebels."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"4"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20311"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"11"
					Exp	=	"1200"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"245"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10002"
					ItemCnt	=	"1"
					ItemIdx	=	"246"
				}
				<YActNode3>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
