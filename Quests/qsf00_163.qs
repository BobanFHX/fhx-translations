<Root>
{
	<Quest>
	{
		Desc	=	"Capture Vagabond Lycanthrope and Vagabond Lycanthrope Warriors and obtain 15 Oily Skins."
		GiveUp	=	"1"
		Id	=	"163"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Proof of Artificiality."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm, it has been several hundred years since they were last seen. Our history book tells us that it has been 300 years since last ones appearance. Of course they were Kimera, do you know what Kimera is? Kimera is a monster that has been created artificially. It was created to increase fighting skills, but this doesn't mean that there weren't any real Wolfmen. 





  There were lots of them 1000 years ago.  Although they were not human, they used to help human.  Because of it The Devil's Den totally wiped them out.  If they are regular Wolfman then they are called Lycanthrope.  Unfortunately I think they were created to fight in a battle. 



   I think you should check it out.  Can you bring me some samples of Lycanthrope that I saw when I was walking near the Shadow Mine entrance.  I think 15 of their oily skins would be sufficient."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20251"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"162;"
					}
				}
				<Branch0>
				{
					Desc	=	"I will see what I can do for you.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"15"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"140"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then you want to try?"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am not interested."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Mmm, my tale is not enough."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Proof of Artificiality"
			Desc2	=	"I see. this is artificially created Kimera.  Do you see this oily skin?  You can't get this kind of skin from a traditional Wolfman. 



  Lycanthrope's skin is slippery to prevent sharp swords from penetrating it.  They were also carrying a weapons.Oh My.  I guess they were created to fight, curse the awful people responsible."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20251"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"87"
					Exp	=	"3072"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"140"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
