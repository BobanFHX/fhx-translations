<Root>
{
	<Quest>
	{
		Desc	=	"Capture injured ghouls, and take 5 of their spirit's stone seals from them."
		GiveUp	=	"1"
		Id	=	"133"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Spirit's stone seal"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ahhh! That was close! I almost died! There's no monsters like them. They are hard to handle, really! I barely got away! Hey, can you help me? When you go to the Black Stone cemetery there are injured ghouls. If you look at some of them, they have a special item they carry.





 I need those. It's normally called a spirit's stone seal. It can't really seal any spirit, but according to the legend, ghouls capture spirits into the stone. I however need them, heh heh...If you can get 5 of them for me, I'll give you something great in return. They are easy to find, all you have to do is wait around Black Stone cemetery."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20236"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"19"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, I'll take care of it for you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"115"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then...I'll leave it to you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I do not want to do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You are so cold."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Spirit's stone seal."
			Desc2	=	"Thank you, thank you! They are such a nuisance, now all the materials have been collected."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20236"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"74"
					Exp	=	"2019"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"115"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
