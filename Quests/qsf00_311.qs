<Root>
{
	<Quest>
	{
		Desc	=	"제시카 심슨의 탄원서를 로그황성 경비대장에게 전달하라."
		GiveUp	=	"1"
		Id	=	"311"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"슬픔의 탄원서"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"이렇게 아무런 관련도 없는 미천한 제 아들놈의 유해를 전해주셔서 너무 감사합니다. 하나 남은 아들까지 잃은 이 어미의 심정은 하늘이 무너지는듯 아프지만.. 복수는 죽어서도 할 수 없을것 같군요. 하지만, 이 사실을 로그황성 경비대장님에게 알려.. 어떻게든 이 울분을 풀 수 있는 방법을 찾고 싶습니다. 

용사님.. 힘드시겠지만, 부탁을 드려되 될런지요. 아들을 모두 잃어 원통한 사연을 담은 이 탄원서를 로그황성 경비대장님에게 전해 주십시요. 부탁드리겠습니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20300"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"55"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"310;"
					}
				}
				<Branch0>
				{
					Desc	=	"어서 슬픔을 딛고 일어서시길 바랍니다. 탄원서는 제가 전해드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"용사님의 은혜는 죽어서도 잊지 않겠습니다."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"232"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"직접 전해주는 편이 좋지 않을까요?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"흑흑... 우리 아들이 살아있다면 얼마나 좋을까.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"슬픔의 탄원서"
			Desc2	=	"이 탄원서는.. 으음...

이런 안타까운 사연이 있다니.. 아무리 외곽의 경비가 허술하기로.. 한 어머니의 아들 두명이 모두 죽임을 당하다니.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20304"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"232"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"12462"
					Fame	=	"0"
					GP	=	"31"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"232"
				}
			}
		}
	}
}
