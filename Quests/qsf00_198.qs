<Root>
{
	<Quest>
	{
		Desc	=	"You must collect 8 dead bodies of wolf."
		GiveUp	=	"1"
		Id	=	"198"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Stuffing of Wolf"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"How are you, I am a hunter who lives in Palmas and my name is Slula Isra.  I got a difficult order. 



  He wanted to stuff Silvery gorge wolf and rough gorge wolf.  I'm not really into it, but the amount they were willing to give was much too great to turn it down.  I needed the money and I couldn't turn it down, but I really don't like this job. 



  You can kill the wolf, but cannot harm their body.  In order to this, I have to catch them by hand, but it's way too difficult.  Can you help me catch 8 wolves?  It doesn't matter rather it can be Silvery Wolf or Rough Wolf, but it needs to be done without any harm to the bodies."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20263"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"28"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm.should I try?  This may be another challenge."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"8"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"168"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Your bravery is incomparable."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I do not want to do that."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm..you probably will not regret it."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Stuffing of Wolf"
			Desc2	=	"Let me see.  Wow all of them have no marks at all, your skill is great.  These are gonna be worth lots."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20263"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"98"
					Exp	=	"4117"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"8"
					ItemIdx	=	"168"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
