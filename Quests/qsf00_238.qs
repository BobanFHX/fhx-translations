<Root>
{
	<Quest>
	{
		Desc	=	"Töte die Morast Löwen und bringe ihre Klauen zu Chris Muffin."
		GiveUp	=	"1"
		Id	=	"238"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Merkwürdige Sammlung (1)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ich bin ein Sammler, der nur seltene Objekte sammelt. Manche Leuten sagen ich habe eine Schwäche, aber ich sammle gerne seltsame Objekte die andere nicht haben.



Hey, möchtest Du mir bei meiner Sammlung helfen? Ich hörte die Klauen des Morastlöwen sind sehr selten. Kannst Du mir 10 von ihnen bringen?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20284"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"46"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich besorge sie Dir."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"201"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Wenn Du sie siehst, weißt Du warum sie so selten und seltsam sind."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Auf keinen Fall."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hm! Du kennst den Spaß nicht, den man beim Sammeln von seltenen Objekten hat."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Merkwürdige Sammlung (1)"
			Desc2	=	"Oh, das sind die Klauen des Morast Löwen... sie unterscheiden sich völlig von den Klauen normaler Löwen."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20284"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"68"
					Exp	=	"13078"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"201"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
