<Root>
{
	<Quest>
	{
		Desc	=	"Obtain reagent bottle from Lino Shlion"
		GiveUp	=	"1"
		Id	=	"37"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Find a reagent bottle"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"As you know medicine has to be very clean.  The water you have in your animal skin pouch can be contaminated.  In order for water to be clean we need a reagent bottle. 





   It's going to be hard to obtain one from around here, and Iegees is far also.  When you go up north, you'll see a bridge, on the bridge you will find bottle seller.  Take this letter and then ask for a reagent bottle, and he will give it to you without a word.  Please hurry."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"7"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20216"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"36;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll look for one."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"36"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you are late, the water you brought can be contaminated."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I can't do it anymore"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What are you doing? Every second counts."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Find a reagent bottle"
			Desc2	=	"Huh? Who are you? Oh you are talking about Drake, this is his letter? Oh really?  Please wait a sec."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"36"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20217"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"83"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"36"
				}
			}
		}
	}
}
