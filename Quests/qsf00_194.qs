<Root>
{
	<Quest>
	{
		Desc	=	"Bring 5 Heads of Dust Hobgoblin"
		GiveUp	=	"1"
		Id	=	"194"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Spark of Uprising"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"You know what happened to those 5 who were killed? It has been a while since we were attacked like that.  Living in this town means a much more difficult life.  That's why in our elder meeting, we decided to wipe them out.  The Devil's Den might attack as well, and we can't afford to have both of them attack us at the same time. 





  Can you lead us in this battle?  It not a big deal, all we want is for our militant men's moral to rise.  I want you to get the heads of those vicious enemies. 





  All you have to do is get usthe heads of 5 Dust Hobgoblin Warriors, who live east of Dust Gorge."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20089"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"26"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"193;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Mmm...I understand."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"166"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I'll ask you this favor."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I think it'll be too much for me to lead."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then who should we use as our leader?  I can't think of a candidate."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Spark of Uprising"
			Desc2	=	"Oh, I was just about finished rounding up our militant army.  You came just in time and I thank you.  Thank you very much.  Because of you our moral is now at an all time high."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20089"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"92"
					Exp	=	"4642"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"166"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
