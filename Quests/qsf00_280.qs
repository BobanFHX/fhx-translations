<Root>
{
	<Quest>
	{
		Desc	=	"생명호수 식인 악어에게서 정인의 반지를 찾아오시오."
		GiveUp	=	"1"
		Id	=	"280"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"정인의 반지"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"정말 감사합니다.. 그런데, 혹시 악어를 처치하는동안 저의 정인의 반지를 못보셨나요? 분명 그놈들에게서 찾을 수 있을듯 한데. 

이제, 정인의 한은 풀어드렸지만 저는 어떻게 살아갈지 막막하네요. 혹시라도 정인이 까고 있으시던 약혼반지라도 찾을 수 있다면 죽을때까지 간직하면서 그분을 기억하고 힘을 얻을 수 있을것 같은데.. 

어렵겠죠? 제가 너무 제 생각만 하고 있는 거죠? 흑흑흑"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20289"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"29"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"279;"
					}
				}
				<Branch0>
				{
					Desc	=	"약혼반지를 찾아드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"214"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"아. 정말 그렇게 해주실 수있으세요? 정말 감사합니다."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"모래밭에서 바늘찾기지.. 그 반지를 어디서 찾겠어요?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"흑흑..역시나..힘든 일이겠군요."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"정인의 반지"
			Desc2	=	"우와..고마워요.. 어서 이리 줘봐요.
바로 이 반지에요. 10캐럿짜리 다이아몬드 반지.. 휴..하마터면 이 비싼 반지를 잃어버릴뻔 했네. 호호호."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20289"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"4636"
					Fame	=	"0"
					GP	=	"25"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"214"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
