<Root>
{
	<Quest>
	{
		Desc	=	"Bring 2 reagent bottles to Drake Walter."
		GiveUp	=	"1"
		Id	=	"38"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Finding Reagent bottle 2"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh you need a reagent bottle?  Hmm I know I have some unsold reagent bottles around here somewhere shuffle...shuffle...ah here it is. I just have 2.  If you take this you can make some medicine.  Your return trip is dangerous, so please be careful."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20217"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"7"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"37;"
					}
				}
				<Branch0>
				{
					Desc	=	"Ahh really? Thank you"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"2"
							ItemIdx	=	"37"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Sniff...sniff...I am glad to help out Mr. Drake whom I owe a debt."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ahh..in a little while."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You mean I have to bring this back to him?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Finding reagent bottle 2"
			Desc2	=	"You came just in time.  If you were little late, your effort could have been in vain, but you came just in time.  Wait a minute, let me go and make some medicine.  

(shuffle...scratch...scratch...boom)"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"2"
					ItemIdx	=	"37"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20216"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"83"
					Fame	=	"0"
					GP	=	"10"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"2"
					ItemIdx	=	"37"
				}
			}
		}
	}
}
