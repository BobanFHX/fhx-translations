<Root>
{
	<Quest>
	{
		Desc	=	"Capture Tiger forest Goblin scout and show Don Mackalint the 10 symbol of Goblin."
		GiveUp	=	"1"
		Id	=	"49"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Symbol of Goblin"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Here Aegis Fortress is well known for numberous Goblins.  They're not really special, but they can make great and intresting things.  Can you do me a favor? They carry a symbol made out of special metal to symbolize their tribe.  They have their own odors and with that they can tell which tribe they are in and what kind they are. 



 The metal is good for drilling. It is also good for making bows and there is no other metal is better than that special metal. I was wondering if you can get some of them for me.  I think about 10 would do.  You can get them near east side of Mirror Lake where tiger forest Goblin scouts roam."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20111"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"7"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll try it"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"45"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"While you are going out to hunt, I'll make sure to fire up the burner."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Can you find someone else?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh…don’t do it if you want to."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Symbol of Goblin"
			Desc2	=	"Hey you did a great job....great job."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20111"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"44"
					Exp	=	"464"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"45"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
