<Root>
{
	<Quest>
	{
		Desc	=	"Collect 10 Seals of the Rebels exchanged"
		GiveUp	=	"1"
		Id	=	"331"
		NumOfQUNode	=	"2"
		Repeat	=	"1"
		Title	=	"Rebel Seals-Badge Exchange [Repeatable]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Rebels and riots are unacceptable, and all orders related to the suppression have been ordered by the prosecutor General Colin.


It is said that all evil forces in the Great Rebellion here carry their own signs of a rebel amulet. 


10 Rebel Talismans can be exchanged for 1 Rebel Seal, and if you collect 10 Rebel Seals, we will exchange them for 1 Rebel Badge."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20311"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"50"
					}
				}
				<Branch0>
				{
					Desc	=	"I will join in the suppression of the rebels."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"The rebels have amulets, seals, and badges. I heard that if you collect 10 badges, there will be a corresponding reward."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"They are rebels... it will be hard."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Those who collect 10 rebel badges will have a corresponding reward."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Rebel Seals-Badge Exchange [Repeatable]"
			Desc2	=	"You've had a lot of trouble with the rebels."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20312"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"10"
					ItemIdx	=	"246"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"11"
					Exp	=	"1200"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"246"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10002"
					ItemCnt	=	"1"
					ItemIdx	=	"247"
				}
			}
		}
	}
}
