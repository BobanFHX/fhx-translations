<Root>
{
	<Quest>
	{
		Desc	=	"집시 홉고블린 파이터 10마리와 집시 홉고블린 대장 10마리를 처치하라."
		GiveUp	=	"1"
		Id	=	"307"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"수색작업"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"납치된 모리스씨를 찾아 비적단 소굴에 들어가 보려고 했지만.. 도저히 연약한 제 힘으로는 비적 소굴 근처에도 갈수가 없었어요. 촌장님이 써주신 편지를 읽어보니, 역시 용사님은 정의감이 남다르신것 같아요. 제발 불쌍한 저의 부탁들 거절하지 마세요. 이 황량한 거인의 영토에서 오직 용사님만이 저의 어려움을 도와주실 수 있어요. 

비적단 소굴을 조사하려면 먼저, 집시 홉고블린 파이터와 대장을 10마리씩 제거해주세요. 비적단 소굴로 잠입하기 위해서는 반드시 하급비적부터 처치해야해요."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20298"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"51"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"306;"
					}
				}
				<Branch0>
				{
					Desc	=	"도와드리죠.. 비적단의 횡포로 힘들어 하시다니..."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"감사합니다..용사님은 저의 영웅이세요."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"10"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1355"
							NPCIdx1	=	"1350"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"비적단은 저도 무섭거든요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"아.. 언제쯤이면 평화롭게 살 수 있을까."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"수색작업"
			Desc2	=	"이제 비적단의 조무래기들은 그 수가 많이 줄었군요.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20298"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"80"
					Exp	=	"19538"
					Fame	=	"0"
					GP	=	"48"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
