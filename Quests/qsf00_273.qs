<Root>

{

	<Quest>

	{

		Desc	=	"Beschaffe das Herz von einem Sandgolem"

		GiveUp	=	"1"

		Id	=	"273"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Seltsame Ereignisse"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Es gibt seltsame Ereignisse zwischen dem Land der Giganten und Andora in letzter Zeit und eines davon ist, dass die Sandgolems begannen zum Schilderwall im Nordosten zu wandern.

			

			Das würde bedeuten, dass der Teufel nach 1000 Jahren wiederauferstanden ist, nach dem er im Sandgolem gebannt wurde. 



Wir sollten den Sandgolem und den Schilderwall inspizieren. Bring mir das Herz des Sandgolems. Das Herz sollte aus einem Manastein bestehen."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"2"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20288"

					}

					<CondNode1>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"53"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich werde es versuchen"

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"10000"

							ItemCnt0	=	"1"

							ItemCnt1	=	"0"

							ItemCnt2	=	"0"

							ItemCnt3	=	"0"

							ItemIdx0	=	"209"

							ItemIdx1	=	"-1"

							ItemIdx2	=	"-1"

							ItemIdx3	=	"-1"

							SvrEventType	=	"1"

						}

						<YActNode1>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Vorsicht! Das sind gefährliche Golems."

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich habe keine Zeit"

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Zu schade, ich werde es wohl selbst versuchen müssen."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Seltsame Ereignisse"

			Desc2	=	"Hast Du es? Gute Arbeit!"

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"3"

				<CondNode0>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"1"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20288"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"54"

					Exp	=	"20809"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"1"

					ItemIdx	=	"209"

				}

				<YActNode2>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

