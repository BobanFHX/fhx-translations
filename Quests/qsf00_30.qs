<Root>
{
	<Quest>
	{
		Desc	=	"Get 3 of mutated spider's legs at north of Rog, and bring it to Loren Patosi."
		GiveUp	=	"1"
		Id	=	"30"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] West of Rog mutated spider."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hello, you must be interested in this great waterfall judging by you coming out here in the middle of nowhere.  When I look at Kings falls, I can imagine the great history of our heros.  If it wasn't for those great heros and their great effort we wouldn't be here today. 





 A 1000 years later, we still feel the Devil's Den's power.  I am a historian, but my specialty is about studying Devil's Den and their effects in history.  I am now studying the mutation monsters and Devil's Den. This isn't just simply a luck and it looks like you are an adventurer, would you mind doing me a favour? Do you know anything about the Relic in Belziev, south of Rog?  In the past it was there so that Belziev can camp there and attack Rog.  If you look, you can see the devil is still existing there and sometimes transformed monsters live there.  Especially mutated poison spiders. Their legs are a very desirable item for study. 

 I've heard that they moved their habitat to upper west of side. 



 If you have time, can you capture me 3 legs of mutated poisonous spider?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20207"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"5"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Really? Sounds interesting."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"3"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"27"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah thankyou. The one who knows how to look at waterfall is great man."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I'm too tired to go."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I hope you have great time looking at the waterfall."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"West of Rog mutated spider."
			Desc2	=	"Sniff...you brought it..but...sniff but....."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20207"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"392"
					Fame	=	"0"
					GP	=	"3"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"3"
					ItemIdx	=	"27"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
