<Root>
{
	<Quest>
	{
		Desc	=	"Give Paulis Quinton 4 Grizzly skins"
		GiveUp	=	"1"
		Id	=	"71"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Grizzly Hunt"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Aron sent you here for a simple request - I think when he saw you he probably saw you as a potential archer. Archers have two basic feats - fast speed and keen observation. 





 Archers need to lure their enemy not too fast or too slow, that what I'm talking about. Nobody is better than Aron. Don't you think he looked very skillful? He always sends all the potential archers to me. I am his teacher. What? You can't believe it? I look tired and old? Don't be so judgmental, I used to be the second best archer in Rog. 





  I need something; I am making a bow right now. I need a leather to cover the bow. Grizzly skin is the best material for this. When you travel a little bit of north of Iegees, you can find them living there.  Can you get me 4 Grizzly skins?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20009"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"11"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"70;"
					}
					<CondNode3>
					{
						Class	=	"4"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I always wanted to catch one of those."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"4"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"65"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If I make a good bow, I'll let you have one. Make sure to bring me high quality skins."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It sounds dangerous."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You don't want to? Then forget about it, I guess you don't want to be an archer."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Grizzly Hunt"
			Desc2	=	"Oh you have returned. You have high quality skins.  Thanks. You came just in time."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20009"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"35"
					Exp	=	"845"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"163004"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"4"
					ItemIdx	=	"65"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
