<Root>
{
	<Quest>
	{
		Desc	=	"You must give 5 heart of Gorge Orc warriors to Rosie Jones"
		GiveUp	=	"1"
		Id	=	"53"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Warrior's heart"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Well..I've seen your skills, now let me test your courage.  In the middle of the battle you have to become ruthless.  If you have any sympathy you can be killed. There are many people like that.  By now of course most of them are probably dead or disappeared. Gorge Orcs are to tell the truth, weak.  They are weak and don't have any organization. When you go into the horned tiger mountain you can find Commander Gorge Orc.  They are strong and tough. 





   I need you to bring their heart for me.  When they look at their own tribe's hearts, their morale will decrease.  You can look at it as killing 2 birds with 1 stone.  Now go and get 5 Oak warrior's hearts."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20182"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"8"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"52;"
					}
				}
				<Branch0>
				{
					Desc	=	"Whatever job you have, just leave it to me"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"47"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Remember that sometimes it's better to have others with you rather than be by yourself."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"They might be too much for me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...if you want to try it, just come and see me."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Warrior's heart"
			Desc2	=	"Wow...exactly five of them.  Great job, not too long ago my commander came and took over this operation.  I guess the work load had been reduced."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20182"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"47"
					Exp	=	"548"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"47"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
