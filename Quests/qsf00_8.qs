<Root>
{
	<Quest>
	{
		Desc	=	"Report back to Marcus Galwin"
		GiveUp	=	"1"
		Id	=	"8"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Reporting the incident"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Do you remember the incident at the bank? It was like a vanishing act, just like the disappearance of the Duke's horse. 





  Rog Officials are now sending knights to investigate it. This is no longer a guard's job. I need to report back to Marcus Galwin as soon as possible. Since I'm on guard duty, do you mind showing him this?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20194"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"7;"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, why not. "
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then please hurry and be on your way."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"8"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I'm not an errand boy."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What? You should finish what you started."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Reporting the incident"
			Desc2	=	"The Duke's horse disappeared too? They are sending knights? I knew I am not going crazy! Something very strange is going on... 





  Thank you for this. Let us hope Rog's knights will figure out what is happening."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"8"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20105"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"127"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"8"
				}
			}
		}
	}
}
