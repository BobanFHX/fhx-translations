<Root>
{
	<Quest>
	{
		Desc	=	"Give hiring Mercenary document to Bulbai Andrea."
		GiveUp	=	"1"
		Id	=	"9"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Hiring mercenary"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Because of all the troubles I haven't even had the time to properly introduce myself, I'm sorry. My name is Marcus Galwin and I am the bank manager here at Rog. Thank you for helping us out. Take this. 





  If you give this document to Bulbai Andrea, you will be able to hire a mercenary to fight by your side. You need to be level 5 though in order to hire a mercenary."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"2"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20105"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"8;"
					}
				}
				<Branch0>
				{
					Desc	=	"I was headed that way anyway."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"When you want to leave your items in the bank, just come and look for me. Thank you again and safe travels!"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"9"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am too busy right now..."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Mercenaries can be very helpful in battle. Anyway, if you want to leave your item in the bank, just come and look for me. Safe travels!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Hiring mercenary"
			Desc2	=	"What is this? Marcus gave this to you as a gift? This has already expired... I can't just let you go empty handed. Here, take this money and buy yourself a mercenary."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"9"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20080"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"127"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"9"
				}
			}
		}
	}
}
