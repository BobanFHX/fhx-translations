<Root>
{
	<Quest>
	{
		Desc	=	"Bring 10 sheepskin from Ahgol Goblin scouts."
		GiveUp	=	"1"
		Id	=	"148"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Sheepskin piece"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Those sheepskin which you brought, well I have been putting them together, and I found out that there are many missing pieces.  I believe that it used to be one whole piece. Now it's broken into 15 different pieces. 





   I think at least 10 more pieces are require, are you up to it?  The way I see it, Ahgol Goblin scouts has it?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20246"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"22"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"147;"
					}
				}
				<Branch0>
				{
					Desc	=	"Scouts?...it needs to be taken care of."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"127"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"this must be  another piece of evidence"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It's bit annoying."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I guess I can trust you.  Yet you disappoint me a bit."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Sheepskin piece"
			Desc2	=	"Hmm...is this rest of it?  I'll try to put them together.  You must have had hard time...wouldn't they bother you?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20246"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"82"
					Exp	=	"2619"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
