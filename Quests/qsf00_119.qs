<Root>
{
	<Quest>
	{
		Desc	=	"You have to capture Shadow Orc Berserker and bring daughter's relic to Albert Perry."
		GiveUp	=	"1"
		Id	=	"119"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Shadow Orc's Light Warrior"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ahh I guess humans tend to get greedy. After you have captured them, now I can only think about the Orcs that attacked my family.  That Orc was strong. 





 A few warriors tried to capture this Orc after hearing my story, but they all failed. I guess I shouldn't say any more knowing that it might kill you as well.  Still you may be able to capture him if you have someone to help you. 





 He is a Dark Orc Shaman that is vicious and violent.  That Orc Lord cannot even touch him. People call him Shadow Orc Shaman Tyrant.  He is the one who killed my daughter and my wife.  On the day he killed my daughter, he took my daughter's relic, and he hangs it around his neck like a trophy. 





 Whenever I see him from afar, my blood begins to boil. Sometimes I fainted because I was too angry. If you capture him and get my daughter's relic for me, I will forever be in debt to you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"19"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20238"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"118;"
					}
				}
				<Branch0>
				{
					Desc	=	"Is that the one who killed your family? Arrrg..."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"105"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Please get my revenge for me."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Phew this looks overwhelming even for me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I have nothing to say.  Thank you for everything."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Shadow Orc's Light Warrior."
			Desc2	=	"What, this is it! Thank you! I will never forget this. You finally took care of my debt. 





 I don't know what to say. Thank you, I'll never forget this as long as I live.  That's all I can say."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20238"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"85"
					Exp	=	"2019"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"157005"
					SItem1	=	"161006"
					SItem2	=	"162005"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"105"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
