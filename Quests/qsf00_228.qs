<Root>
{
	<Quest>
	{
		Desc	=	"Töte den Wasserratten-Späher und nimm den Bericht des Stammes an dich"
		GiveUp	=	"1"
		Id	=	"228"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Bericht des Stammes"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ich habe eine letzte Bitte an  Dich. Wir beauftragen Dich um Nachforschungen über den Wasserratten-Stamm anzustellen. Kannst Du den Wasserratten-Späher abfangen und seinen Bericht beschaffen?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20281"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"227;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"41"
					}
				}
				<Branch0>
				{
					Desc	=	"Das ist leicht."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"194"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Dankeschön. Es wäre eine große Hilfe, wenn Du den Bericht beschaffst."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Das ist unmöglich."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ich schätze ich habe Dich überschätzt."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Bericht des Stammes"
			Desc2	=	"Alles klar? Ich wusste Du würdest mich nicht enttäuschen. Gut gemacht!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20281"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"50"
					Exp	=	"9518"
					Fame	=	"0"
					GP	=	"4"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"194"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
