<Root>
{
	<Quest>
	{
		Desc	=	"I must alert everyone that Arketlav's knight has arrive from Rog yellow castle."
		GiveUp	=	"1"
		Id	=	"152"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"God's knights (Arketlav)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Phew~ difficult, it is difficult, this road is pretty far.  After the rest everyone will move at sunset and we will gather here.  Disperse...Hmm..no no...communication wizard can't go...Hmm...man...he's gone...they are only quick in this situation.  I'm going nuts...man...what...you never seen crazy knight before? 





  Are you picking a fight? How dare you challenge Arketlav? What...well...if you looked with envy then I feel better hee hee...Ah...anyway it turned out well.  These communication wizard disappeared. Man...anyway I would like you to give this report to Rog.  Can you give it to Rog...you never know, 





  later our knights can be a little help for you hee hee."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"22"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20248"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm...really. That's fine"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ha ha you are great guy."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"131"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"What do you think about me?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I think of you as a solider. Would I think of anything else?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"God's knights (Arketlav)"
			Desc2	=	"Hmm have they arrived?  You are late, truly late and where is communication wizard? 



   And you brought wrong person here.  Is he paying attention or what? He is really stupid.  Anyway good job."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"131"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20212"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"1202"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"131"
				}
			}
		}
	}
}
