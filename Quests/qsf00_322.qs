<Root>
{
	<Quest>
	{
		Desc	=	"이교도를 처단하고 이교도의 징표 20개를 키르크 프리에게 전달하라."
		GiveUp	=	"1"
		Id	=	"322"
		NumOfQUNode	=	"2"
		Repeat	=	"1"
		Title	=	"이교도 처단"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"천년전 악마 벨제붑이 이디오스에 나타났을때, 그를 추종하는 이교도들이 득세했었다는 사실은 용사님도 잘 알고 있을 것입니다. 그런데, 제가 여기 태양의 파편을 조사해본 결과, 바로 여기가 이교도들의 거점지라는 사실을 발견했습니다. 이교도들의 수가 늘어나 악마 소환의식이 진행된다면.. 벨제붑의 등장은 시간문제입니다.

제발.. 이디오스 대륙에 다시 한번 피바람이 불지 않도록.. 이교도의 제거에 모든 힘을 다 해 주시길 바랍니다. 이교도를 제거하고 이교도의 징표를 20개씩 모아오시면 그에 상응하는 보상을 해드리겠습니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20303"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"49"
					}
				}
				<Branch0>
				{
					Desc	=	"악마 벨제붑의 소환을 막아야죠. 도와 드리겠습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"시간이 촉박합니다. 악마 소환의식이 곧 시작될지도 모르니 서둘러 주십시요."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"20"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"240"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"악마라니.. 제가 대적할 만한 상대가 아니군요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"정녕.. 악마 벨제붑으로 인한 피바람이 다시 한번 불어 닥칠것인가...."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"이교도 처단"
			Desc2	=	"이교도의 처단은 계속 되어야 합니다. 다시 지원해주십시요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20303"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"3500"
					Fame	=	"0"
					GP	=	"5"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169040"
					SItem1	=	"169041"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"240"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
