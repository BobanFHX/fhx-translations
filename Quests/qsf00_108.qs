<Root>
{
	<Quest>
	{
		Desc	=	"Catch 5 Careless Ghouls, and bring their hoods."
		GiveUp	=	"1"
		Id	=	"108"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Old Grave Keeper"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Heheh... Without preamble, welcome...  Welcome I need people to help me right now.  There are so many Ghouls in this graveyard.





 No matter how many I¡¯ve caught of them, there are still many Ghouls here.  We should kill whoever disturbs the sacred slumber of the dead.  You - go now, and kill all the Careless Ghouls.  Bring 5 hoods as evidence.  Go ahead.   Show me what you are capable of."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20236"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"72;"
					}
					<CondNode3>
					{
						Class	=	"4"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"Don¡¯t yell at me.  I am leaving."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"98"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hurry... Hurry up... Do you want to be killed by me also?"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"What a temper he has"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What?  What did you say?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Old Grave Keeper"
			Desc2	=	"Heheh... Hurry up.  An archer should be faster than that.  You are so slow."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20236"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"64"
					Exp	=	"1359"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"98"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
