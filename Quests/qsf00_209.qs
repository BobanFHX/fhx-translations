<Root>
{
	<Quest>
	{
		Desc	=	"Bringe das Tagebuch zum Offizier von Akedrav!"
		GiveUp	=	"1"
		Id	=	"209"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der Verräter Andoras (4)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Edward liebte die Tochter des Grafen, doch der Graf erlaubte dieses Treffen nicht. Deswegen machte er diese schlimmen Sachen.



In dem historische Dokument stand was von einen Seelenhandel. Wir müssen Edward sofort finden, wenn er den Handel mit dem Teufel eingeht, wird die Dunkelheit die Welt für immer beherrschen. Bringe dem Offizier von Akedrav dieses Tagebuch und richte ihn aus das wir Edward finden und stoppen müssen."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20276"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"208;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"33"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich bringe es ihm."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Bringe es ihm, so schnell Du kannst."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"179"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich bin müde."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Tu es für Welt von Herrcot.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der Verräter Andora (4)"
			Desc2	=	"Mein Gott.. Edward ist der Verräter!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"179"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20275"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"42"
					Exp	=	"2207"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"179"
				}
			}
		}
	}
}
