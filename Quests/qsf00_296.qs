<Root>
{
	<Quest>
	{
		Desc	=	"긴꼬리 쥐 일족 장로와 암살자를 각각 10마리 제거하라."
		GiveUp	=	"1"
		Id	=	"296"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"밝혀지는 음모"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"가져오신 비단쥐족 정찰 보고서의 내용을 살펴보니.. 긴꼬리 쥐족이 비단쥐족을 오염시키고 있었으며.. 몇마리나 오염되어있는지 그리고 오염도는 어느정도인지를 긴꼬리일족 수뇌부에 보고하는 내용이었습니다.
음모의 동기를 알아내어 이디오스에 악영향을 끼칠것이라는 결론이 나와야만 로그황실에서도 위기의식때문에 지원을 해줄텐데.. 이정도 정황만으로는 상부에 보고하기 위한 자료로는 부족하네요. 정보를 얻기 위해서는 긴꼬리쥐 일족 수장을 잡아들여야 하는데.. 

수장을 호위하는 암살자와 장로를 먼저 처치해야겠습니다. 암살자 10마리 그리고 장로 10마리를 사냥해 주십시요."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20296"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"44"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"295;"
					}
				}
				<Branch0>
				{
					Desc	=	"힘든일이 되겠군요.. 다녀오겠습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"10"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1160"
							NPCIdx1	=	"1195"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"책임감있는 모습이 보기좋군요. 힘내세요. 그리고, 단단히 준비를 하고 가세요. 힘든 여정이 될것입니다."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"제 힘으로는 더이상 감당하기 힘듭니다. "
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"혼자서는 이디오스대륙에서 생활해 나가기 힘들텐데.. 어서 좋은 분들을 만나 힘을 얻도록 하세요."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"밝혀지는 음모"
			Desc2	=	"역시, 대단한 실력이시군요. 용사님 정도의 실력이라면.. 수장을 잡아들이는 일쯤은 쉬울듯 하군요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20296"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"10858"
					Fame	=	"0"
					GP	=	"54"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
