<Root>
{
	<Quest>
	{
		Desc	=	"You must deliever the medicine to the Alex who has the wounded hand."
		GiveUp	=	"1"
		Id	=	"39"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Deliver medicine"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Here, the medicine is done.  Hee hee, thanks to you my daughter will be cured.  Now you can deliver this medicine to your friend. 





  Don't want to brag, but you should know that my medicine is closest thing to perfect medicine.  Here you go. Anyhow, when would this Spring go back to it's old form? 





   This use to be so clear.  That black wizard he makes my blood boil."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20216"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"7"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"38;"
					}
				}
				<Branch0>
				{
					Desc	=	"I am glad that you finished your work."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"38"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ho ho thanks to you my daughter can live again. Please have a safe trip."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am tired. I don't want to move anymore."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"But your friend needs medicine!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Deliver medicine"
			Desc2	=	"Oh brave solider, you brought the medicine. Thank you, 



 thank you very much. Thank to you brave solider, now I can go back to the Aegis Fortrss.  Thank you very much."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"38"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20215"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"44"
					Exp	=	"83"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"158001"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"38"
				}
			}
		}
	}
}
