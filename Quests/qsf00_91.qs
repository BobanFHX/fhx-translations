<Root>
{
	<Quest>
	{
		Desc	=	"You need to bring oblivious well water to Donny Walburg who guards the Fog Graveyard."
		GiveUp	=	"1"
		Id	=	"91"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Deliver Well Water"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Heeh heeh, anyway, there is someone else who needs this water. If you go further up towards the Fog Graveyard youll meet Donny Walburg. 



 He needs this as well. I saved one bottle of oblivious well water along with this letter.  Give this to him.  Can you give it to him right away? Dont you open this during the trip. Right? Do not open it."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20234"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"13"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"90;"
					}
				}
				<Branch0>
				{
					Desc	=	"I¡¯ll help you, but I have a bad feeling about this."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"82"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you give this to him, he¡¯ll treat you well."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I feel that I can¡¯t trust you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I speak like that, I can¡¯t help myself. Would you reconsider?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Deliver Well Water"
			Desc2	=	"Which wizard gave this to me? Hmm this is strange. I don¡¯t know too many wizards. I wonder what¡¯s in here? 





 Oh no you who are you?... arrrg this¡¦this is¡¦assassi¡¦assassi¡¦assassin¡¦"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"82"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20192"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"193"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"82"
				}
			}
		}
	}
}
