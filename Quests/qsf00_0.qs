<Root>
{
	<Quest>
	{
		Desc	=	"Deliver abrasive bag to Leo Canton."
		GiveUp	=	"1"
		Id	=	"0"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Abrasive delivery"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Excuse me, are you here for the first time? You were looking around a lot, heh heh. 





 I work at the material store here in Rog. I know we only just met, but could you help me out? I need to make a delivery, but I stumbled and hurt my leg when I was hurrying to get here�. I am running out of time. 





 Is there any chance you could deliver this bag to Leo Canton, the blacksmith? He should be at his forge."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20201"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Of course I will help the wounded."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Thank you. Thank you very much. Now please hurry!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"You should do your own work."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah, yes. I guess I have no choice..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Abrasive delivery"
			Desc2	=	"Do you have business with me? If you don't, then please don't interrupt. 

	



 What? Ah, the abrasive delivery? I guess Roton couldn't make it in time so he asked someone else to finish the job. Anyway, thank you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20108"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"13"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"1"
				}
			}
		}
	}
}
