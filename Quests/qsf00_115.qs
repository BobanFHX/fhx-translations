<Root>
{
	<Quest>
	{
		Desc	=	"Bring 15 Deformed Skull Warrior's leg bones."
		GiveUp	=	"1"
		Id	=	"115"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Deformed Skull Warrior's Leg Bone"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm not that long ago I heard the news that undead started to appear, but now they are at the door.  They are not particularly strong, but they are tough to kill.  Some of them you have to chop into pieces in order for it to die. 





 It's hopeless - hey do you want to try and earn the reward that is being offered by James Maido who lives at the Deadman's Holy Ground?  You need to hunt 15 Deformed Skull Warrior's leg bones.  All you have to do is bring back 15 leg bones for evidence.  They usually come out from the left of Black Bridge.  Madio is at Black Bridge."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20187"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll try it then."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"15"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"103"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then good luck."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't want to, I'll quit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then quit."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Deformed Skull Warrior's Leg Bone"
			Desc2	=	"Mmm? Reward? Are you a hunter? Great, 





 make sure to work really hard for our country."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20187"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"64"
					Exp	=	"1767"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"15"
					ItemIdx	=	"103"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
