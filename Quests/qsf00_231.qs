<Root>
{
	<Quest>
	{
		Desc	=	"Bringe die Tasche mit dem Kristall zum Offizier des Dazamor Orden."
		GiveUp	=	"1"
		Id	=	"231"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der Kristall in der Tasche"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Während Du mir die richtige Tasche gebracht hast, habe ich die falsche Tasche untersucht -und ich fand einen sonderlichen Kristall darin. Ich hörte, dass alle Kristalle aus den stillen Sümpfen vom Obersten des Dazamor Ordens gesammelt werden.
			
			Würdest Du dem Offizier des Dazamors Orden diese Tasche bringen?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20282"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"230;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"42"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich bringe den Kristall."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Vielen Dank!"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"195"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Bringe du ihn doch."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh... Entschuldigung bitte die erneute Mühe..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der Kristall in der Tasche"
			Desc2	=	"Diese vielen Kristalle.. Wir müssen alle Kristalle finden, damit die Sümpfe wieder ein friedlicher Ort werden.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"195"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20281"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"25"
					Exp	=	"2467"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"195"
				}
			}
		}
	}
}
