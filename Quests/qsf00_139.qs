<Root>
{
	<Quest>
	{
		Desc	=	"Get rid of 8 army of zombies at oblivious sea cliff."
		GiveUp	=	"1"
		Id	=	"139"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Discarding tyrant zombie"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"What do I do...yo, you are that brave soldier. I was bit occupied and couldn't say thank you. Thank you very much. 



 However...I know this would be rude, but can I ask you for a favor? 



 When I checked my medicine basket, all my medicine plants had blue poison every where. 



 Of course this place is dead man's holy land, but since very few people visit here, a lot of plants grow here. 



I didn't see it when I was getting them, but now I think back there were lot of parts that are also infected. 



 According to the patrol leader, ever since undead monsters appear, medicine plants started to get infected. For right now, he leader said he can't do anything about the undead right now. 



 nobody will infected medicine plants from me... 

 I don't think I can make a living anymore...sniff, sniff 



 please have a pity on me and help me? Can you get rid of those undead at the  oblivious sea cliff?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20240"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"19"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"128;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do it for the lady."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah...you look like a prince."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"8"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"495"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Try it on your own."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What a heartless person."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Discarding tyrant zombies"
			Desc2	=	"Thanks to you I was able to go back home safely."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20240"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"74"
					Exp	=	"2019"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
