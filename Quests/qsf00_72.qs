<Root>
{
	<Quest>
	{
		Desc	=	"Show Quinton's symbol to Gilron Chopa."
		GiveUp	=	"1"
		Id	=	"72"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Cursed Forest Hunter"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I was waiting for you. You came just in time. There is a friend of mine who hunts at Cursed Forest. 





  It's a bit strange. He asked me to send a smart guy to him. He said that he had a new job. Do you want to go? Hmm, this is a symbol that I give to you. Why don't you go?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20009"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"12"
					}
					<CondNode2>
					{
						Class	=	"4"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"71;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll go and check it out."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"66"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"He looks a bit rough, but yet he is nice guy. (Nice enough to kill without mercy!)"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I still need to finish my business here."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Well if you look into it, it might be a good thing."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Cursed Forest Hunter"
			Desc2	=	"Cough, cough, you are the one sent by that old man. It's good that you came, I needed some help anyway."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"66"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20236"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"28"
					Exp	=	"168"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"66"
				}
			}
		}
	}
}
