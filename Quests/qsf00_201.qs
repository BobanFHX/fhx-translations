<Root>
{
	<Quest>
	{
		Desc	=	"Liefere die ausgestopfte Spinne zu Bruder Stefan Schmidt."
		GiveUp	=	"1"
		Id	=	"201"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Lieferung einer Trophäe"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey, ich frage Dich zum letzten Mal etwas. Ich muss diese ausgestopfte Spinne zu Graf Hudson bringen, aber ich muss hier bleiben.



Würdest Du sie bitte meinem Bruder bringen? Er wird sie dann zu Graf Hudson schicken. Kannst Du das für mich machen?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"29"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20264"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"200;"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich mache den Job."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Wenn Du Erfolg hast, gibt Dir Stefan eine Menge Geld."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"174"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Finde jemand anderes."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Alles klar. Ich kann Dich nicht zwingen."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Lieferung einer Trophäe"
			Desc2	=	"Es ist die ausgestopfte Spinne die Graf Hudson braucht. Dankeschön."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"174"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20263"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"55"
					Exp	=	"1254"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"174"
				}
			}
		}
	}
}
 