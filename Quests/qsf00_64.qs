<Root>
{
	<Quest>
	{
		Desc	=	"Deliver warrior's symbol to Scott Grims at Deadman's Land around the middle of three way road."
		GiveUp	=	"1"
		Id	=	"64"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Meeting With the Warrior With Pride"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Right, I like you. I think I can help you grow. We never openly helped anyone become more mature? Since we do not trust people we don't aggressively go after them. 





  We don't go after the weak to make them suffer. If we teach just anyone the strength their lives will lose meaning. I'll stop my lecture. 





   Go to the Deadman's Land and go to Scott Grims who is comforting the dead. If you show him this symbol of pride then he will show you the way out."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20006"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"11"
					}
					<CondNode2>
					{
						Class	=	"1"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"63;"
					}
				}
				<Branch0>
				{
					Desc	=	"I understand."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"58"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"But don't rush things.  Everything has an order in which they are to be done."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I will finish things here first."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then finish what you need to finish and go."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Meeting With the Warrior With Pride"
			Desc2	=	"What the... who are you? What? Are you a warrior?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"58"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20187"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"23"
					Exp	=	"147"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"58"
				}
			}
		}
	}
}
