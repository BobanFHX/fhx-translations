<Root>
{
	<Quest>
	{
		Desc	=	"Show warrior’s pride by killing a Lord of Skeletons."
		GiveUp	=	"1"
		Id	=	"105"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Warrior’s Pride"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"He is the one who received the acknowledgement of pride from Sir. Dorica.  The dignity coming out of him is enormous.  People call me Warrior’s Pride, but it is just an empty name.  The real warriors are invincible and undefeated predecessor warriors.  I am only a guard protecting a bridge of Rog.  However, I know what I should do as a warrior.





 That is a principle that leads what I am and what I will be.  I am going to test that you have your own pride.  Now the undead wander the ground of the dead, which has never happened before.  When everyone is in chaos, you should set your colleagues minds at rest by showing them your warrior’s power and make them aware of the dignity of warriors.  You should do that by killing a Lord of Skeletons at Silent Woods south of here."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"5"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20187"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
					<CondNode2>
					{
						Class	=	"1"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"64;"
					}
					<CondNode4>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Prove the pride of warriors."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"1"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"631"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Great attitude.  Show me your ability."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Why should I do such a task?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You don't know the consequences of this task.  You are so disgraceful."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Warrior’s Pride"
			Desc2	=	"Hahahah... It’s such a pleasure.  Even the undead can't beat someone with warrior’s pride.  It’s such a thrill that I haven't felt for a while.  Hahaha"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20187"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"1359"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150004"
					SItem1	=	"154003"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
