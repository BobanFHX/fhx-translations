<Root>
{
	<Quest>
	{
		Desc	=	"You must deliver Sheepskin to Joshua Caroline."
		GiveUp	=	"1"
		Id	=	"173"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Belzev Convoy 3 (Decoding a Puzzle)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"A fool like me could never figure out what these writing mean.  Shadow Mine town was made up by a couple of miners who gathered together. There is no one who is in charge. 





  Maybe you could find someone who will be able to read it there.  There is a priest named Dajahmaru who is over 100 years old.  I heard he was pretty well known in his time. 





Would you mind showing this sheepskin to him?  I think he might know the meaning of it."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20198"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"25"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"172;"
					}
				}
				<Branch0>
				{
					Desc	=	"I will look for him."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"150"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then good luck finding him."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have other messages to deliver."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...I see. Then be on your way."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Belzev Convoy 3 (Decoding a Puzzle)"
			Desc2	=	"Ho ho...who comes to see this lonely old man?  You look like a strapping young man. Well what can I do for you? 



  Oh...you want me to decode this? Well I'll give it a try.  Hmm...hmmm...this is The Devil's Den language.  It is an ancient tongue not spoken for a long time in Edios.  Ho ho...I may not know much I know little bit about Devil's Den's language."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"150"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20256"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"22"
					Exp	=	"866"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"150"
				}
			}
		}
	}
}
