<Root>
{
	<Quest>
	{
		Desc	=	"You must bring 2 Small Grizzly's Front Legs to Jessica Allen"
		GiveUp	=	"1"
		Id	=	"19"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Small Grizzly's Front Leg"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey, glad you came. Even if I seek fortune yet, don't you think that I look like I have at least a friend?  I really believe in the value of friendship. 

 That's not that important, but I have a friend who can make a great boots. Her name is Jessica Gilon. 



 She is supposed to make party boots for Ron the Duke.  The materials for the boots will require Small Grizzly's front legs.  They are located around North west of King's Fall.  They're pretty expensive materials.  I don't have any materials left. Not only that, but this creature is one of the more vicious bear out of all bears.  It's difficult to find.  If you take the front legs to Jessica Gilon then she might even give you a pair."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20065"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"5"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"18;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"2"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"16"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hurry, hurry and go."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't believe you at all"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm! I am the soldier's favorite vender."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Small Grizzly's Front Leg"
			Desc2	=	"Ah! What is this? You were asked this favor by Basken?  That guy has his uses.  Thank you very much.  I was searching for it everywhere.  I don't have too much to give you, but I can confidently make boots.  I hope you use these well."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20038"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"99"
					Exp	=	"302"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"164002"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"2"
					ItemIdx	=	"16"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
