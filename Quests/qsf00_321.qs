<Root>
{
	<Quest>
	{
		Desc	=	"태양의 파편안에서 키르크 프리를 찾아라."
		GiveUp	=	"1"
		Id	=	"321"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"태양속으로"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"혹시. 태양의 파편이라는 지역을 알고있나? 내 뒤쪽 깊은곳의 어둠의 문으로 들어가면, 태양의 파편이라는 이디오스 대륙과 동떨어진 별개의 시간의 세계가 존재한다네. 
그곳은 천년전 악마 벨제붑이 이교도들을 현혹하여 건설한 곳이기도 하지. 

그런데, 불과 몇 시간 전에 나의 동료 키르크가 저 어둠의 문으로 들어간 후 돌아올 생각을 하지 않고 있네.. 혹시, 자네도 어둠의 문을 통과하여 태양의 파편으로 가는 길이라면.. 키르크가 두고간 가방을 좀 전해주겠나? 가방안에 응급처치용 의료도구와 비상식량까지 다 두고 가버렸네.."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20302"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"49"
					}
				}
				<Branch0>
				{
					Desc	=	"음.. 흥미로운 곳이군요. 기꺼이 탐험을 해야죠.. "
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"키르크가 어둠의 문으로 들어간지 불과 두어 시간도 지나지 않았으니.. 어서 가보게"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"244"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"어둠의 문? 또다른 시간의 세계? 살아서 돌아올 수 있을까요? "
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"젊은이가 그렇게 모험 정신이 없어서야...."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"태양속으로"
			Desc2	=	"네.. 제가 키르크 프리입니다. 쉿!! 엎드리고 숨을 죽이세요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20303"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"244"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"17137"
					Fame	=	"0"
					GP	=	"21"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"244"
				}
			}
		}
	}
}
