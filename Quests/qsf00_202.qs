<Root>
{
	<Quest>
	{
		Desc	=	"Vernichte 15 weiße Löwen und 10 Löwen."
		GiveUp	=	"1"
		Id	=	"202"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Ausrottung der Löwen"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ah.. Ein großer Tempelherr ist hier stationiert um die Ursache des Bösen zu erforschen. Wir brauchen mehr Männer um das Böse zu bekämpfen.


So sind einige Abenteurer gekommen um zu helfen, aber der Tempelherr ist von einem wilden Löwen verletzt worden. Es hat den Auftrag gegeben die Löwen zu töten. Wir müssen nur dieses Areal beschützen und können hier nicht weg.


Kannst Du diese Löwen töten? Der Tempelherr will das 10 Löwen und 15 weiße Löwen getötet werden, damit er seine Rache hat."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20258"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"30"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich helfe Dir."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Vielen Dank."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"15"
							KillCnt1	=	"10"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"855"
							NPCIdx1	=	"845"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich bin zu beschäftigt."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Das ist in Ordnung. Dann muss ich es doch selber tun."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Ausrottung der Löwen"
			Desc2	=	"Oh, Du hast prächtige Fähigkeiten. Wie schnell Du bist! Jetzt kann ich beruhigter wache schieben."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20258"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"99"
					Exp	=	"4348"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
