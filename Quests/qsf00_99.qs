<Root>
{
	<Quest>
	{
		Desc	=	"Capture 8 Deformed Skulls and 6 Deformed Skull Warriors."
		GiveUp	=	"1"
		Id	=	"99"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Attack of the Undead"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Orcs are orcs, but I just heard urgent news.  West of here there is a plain, and I heard that skeletons are camping there. 



 I have been hearing about the undead appearing, but I never thought they were this close.  We have to get rid of them somehow - I think you should go and get rid of their front line for us. From what we can see, there are 8 Deformed skulls and 6 Deformed skull warriors.  Can I ask you this favour?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"98;"
					}
				}
				<Branch0>
				{
					Desc	=	"Of course."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"8"
							KillCnt1	=	"6"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"350"
							NPCIdx1	=	"355"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"All of a sudden we’re going to get real busy."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Don’t you think it’s a bit rude to ask someone you just met?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Do you think so? I guess it was too much to ask you, I apologise."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Attack of the Undead"
			Desc2	=	"Hmm great, you’ve done well. Was it tough? From the looks of it, you could probably handle it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"64"
					Exp	=	"1359"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
