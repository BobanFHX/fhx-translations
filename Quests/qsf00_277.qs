<Root>

{

	<Quest>

	{

		Desc	=	"Töte die Sturmspinne und bringe 10 Beinhaare zu Gaby Tatalsul "

		GiveUp	=	"1"

		Id	=	"277"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Falsche Zutat"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Es tut mir leid. Was Du mir gebracht hast, waren nicht die richtigen Beinhaare. Ich habe es so oft damit versucht, aber es geht damit nicht. 



Vielleicht waren es die Beine der Sturmspinne, nicht die der Felsenspinne. Entschuldige die Umstände. Bringe mir doch 10 Beine dieser Spinne."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20053"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"276;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"52"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich nehme diese Aufgabe an."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"10000"

							ItemCnt0	=	"10"

							ItemCnt1	=	"0"

							ItemCnt2	=	"0"

							ItemCnt3	=	"0"

							ItemIdx0	=	"212"

							ItemIdx1	=	"-1"

							ItemIdx2	=	"-1"

							ItemIdx3	=	"-1"

							SvrEventType	=	"1"

						}

						<YActNode1>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Das wird die beste Rüstung der ganzen Gegend."

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich mache das später."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Ah... Die Arbeit der neuesten Rüstung war beinahe geschafft.."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Falsche Zutat"

			Desc2	=	"Jetzt kann ich die richtige Rüstung machen."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"3"

				<CondNode0>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"1"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20053"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"47"

					Exp	=	"19538"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"169024"

					SItem1	=	"169025"

					SItem2	=	"169026"

					SItem3	=	"169027"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"10"

					ItemIdx	=	"212"

				}

				<YActNode2>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

