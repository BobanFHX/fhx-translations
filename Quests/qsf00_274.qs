<Root>

{

	<Quest>

	{

		Desc	=	"Finde Hinweise bei den Felsengoblins."

		GiveUp	=	"1"

		Id	=	"274"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Anweisungen vom Lebhaften"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Hm ... das ist kein Manastein, sondern ein Kristall der bösen Welt. Die Anhänger des Teufels benutzen den Kristall um die Sandgolems zu kontrollieren.

.

Wir müssen herausfinden wer die Anhänger sind, kürzlich verhielten sich die Anführer der Felsengoblins seltsam. Geh und finde heraus was mit den Felsengoblins geschehen ist. Bringe mir alle Hinweise die Du finden kannst."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20288"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"273;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"54"

					}

				}

				<Branch0>

				{

					Desc	=	"Mal sehen was ich finde."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"10000"

							ItemCnt0	=	"1"

							ItemCnt1	=	"0"

							ItemCnt2	=	"0"

							ItemCnt3	=	"0"

							ItemIdx0	=	"210"

							ItemIdx1	=	"-1"

							ItemIdx2	=	"-1"

							ItemIdx3	=	"-1"

							SvrEventType	=	"1"

						}

						<YActNode1>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Sei vorsichtig.. Felsengoblins wandern in Herden."

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich habe keine Zeit."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Ich sehe schon. Dann muss ich es allein machen."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Anweisungen vom Lebhaften"

			Desc2	=	"Was ... Du hast einen verdächtigen Brief gefunden. Ich werde einen prüfenden Blick darauf werfen."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"3"

				<CondNode0>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"1"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20288"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"68"

					Exp	=	"22130"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"1"

					ItemIdx	=	"210"

				}

				<YActNode2>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

