<Root>
{
	<Quest>
	{
		Desc	=	"Catch 10 Vicious Nieas"
		GiveUp	=	"1"
		Id	=	"85"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Spirit Queen’s Grief"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey vagabond nice to meet you. I am the Spirit Queen Elder Udinel who takes care of the water. Somehow you and my faith have been connected. Unfortunately this is not when we can spend a lot of time talking about it. 



 Belzev뭩 worshiper뭩 curse on the water spirit made it difficult for us. My influence saved them. I don뭪 know when, but those slimes started to appear and started to have a negative effect on us. 



 If it were regular slimes they wouldn뭪 do this much harm, but these slimes are different. Vagabond, if this is your faith why don뭪 you send my companions to the mana world? Please kill 10 Vicious Nieas to return them to their normal mana state."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20226"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"13"
					}
				}
				<Branch0>
				{
					Desc	=	"I’ll do what I can."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"305"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Mana’s strength will protect you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry this is my limit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Maybe this isn’t your faith."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Spirit Queen’s Grief"
			Desc2	=	"Ah I can feel their pain. However, thank you.  I am glad you used your strength to save us. 





  Is this the power of faith?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20226"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"1085"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
