<Root>
{
	<Quest>
	{
		Desc	=	"Bringe das entschlüsselte Dokument dem Offizier von Akedrav."
		GiveUp	=	"1"
		Id	=	"205"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Seelenhandel (3)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Dieses Dokument enthält einen Seelenhandel mit dem Teufel. Es geht darum das jemand seine Seele verkauft und der Teufel das Böse wiederauferstehen lassen will. Es sieht so aus als wenn das Böse Belziev ist.


Es ist ein echter Hinweis darauf das jemand seine Seele für das Böse verkaufen will. Gib dieses Dokument dem Offizier von Akedrav um den verrückten Mann zu finden. Beeil Dich."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20233"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"204;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"30"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich mache es."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Super vielen Dank."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"176"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich bin zu müde."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Was? Bist Du verrückt? Das sollte so schnell wie möglich zum Offizier gebracht werden"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Seelenhandel (3)"
			Desc2	=	"Oh nein, wird der Teufel bald wieder auferstehen? Es gibt viele Hinweise darauf.. aber.. durch einen Seelenhandel? Es ist schrecklich."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"176"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20275"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"25"
					Exp	=	"1235"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"101035"
					SItem1	=	"101036"
					SItem2	=	"101037"
					SItem3	=	"101039"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"176"
				}
			}
		}
	}
}
