<Root>
{
	<Quest>
	{
		Desc	=	"You should kill the Lord of Skeletons and bring his skull."
		GiveUp	=	"1"
		Id	=	"107"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Fearful Devil"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"To tell the truth, there is a reason why I was injured. While I was training and wandering around Deadman¡¯s Holy Land, I saw a skeleton that I had never seen before. 



 According to ancient history, when they go through the change it¡¯s an indication that a devil is about to be reborn. When I was investigating further, I found out that they were moving in order.  I used a hide spell to investigate further, then I seen something great. 



 There were Deformed Skull Lords gathered there and they were in the middle of a revive spell.  Then a few minutes later a scary Skull Lord was reborn!?! 



 If it were just Deformed Skull Lords, I would have taken care of them, but it was too much when this Skull Lord appeared. I am no match against them. I wasnt afraid to lose my life, but I needed to tell someone about this important information, so I barely escaped with my life. We need to beat them no matter what. 



 For you yourself it may be too much to handle, but if you bring those who have similar skills, then maybe you can beat them. Please defeat that Skull Lord. Please bring me back the head, so that I can ask for help at another shrine."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"5"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20233"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"16"
					}
					<CondNode2>
					{
						Class	=	"16"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
					<CondNode4>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"106;"
					}
				}
				<Branch0>
				{
					Desc	=	"I¡¯ll do my best to defeat them."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"97"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hew.. I¡¯ll leave it to you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don¡¯t have a friend."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You should always make friends."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Fearful Devil"
			Desc2	=	"Mm Such a large skull.  As soon as we bring this to Rog, Dajamaroo and Arketlev will start working seriously.  You are a proud paladin,  why would it be necessary to describe more?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20233"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"1359"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150006"
					SItem1	=	"153002"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
