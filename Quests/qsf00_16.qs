<Root>
{
	<Quest>
	{
		Desc	=	"Hunt 10 Black Wolf and 1 Black Wolf Pack Leader near the Belziev ruins, northwest of Rog."
		GiveUp	=	"1"
		Id	=	"16"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Wolf pack"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Are you the one that people are talking about? I have a favour to ask. Have you ever seen a Black Wolf? They roam in small packs and have sharp teeth that glimmer in the night. They live northwest of Rog, near Belziev's ruins. I often heard they attack unwary patrols and lumber jacks. I learned that the hard way...



 It happened a couple of days ago. As I was getting lumber for my stove, the Black Wolves sneaked up on me and attacked. I managed to get away, but I became crippled for life. Now I want revenge. 



  I want you to hunt them down and kill them one by one. Please. Make sure you take out their pack leader too. I will watch carefully from here. I need to see their dead bodies..."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20205"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"4"
					}
				}
				<Branch0>
				{
					Desc	=	"I will get your revenge."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"1"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"75"
							NPCIdx1	=	"80"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Make them suffer..."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Uhm, no thanks..."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Guess I will have to do it myself then..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Wolf pack."
			Desc2	=	"Heh heh heh! Their debt has finally been paid... Now I can hang their heads above my bed! "
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20205"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"94"
					Exp	=	"270"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150001"
					SItem1	=	"153000"
					SItem2	=	"154001"
					SItem3	=	"156001"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
