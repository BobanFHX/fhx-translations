<Root>
{
	<Quest>
	{
		Desc	=	"Töte den alten Hobgoblin-Kämpfer um das geheime Gesprächsdokument zu bekommen!"
		GiveUp	=	"1"
		Id	=	"206"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der Verräter Andoras (1)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Weißt Du wer ich bin? Ich war Andoras Tempelherr.. nun ist der Tempel zerstört, es war der beste Tempel. Die Ursache dafür war der Krieg mit Hobgoblins und anderen dunklen Banden.



Fakt ist, sie haben uns nicht alle vernichtet, aber es gibt Verräter in unserem Reihen. Sie haben eine Vereinbarung mit dunklen Banden, die das historische Dokument wollen. Die müssen ein Dokument mit einer Absprache haben. Wenn wir es finden, wissen wir wer der Verräter ist.



Allerdings bin ich während des Krieges verletzt worden. Wenn Du den alten Hobgoblin-Kämpfer findest und ihn töten kannst, dann könntest du vielleicht auch das Dokument des Verrats finden. Wir wären Dir sehr dankbar."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20276"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"31"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde ihn töten"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"177"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ich bin sehr erfreut."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Das geht mich nichts an."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du hast kein Mitgefühl..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der Verräter Andoras (1)"
			Desc2	=	"Jetzt können wir raus finden wer der Verräter ist! Vielen Dank!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20276"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"5"
					Exp	=	"4936"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"177"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
