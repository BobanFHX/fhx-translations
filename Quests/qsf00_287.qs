<Root>
{
	<Quest>
	{
		Desc	=	"팔마스 탈주마법사에게서 팔마스 수정구를 구해 오시오."
		GiveUp	=	"1"
		Id	=	"287"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"민병대 서임"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"우선, 민병대에 가입을 위해서는 여기 민병대 신청서에 서명을 하게나, 아, 물론 우리 민병대는 마물들과 탈주병들을 처리하고 있긴 하지만 임무자체는 그리 위험한 일은 절대로 없네. 그러나, 자기 몸도 지키지 못할 허약한 몸과 정신을 가진자들이 남을 돕는다는건 말이 안되기때문에, 지금부터 간단한 자격검증을 위해 가장 쉬운일을 하나 해줘야겠네.

저쪽 다리건너 언덕위에서 맨틀을 뒤집어 쓴 팔마스 탈주병 놈들을 몇놈 보았는데. 예전에 팔마스 마법기사단에 있었던 모양이야. 내 기억이 맞다면 저 마법사 나부랭이들이 팔마스 수정구를 관리하던 놈들인데, 로그성과 교신을 하기 위해서는 반드시 필요한 물품이라네. 유리구슬 따위를 관리하던 마법사들은 아주 쉬운 상대들이지. 안그런가? 하하하. 자네같은 용기와 노련미를 가진 젊은이들에게는 나뭇가지 하나 꺾어 버리는것과도 같은 일일테지.. 자. 어서 서두르게.

팔마스의 재건을 위해서는 로그성과의 교신이 절실히 필요하네. 팔마스 탈주마법사에게서 팔마스 수정구를 가져 오게나!!"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20293"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"34"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"286;"
					}
				}
				<Branch0>
				{
					Desc	=	"나뭇가지 몇개 꺾어 버리고 수정구를 구해오면 되는것인가요? 쉽군요. 하하"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"219"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"역시, 내가 사람보는 눈 하나는 있단 말이지..하하하"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"어이.. 나뭇가지가 아니자나, 팔마스 마법기사단의 마법사들이라고, 그들은 마법사 중에서도 2서클 급이란말이야.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"두려움에 벌벌떠는 비린내를 풍기는 애들은 필요없네. 방해되니 저리가봐."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"민병대 서임"
			Desc2	=	"벌써 왔는가.. 역시 팔마스 민병대에는 자네같은 인재가 필요하네. 어서 여기에 서명을 하시게나.. 하하하"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20293"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"69"
					Exp	=	"5373"
					Fame	=	"0"
					GP	=	"20"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"219"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
