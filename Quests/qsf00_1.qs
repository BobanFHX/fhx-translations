<Root>
{
	<Quest>
	{
		Desc	=	"Give leftover money to Roton Pleast."
		GiveUp	=	"1"
		Id	=	"1"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Leftover money delivery"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh, almost forgot. Here is the money that I owe Roton. I am sorry, but can you send him a message for me? Tell him that if he is late again, I will not do business with him again.  "
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20108"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"0;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll deliver it to him."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I'll leave it to you to finish the job."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"2"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I need to take care of other business."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I understand, you're busy. I'll look for someone else."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Leftover money delivery"
			Desc2	=	"I will make sure not te be late next time... Thank you for helping me out. You will be blessed."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"2"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20201"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"6"
					Exp	=	"13"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"2"
				}
			}
		}
	}
}
