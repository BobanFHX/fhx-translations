<Root>
{
	<Quest>
	{
		Desc	=	"Give contaminated skins to Harold Beuron."
		GiveUp	=	"1"
		Id	=	"45"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Harold the Wizard"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"From this fortress and head northwest road you will meet Iegees’ wizard Harold Beuron.  Go there and give him this sample and find out if there’s any problem.  I have other things to take care of. 





   Then I leave you with this favor.  As long as we have these evidence, you should move quickly. I need you to move quickly, then good luck on your quest."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20195"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"9"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"44;"
					}
				}
				<Branch0>
				{
					Desc	=	"Harold right, I got it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"42"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Time is running out, is everything going according to what Lauren predicted?"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I would like to quit now"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You are not thinking, think about what is good for this kingdom."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Harold the Wizard"
			Desc2	=	"Huh? Who are you? Hmm…this is? Little strange for croc skin.  Let me see them. 





   Due to the weather mirror lake crocs have one of the finest quality around.  These ones look like it was made for someone. 





   I see, this is Kiene’s curse. Somebody is putting curse on it for sure.  Who could of done this?  First I need to know the source of the problem.  This curse is difficult to get rid of due to the fact that one who put on the curse died."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"42"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20222"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"111"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"42"
				}
			}
		}
	}
}
