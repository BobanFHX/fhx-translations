<Root>
{
	<Quest>
	{
		Desc	=	"Bring notification report from bank to Rog's Security Commander, Gary Gomon."
		GiveUp	=	"1"
		Id	=	"6"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Missing account book"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh man, this is bad... I am sure I left the account book in the safe, but now it's gone! Am I going crazy? The key is here... The safe is here... But the account book is missing! 



 I am too afraid to face the commander. Please, could you tell him about the incident? This is terrible news. What will happen if the customer's items get mixed up..."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20105"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"5;"
					}
				}
				<Branch0>
				{
					Desc	=	"Really? I'll hurry."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Thank you very much. I need to list all the missing items in the account book, I need to take care of bank business, I need to..."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"7"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"This is your problem, not mine."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Man.. Oh man. What about me? How do I solve this mess?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Missing account book"
			Desc2	=	"The account book is missing? Is this true? By the gods, strange things are happening lately... 





  Only yesterday the Duke's carriage horse, Rodin, disappeared. Very strange... I think something big is going to happen. 



 Anyway, thank you for the message."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"7"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20194"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"29"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"7"
				}
			}
		}
	}
}
