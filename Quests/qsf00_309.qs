<Root>
{
	<Quest>
	{
		Desc	=	"집시 홉고블린 워로드를 처치하고 단서를 획득하라."
		GiveUp	=	"1"
		Id	=	"309"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"마지막 수색"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"용사님. 이제 마지막이에요.

조금만 더 힘내주세요. 파악한 정보에 의하면 비적단 소굴의 최종 우두머리는 집시 홉고블린 워로드 였어요. 분명, 이놈을 처치하면 모리스씨의 생존을 알 수 있는 단서를 찾을 수 있을거에요."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20298"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"55"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"308;"
					}
				}
				<Branch0>
				{
					Desc	=	"단서라.. 어쨌든 워로드를 처치하다보면 실마리를 풀 수 있겠죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"231"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"용사님.. 너무 늠름하세요."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"끝도 없는 전투에 너무 지쳤네요. 이제 그만...."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"이번이 마지막인데..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"마지막 수색"
			Desc2	=	"정녕 이 해골유해가 모리스씨란 말인가요.. 너무 슬프군요. 내심 무사하길 바랬지만.. 저의 작은 바램은 이루어 질 수 없는 것인가요..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20298"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"24924"
					Fame	=	"0"
					GP	=	"62"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"231"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
