<Root>
{
	<Quest>
	{
		Desc	=	"You must deliever Kogai's Note to Duston Canton."
		GiveUp	=	"1"
		Id	=	"171"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Belzev Convoy (Kogai's Note)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Sniff.sorry, would you like to hear my story? I am  Kogai a militant soldier from Dust gorge.  Sniff..I was sent by my militant commander to check out the Lycanthrope sightings. 





  I found out that they move about in a certain pattern.  While I was following them, I was discovered and attacked. 





  My wounds look pretty bad, and I don't think I can make it to Shadow Town.  Cough..this is a journal that I kept, can you give this note to Duston Canton my commander?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20254"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"25"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes I'll make sure it gets to him."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"148"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I'll thank you for this favor."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry. I am also wounded and need rest."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"but....ughhh."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Belzev Convoy (Kogai's Note)"
			Desc2	=	"Kogai is dead? Really? He is one of the swiftest men in this gorge, and I can't believe he has been killed. I can't believe it.  I just can't believe it!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"148"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20198"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"22"
					Exp	=	"866"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"27009"
				}
			}
		}
	}
}
