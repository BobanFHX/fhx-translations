<Root>
{
	<Quest>
	{
		Desc	=	"Take this order from Philanchea Dinos and you must return."
		GiveUp	=	"1"
		Id	=	"153"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"god's knight 2 (Arketlav)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Aww...sloppy guy, he should of left communication wizard and then disperse them.  That idiot is now commanding all the knights...what would come of it? I am afraid that you've been through a lot. 



   This is an order to Arketlav knights to him.  Take this and give this to him."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20212"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"23"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"152;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes sir...I will obey your order."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then please do a good job for me."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"132"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"At Rog I have business to take care of."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then come back afterwards."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"god's knight2 (Arketlav)"
			Desc2	=	"Ohhhh...you have come? Will he scold me? Heh Heh I am like that. Huh? This is an order? Ah...slowly...a war? This is a war, war!!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20248"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"132"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"1202"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"132"
				}
			}
		}
	}
}
