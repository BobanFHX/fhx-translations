<Root>
{
	<Quest>
	{
		Desc	=	"At the Fog Graveyard bring the head of Harold the Wizard."
		GiveUp	=	"1"
		Id	=	"93"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Necromancer¡¯s Infiltration"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"You stupid hold on. Hey Donny! Eat this and wake up. Donny hmmm I did the emergency healing.  Soon hell wake up, but he inhaled quite a lot. He simply lost consciousness. Anyway, you are so stupid! How can you fall for such a trick? 





 I saw not too long ago, a wizard went toward the Fog Graveyard. I think he knew this was the only way to get there.





 I guess he was planning to knock us down with an outsider, but you became a joke to them! I saw someone pass us who was smiling - now I know why! If you dont want me to be suspicious of you, you must bring me the head of the wizard."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20191"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"92;"
					}
				}
				<Branch0>
				{
					Desc	=	"Necromancer, wait for me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"84"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"As soon as I become well and Donny regains his consciousness we¡¯ll try to get there."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I¡¯ll wait until he comes to."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hah!! You are foolish."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Necromancer¡¯s Infiltration"
			Desc2	=	"Is this that wizard? I wonder what he was planning. I dont get it. This is only a graveyard."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20191"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"53"
					Exp	=	"1217"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150003"
					SItem1	=	"157004"
					SItem2	=	"161005"
					SItem3	=	"162004"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"84"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
