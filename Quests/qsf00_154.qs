<Root>
{
	<Quest>
	{
		Desc	=	"You must capture 5 Ahgol Goblin warriors."
		GiveUp	=	"1"
		Id	=	"154"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"God's knit 3(Arketlav)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"As you know all of Rog citizens know that there are 2 main gods? What? You do not know? Are you sure you are a Rog?  Rog worship gods that protect Rogs.   In the past they took revenge on Edios by combing strength with King Prekin, and I am sure they are still watching over us. 



  Out of those two we are followers of Arketlav. Rests are worshipers of Dashamod, to tell you the truth, they are bit uncoordinated, and they tend to drag things a bit. They are unlikable folks. When you become a knight, remember that Arketlav is the best of them all, don't forget that. Heh heh heh. 



 Anyway have you heard the rumors about devil's den activity near giant's territory?  The whole regions are talking about it. We are leading the way to stop them. We will be the last, I guess.?  Ha Ha...I have heard that devil's den influence has been reached to Ahgol Goblins. 



  I like to obtain latest news, would you like to help me and capture 9 Ahgol Goblin warriors at Ahgol gorge? When they are killed and if there's a black smoke that means devil's den influence is already upon them. It's bit blurry, but can you check it out?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20248"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"22"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"153;"
					}
				}
				<Branch0>
				{
					Desc	=	"I will try my best."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Well that's great, great."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"9"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"620"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Let's quit now."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you want to quit I don't care. I have many soldiers lined up to work for me."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"God's knight 3 (Arketlav)"
			Desc2	=	"Hmm did those guys already been influenced by devil? They are decedents of Edios. 





  They are bit strange that they were easily been influenced by devil in the past, which was written in the history book."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20248"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"82"
					Exp	=	"2619"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
