<Root>
{
	<Quest>
	{
		Desc	=	"Bring 6 Gorge Orc warrior officer's wrist band and 6 Gorge Orc wizard's staff to Kevin Dillon."
		GiveUp	=	"1"
		Id	=	"55"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Gorge Orc hunt 2"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"From looks of it, I think I can give you a difficult assignment. Although Orcs are stupid,they are not complete fools. I need to warn you about the surprise attack that happened at the dark gate.  If you go to the mouth of the horned tiger mountain you can find the Gorge Orcs camp.  There lives the leader of the pack.  I don't want to stir them up, but I think a light warning should be a good idea. 







  Anyway if we can get 5 Gorge Orc wizard's staff and 6 wrist bands of warrior officers it will  probably scare them for awhile.  Do you want to try?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"9"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20196"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"54;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I think this will be a job worth doing."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"6"
							ItemCnt1	=	"5"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"49"
							ItemIdx1	=	"50"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I'll believe in your skills"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"What, by myself?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I guess it might be too much for you then."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Gorge Orc hunt 2"
			Desc2	=	"Oh..with this we can scare them for good.  I will look at you in a different light. Great job, I will count on you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"4"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20196"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"23"
					Exp	=	"639"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"154002"
					SItem1	=	"156002"
					SItem2	=	"161004"
					SItem3	=	"167001"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"6"
					ItemIdx	=	"49"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"50"
				}
				<YActNode3>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
