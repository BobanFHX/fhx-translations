<Root>
{
	<Quest>
	{
		Desc	=	"Collect 10 Rebel Badges."
		GiveUp	=	"1"
		Id	=	"337"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Path to Plate Gloves of Honor Exchange"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Defeat all evil forces in the Great Rebellion and collect 10 Rebel Badges.




The badge can be obtained by exchanging seals and talismans."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20310"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"50"
					}
					<CondNode2>
					{
						Class	=	"19"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I will join in the suppression of the rebels."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"The rebels have talismans, seals, and badges. Hearing that, if you collect 10 badges, you will have a corresponding reward."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"They are rebels... it will be hard."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Those who collect 10 rebel badges will have a corresponding reward."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Path to Plate Gloves of Honor Exchange"
			Desc2	=	"You've had a lot of work with the rebels."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20310"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"10"
					ItemIdx	=	"247"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"11"
					Exp	=	"1200"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"180040"
					SItem1	=	"180041"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"247"
				}
			}
		}
	}
}
