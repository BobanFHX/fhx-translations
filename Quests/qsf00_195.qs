<Root>
{
	<Quest>
	{
		Desc	=	"Capture 5 Dust Hobgoblin Commanders and Dust Hobgoblin rods."
		GiveUp	=	"1"
		Id	=	"195"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Dust Hobgoblin's First Attack"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Thanks to you our militant army's moral is at its highest point.  As you know they will go ahead with their all out attack.  This is our chance to get rid of them once and for all and bring peace to our town. 



 You and your pals can go on ahead and capture 5 Hobgoblin Commanders and Dust Hobgoblin Rods, so that you can lower their moral.  Don't be foolish and don't jump in by your self."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20089"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"28"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"194;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes sir, I'll gather my strength."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"5"
							KillCnt1	=	"1"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"790"
							NPCIdx1	=	"830"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you have companions it'll be lot easier."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I will meet with you later"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm, if you want it that way?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Dust Hobgoblin's First Attack"
			Desc2	=	"Ha Ha Ha.it feels great.  Because of this battle we almost wiped out the Dust Hobgoblins.  Although we didn't wipe them all out, they will be much more manageable. For awhile they will not bother us. While they are recovering we can strengthen our army and find a way to stop them once and for all."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20089"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"38"
					Exp	=	"3837"
					Fame	=	"0"
					GP	=	"2"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"160001"
					SItem1	=	"160002"
					SItem2	=	"168000"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
