<Root>
{
	<Quest>
	{
		Desc	=	"긴꼬리쥐족 일꾼을 잡아 단서를 조사하라."
		GiveUp	=	"1"
		Id	=	"294"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"긴꼬리쥐족 조사"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"역시.. 내 생각이 맞군요.. 마녀봉우리에서 피어오르는 독구름은 고용의 습지에 서식하는 생물들에게 악영향을 주고 있네요. 표본으로 구해오신 비늘리자드맨 껍질과 다크슬라임의 진액을 조사해 본 결과, 대량의 독액이 포함되어있었어요. 그렇다면, 이곳 고요의 습지에서 살고있는 비단쥐 일족과 긴 꼬리쥐 일족에게도 영향을 줬을텐데...

먼저, 긴꼬리쥐 일족을 조사해 봐야겠어요. 서쪽으로 가시면 긴꼬리 쥐일족이 서식하는 곳이 나올것입니다. 그중에 일꾼 녀석들이 가장 만만한 상대이니 조사해 보세요. 이상한 물건이나 특이한 상황이 발견되면 즉시, 돌아와서 보고해 주세요."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20296"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"38"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"293;"
					}
				}
				<Branch0>
				{
					Desc	=	"쥐족이라... 금방 다녀오도록 하죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"225"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"서둘러 주세요. 점점 더 야생생물들이 사나워 지고 있는것 같아요."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"제가 쥐만 보면 울렁증이 나타나서.. 죄송합니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"쥐가 얼마나 귀여운데.. 실망입니다."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"긴꼬리쥐족 조사"
			Desc2	=	"역시. 이 녹색치즈 였었나.. 마녀의 봉우리에서 피어오르는 녹색연기와도 관련이 있어 보이는군요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20296"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"4605"
					Fame	=	"0"
					GP	=	"20"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"225"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
