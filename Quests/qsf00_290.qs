<Root>
{
	<Quest>
	{
		Desc	=	"해독된 양피지를 민병대장 로엔 하이스에게 전달하라."
		GiveUp	=	"1"
		Id	=	"290"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"나쁜소식 그리고 더 나쁜소식"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"휴~ 힘든 작업이었네. 몇몇 단어들은 도저히 판독이 불가능하지만, 전체적인 내용은 알아 냈네.. 그런데, 좀 이상한데..
이 양피지에 쓰여진 글귀는 지극히 비밀스러운 일을 기록해 둔것 같은데. 이 양피지의 출처가 도대체 어디인지 궁금하구만, 양피지의 내용은..그러니까..... 

아니지.. 자네는 알지 않는게 좋을 듯 하군. 자, 여기 밀봉된 해석본을 민병대장에게 전해 주게나."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20294"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"34"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"289;"
					}
				}
				<Branch0>
				{
					Desc	=	"궁금해지는군요. 그러나, 비밀유지서약을 거역할 수는 없죠. 믿고 맡겨주세요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"221"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"어서, 전해주시게. 그 해석본을 읽는 민병대장의 얼굴이 궁금해지는구만.. 허허 이일을 어쩐다.."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"불안한걸요. 무너가 잘못되어가는 느낌.. 정중히 거절하겠습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"어쩌면, 해석을 못했다고 하는편이 서로에게 좋은 일일지도 모르지..암..그렇고 말고.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"나쁜소식 그리고 더 나쁜소식"
			Desc2	=	"아!! 이일을 어쩌면 좋단 말인가.. 마계어가 아닌 암호였었나.. 그리고 그 양피지는 팔마스 공작님이 작성하신것이고, 렉터 쇼우에게 주어진 비밀임무라고? 이런.. 이런.. 이번일로 나의 명성이 실추되겠구만.. 어쨌든, 자네의 수고에 감사드리네. 자네를 이상한 일에 끌여 들였구만, 어떻게든 보상은 하고 싶네. 자, 여기 무기들 중에서 하나를 골라 보게나.. 우리 하이스 가문에서 가보로 내려오는 것들이네만, 이제 나에겐 필요가 없을듯 하네.. 모든 책임은 내가 질것이니 자네는 어서 자리를 피하게나.. 이시간 부터 더 이상 팔마스 민병대는 존재하지 않는 것이네."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20293"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"221"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"34"
					Exp	=	"3500"
					Fame	=	"0"
					GP	=	"15"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169029"
					SItem1	=	"169030"
					SItem2	=	"169032"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"221"
				}
			}
		}
	}
}
