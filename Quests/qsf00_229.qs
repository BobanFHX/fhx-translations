<Root>
{
	<Quest>
	{
		Desc	=	"Töte die Schuppenechsen-Jäger und bringe den gestohlenen Rucksack zurück"
		GiveUp	=	"1"
		Id	=	"229"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der gestohlene Rucksack"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey Du, ich habe eine Aufgabe für Dich. Bitte bringe meine gestohlene Tasche zurück, dort drin ist alles was ich besitze - von Reiseausrüstung bis Geld. Ein Schuppenechsen-Jäger hat mir meine Tasche gestohlen.
			
			Ich gebe Dir so viel wie möglich, wenn Du mir meine Tasche zurück bringst."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20282"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"41"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich bringe sie dir."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"195"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Danke , Vielen Dank.."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich habe Angst."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ich verstehe... dann muss ich meine Reise abbrechen..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der gestohlene Rucksack"
			Desc2	=	"Ich wusste Du kannst es, Danke!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20282"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"36"
					Exp	=	"9518"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"195"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
