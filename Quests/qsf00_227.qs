<Root>
{
	<Quest>
	{
		Desc	=	"Bringe Vincents Brief zum Offizier des Dazamor Ordens."
		GiveUp	=	"1"
		Id	=	"227"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Untergrundkristall"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Das ist der Kristall von Belziev, den der Dämonenlord vor 1000 Jahren benutzte, um böse Kreaturen aus der Unterwelt zum Leben zu erwecken. Seine Energie ist schwach weil er beschädigt ist, aber es ist definitiv der Untergrundkristall...



Wenn das wirklich vom stillen Sumpf kommt, werden alle Kreaturen die im stillen Sumpf leben davon beeinflusst.. bring den Brief dem Offizier des Dazamor Ordens."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20279"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"226;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"39"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde es tun."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Beeil Dich."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"193"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich habe keine Zeit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Die Katastrophe in den stillen Sümpfen ist dir egal?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Untergrundkristall"
			Desc2	=	"Der Untergrundkristall? Das muss die Ursache sein, warum die Tiere gewalttätig geworden sind."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"193"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20281"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"56"
					Exp	=	"1942"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"193"
				}
			}
		}
	}
}
