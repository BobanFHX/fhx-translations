<Root>
{
	<Quest>
	{
		Desc	=	"You must give miner's letter to Dust Canton."
		GiveUp	=	"1"
		Id	=	"166"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Suspicious Dust Orcs"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"These Dust Orcs only used to steal weapons or farming tools once in awhile, but now they are starting to steal valuable stones as well.  I wonder if they acquired forge skills or refining skills. 





  Why would they steal them?  I keep meaning to tell Dust, but every time I forget. I would appreciate it if you could do it for me, can you give this letter to him?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20252"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"23"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"165;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes I'll go now."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"144"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I am sorry for the all the trouble."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I guess you have other work that needs to be done.  I understand."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Suspicious Dust Orcs."
			Desc2	=	"Hmm, really? I didn't hear about this either.  I heard it from the Dashmod Priests that some character named Belzev is reborn. 



 I am thinking that his rebirth has something to do with everything getting messy around here"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"144"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20198"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"679"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"144"
				}
			}
		}
	}
}
