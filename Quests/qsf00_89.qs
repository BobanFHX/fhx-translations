<Root>
{
	<Quest>
	{
		Desc	=	"You must take a letter of apology for breaking a promise to James Madio."
		GiveUp	=	"1"
		Id	=	"89"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Broken Promise"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey can you do me another favour? I was supposed to meet with James Madio from the Black Bridge, but I was delayed when I was attacked by scorpions. 





 Since I can't move right away, can you send this letter explaining my late arrival to him? I think he has very urgent things to do and I don't want him waiting for me."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20233"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"13"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"88;"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, I’ll give this letter on my way there."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"80"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I know you’re pretty busy, and I apologise."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am headed to Fog Graveyard."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm I wish Arketlav’s blessings will be with you."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Broken Promise"
			Desc2	=	"Oh no Valenci must have let his guard down. He normally would not let those scorpions get him like that. Anyway I understand. I guess I’ll have to wait. Work is important, but not as important as a person’s life."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"80"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"193"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"80"
				}
			}
		}
	}
}
