<Root>
{
	<Quest>
	{
		Desc	=	"Give Blanko Luliks 6 contaminated croc skin gloves."
		GiveUp	=	"1"
		Id	=	"44"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Contaminated lake"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm...hey you, I don't know who you are, but it looks like my subordinate owes you one.  Since you already started to help me, do you mind helping me little bit more? The experience you learned here will come in handy in later on in your life.  The reason why I wanted the report was to find my missing friends. 





  They lived by hunting near the Black Door.  They came to see me couple of days ago, but then they disappeared.  They're skills are good enough to fend off any animals including Orcs, but I believe some kind of accident might of happened to them. Something is differently wrong and I need to you to go there and check it out. 





  The Mirror Lake it is known for crocs, if you find any contaminated crocs take their skins and bring them back to me.  About 6 of them would do to make sure what is going on."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20195"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"9"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"43;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do my best"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Curse...cur..maybe it finally started."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"6"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"41"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I think I already finished my job here."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...I guess I have to ask one of my men to do this."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Contaminated lake"
			Desc2	=	"Oh great job. These must be those contaminated croc's skin.  Wow  He looks pretty ugly and mean.  Thank a lot, thank you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20195"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"23"
					Exp	=	"639"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150002"
					SItem1	=	"152002"
					SItem2	=	"153001"
					SItem3	=	"157003"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"6"
					ItemIdx	=	"41"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
