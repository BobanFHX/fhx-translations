<Root>
{
	<Quest>
	{
		Desc	=	"You must bring 5 sheepskin from Ahgol Goblin."
		GiveUp	=	"1"
		Id	=	"147"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Ahgol's gorge Goblin."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I am saying this to you because I am appreciate of what you've done.  I actually seen same sheepskin before.  If you go out of Midland and head north, you will see Ahgol's gorge. 





  There lived Ahgol Goblin tribe for a long time. I have seen Ahgol Goblin holding up the sheepskin and jump up and down in joy Hmm...I wonder if that sheepskin is involved with this incident? 



 You never know, if you steal it from them and bring it to the elder of Arketlav heh heh heh...you never know? You might get chance to get to be part of the knights?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20244"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"21"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"145;"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm..aside from heroism this is important work."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Heh, heh, heh...at least you must obtain 5 then they might recognize you."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"126"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Don't talk to me about nonsense"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Well you must be on your way, thank you for everything."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Ahgol's gorge Goblin"
			Desc2	=	"Oh did you take care of it?  Huh? This? Another sheepskin, let me take a look.  Hmm...this seems like another piece, Ahgol Goblin has it? 



  I don't know where you got the information, it must be sheepskin written in devil's writing."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"3"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20246"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"5"
					ItemIdx	=	"126"
				}
				<CondNode2>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"79"
					Exp	=	"2408"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
