<Root>
{
	<Quest>
	{
		Desc	=	"You must bring 5 Stone Bat's Teeth to Peter Carlos."
		GiveUp	=	"1"
		Id	=	"179"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Stone Bat's Teeth"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh? I can't believe I am still alive.  Phew~thank you very much.  I have been saved twice thanks to you.  How can I repay you. 



 Oh boy~oh boy~ my skin is completely healed, but my insides are still not completely well.  It was made to kill and therefore I don't think they gave us a complete antidote.  It is true that I am a spy in this area so I know a lot.  In this case the only solution is to obtain Stone Bat's Teeth. 



 Fortunately this area is a mining area where Stone Bats live.  Can you please get me 5 Stone Bat's Teeth."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20257"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"27"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"178;"
					}
				}
				<Branch0>
				{
					Desc	=	"Well I guess I have to help you all the way."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I'll leave it up to you."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"154"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am injured as well...so it might be too difficult for me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah...sorry to hear that, you're injured because of me."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Stone Bat's Teeth"
			Desc2	=	"Ah? you have brought it.  Crunch, crunch?I should clean it, but the situation is very dire, so I have to just eat them.  If I let it go, it might get even worse.  Thank you for everything"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20257"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"95"
					Exp	=	"3837"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"154"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
