<Root>
{
	<Quest>
	{
		Desc	=	"Töte den Hobgoblin-Krieger um das rote Reagenz zu bekommen!"
		GiveUp	=	"1"
		Id	=	"207"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der Verräter Andoras (2)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Danke für das Dokument, allerdings zerstäuben die Hobgoblins Reagenzien auf ihre Dokumenten; somit können wir diese nicht lesen. Wenn wir den Inhalt lesen wollen, brauchen wir ein rotes Reagenz.



Bitte finde und töte den Hobgoblin-Krieger um das rote Reagenz zu bekommen."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20276"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"206;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"31"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde es gleich tun."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"178"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"So werden wir den Verräter schnell finden."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich habe keine Zeit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Aber, wir müssen den Verräter finden!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der Verräter Palmas (2)"
			Desc2	=	"Jetzt können wir das Dokument lesen. Mal sehen was drinnen steht..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20276"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"5"
					Exp	=	"4936"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"178"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
