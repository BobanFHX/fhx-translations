<Root>
{
	<Quest>
	{
		Desc	=	"Skin 5 Small Grey Wolf and take their soft fur to Roy Clower."
		GiveUp	=	"1"
		Id	=	"4"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Oily towel"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Good day, champ! I don't think you coming here is a coincidence. I am Roy and I sell axes. I don't want to brag, but I have the best axes in town. I love them more than my own children. To keep my axes in the best shape, I have to clean them with a soft wolf's skin every morning. This morning my towel became too worn-out to use. 





   If you venture northeast of Rog Castle, you can find a pack of Small Grey Wolf. If you get me their fur I can make a new towel. Please bring me about 5, that should do."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20002"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"3;"
					}
				}
				<Branch0>
				{
					Desc	=	"Should be an easy job."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ha ha, I knew you were the right guy. Thank you, champ!"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"5"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't want to."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ouch, I guess it was a coincidence after all..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Oily towel"
			Desc2	=	"Back already? Wow, these skins are perfect for my beautiful axes. Thank you! Here is a little compensation for your troubles."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20002"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"7"
					Exp	=	"93"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150000"
					SItem1	=	"152000"
					SItem2	=	"154000"
					SItem3	=	"156000"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"5"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
