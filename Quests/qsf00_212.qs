<Root>
{
	<Quest>
	{
		Desc	=	"Bringe das Herz des Riesen Bär zu Lyon Markess."
		GiveUp	=	"1"
		Id	=	"212"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der Apotheker"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Danke für das Herz des Riesen Bär, aber es ist roh und unbrauchbar.



Wir müssen eine Medizin aus dem Herz machen. Lyon, der Apotheker beherrscht diese Kunst. Er wohnt im Osten nahe der Berge. Kannst Du im bitten aus dem Herz eine Medizin herzustellen?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20277"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"211;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"32"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich tue es."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Dann beeil Dich bitte."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"180"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich kann Dir nicht helfen."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du bist ein kaltblütiger Mann..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der Apotheker"
			Desc2	=	"Was ist das? Das Herz eines Riesen Bär??"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"180"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20278"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"39"
					Exp	=	"1845"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"180"
				}
			}
		}
	}
}
