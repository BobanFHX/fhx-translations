<Root>
{
	<Quest>
	{
		Desc	=	"You have to pay Ghiyorn Chover for the holy water at the Black-Stone Graveyard."
		GiveUp	=	"1"
		Id	=	"100"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Ghiyorn¡¯s Holy Water"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"The appearance of the undead is a very rare case, but not to humans. That¡¯s not true.  Actually, the undead have appeared without a cause once in a while.



 I¡¯ve seen the undead several times since I started my new post.  However, I¡¯ve never seen the appearance of the undead in such a number.  Frankly it is unexpected.  They are too many. 



It takes so much trouble to kill them even with a weapon.  We need something that can kill them with a single blow.  Hey!!  You!!  If you go to the east, there is Black-Stone Graveyard.  There is a grave keeper called Ghiyorn whose family have been undertakers for 3 generations.  I heard the family have seen many undead in these generations.  Many people were hurt by the undead.  About 40 years ago??  A Dajamaroo priest initiated the family into the secrets of making holy water that is very effective against the undead.  If we had the holy water, it would be easier to fight them. 





 Well  The problem is that the old man likes money a lot..  I think it is impossible to learn the secret recipe.  Ill give you some money, and can you buy a couple of bottles of holy water from the old man??"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"99;"
					}
				}
				<Branch0>
				{
					Desc	=	"I¡¯ll do that."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"92"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"The old man is a miser."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am tired of it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I see.  I¡¯ll ask someone else then."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Ghiyorn¡¯s Holy Water"
			Desc2	=	"What?  You want the holy water??  Do you think it can be given to just anybody?  It has been handed down only to my family for generations.  I can¡¯t give it to you.   Don¡¯t even think about it. 





Wait a minute!!  It looks like gold coins that shine on your hand.  Mm Gold coins Gold coins.. Gold always makes me excited just by looking at it.  Hahahah... If you give me the gold coins I¡¯ll give you a couple of bottles of the holy water."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"92"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20236"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"16"
					Exp	=	"250"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"92"
				}
			}
		}
	}
}
