<Root>
{
	<Quest>
	{
		Desc	=	"마계어로 쓰여진 양피지의 해독을 위해 프리드리 교수님을 찾아가라."
		GiveUp	=	"1"
		Id	=	"289"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"마계어 양피지 해독"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"허허, 이게 무엇이란 말인가. 이 알수 없는 문자로 쓰여진 글귀는.. 도대체가 알아 볼 수가 없구만, 분명히 마계어 같은데...
아! 프리드리 교수님이라면, 분명 해석이 가능하실 것이야. 자네는 이 양피지를 가지고, 프리드리 교수님을 찾아가게나, 최근에 교수님은 호수유적에 학술 조사를 위해 가신다고 하셨으니, 아직 그곳에 계실것이야. 어서 서두르게나, 우리 팔마스 민병대의 명성은 자네의 손에 달려있네."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20293"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"34"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"288;"
					}
				}
				<Branch0>
				{
					Desc	=	"팔마스 민병대를 위하여!! 당연히 다녀와야죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"220"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"어서 서둘러 가시게나."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"마계어.. 그런 언어를 읽을 수 있는 사람이 있을까요? 악마가 아니라면.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"자네.. 갑자기 왜이러나? 팔마스 민병대의 명성을 널리 알리는 중요한 일이네.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"마계어 양피지 해독"
			Desc2	=	"이 문자는.. 자네가 이걸 어디서 구해왔나?.. 어디 볼까.. 흠~ 으음..음..으.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20294"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"220"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"34"
					Exp	=	"3500"
					Fame	=	"0"
					GP	=	"15"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"220"
				}
			}
		}
	}
}
