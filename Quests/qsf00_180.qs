<Root>
{
	<Quest>
	{
		Desc	=	"You must deliver the Spy's Bag to the Palmas's spy leader Trans."
		GiveUp	=	"1"
		Id	=	"180"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"To Palmas"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Now that my body is fully healed, I should be on my way. Thank you so much for your help. I guess I owe you my life and am forever indebted to you.  Thank you very much. 



  However, I must head to Rog castle.  Though I am healed, I still need time for my body to function properly.  I need to report this to my officer.  I was wondering if you are headed toward Palmas, can you give this bag to Trans  my commanding officer?  Trans belongs to the royalty of Rog and is now working in Palmas. 





   I am sorry to ask you for another favor, but knowing that The Devil's Den are moving ever so close, I feel it is urgent.  Please do this favor for me."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"28"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20257"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"179;"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm..it's on my way I'll give it to him."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Thank you"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"155"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, but I am not headed toward Palmas."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah, I see.  I guess you can't do it then."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"To Palmas"
			Desc2	=	"Mmm..you knew who I was? I wanted my identity to remain a secret, but anyway what is it? 





 Mmm...mmm...really? My spy owes you a debt of gratitude, and also you brought this all the way here.  As a superior I give you my thanks, and I will use this to the fullest extent."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20258"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"155"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"24"
					Exp	=	"2486"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"155"
				}
			}
		}
	}
}
