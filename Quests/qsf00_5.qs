<Root>
{
	<Quest>
	{
		Desc	=	"Bring stolen bow from Vagabond Goblin to Elmo Leon."
		GiveUp	=	"1"
		Id	=	"5"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Count's bow"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh no! What should I do? 



 Huh? Who are you? My expression is strange? Let me explain. A few days ago I finished a beautiful new bow for the count of Rog. But as I was walking around Rog I was robbed by a Vagabond Goblin. Disgusting little creature stole my beautiful bow! I could report it to the town guards, but they wouldn't listen to a merchant like me... 





   Please help me and take back the count's bow from the Vagabond Goblin!"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20003"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"4;"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, I'll get it for you right away."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Please hurry, the count will have my head if I don't deliver his precious bow on time... There should be a Goblin camp straight from the north gate."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"6"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I know your situation is bad, but I am busy."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh no! Who else can I turn to? I will have to get away before the count finds me..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Count's bow"
			Desc2	=	"Oh, thank you! Thank you! I owe you my life. Thank you very much. Thank you. 



  I don't have much, but please take this. I will always be grateful.







  By the way, if you are looking for another job, you should head to the bank. I've heard they are in trouble... I will definitely stay away or I might get in trouble as well."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20003"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"28"
					Exp	=	"93"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"162000"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"6"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
