<Root>
{
	<Quest>
	{
		Desc	=	"Kill 9 Bulldog skull warriors and 9 Bulldog skull archer."
		GiveUp	=	"1"
		Id	=	"125"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Death Ruler [Frontal attack]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Anyway, the Daijaru will begin their move and we must complete our investigation. During my investigation it's been indicated that what I found over by the Black Stone Cemetery ended up leading me to the Fog Cemetery. When I followed it, I found three creatures still alive while another being was heading into the cemetery. 





 They used sleep spells to put the guards to sleep, and continued in deeper into the cemetery. There were too many undead, so you and I need to make a way before.... 





 Why don't you go head to head against 9 Bulldog skull warriors and 9 Bulldog skull archers. I will take another side of the dungeon. I'll meet up with you again later."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20239"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"18"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"124;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes, I understand."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"9"
							KillCnt1	=	"9"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"485"
							NPCIdx1	=	"435"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Move carefully, they have pretty good hearing."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am a little afraid."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Afraid? what are you afraid of? Forget it, I'll do it myself, coward!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Death ruler [frontal attack]"
			Desc2	=	"Hmm...first of all, did you get rid of those in the front? I got rid of mine. 





. Those unknown creatures had some great spirit...not too many of them can have that kind of spirit."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20239"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"71"
					Exp	=	"1839"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
