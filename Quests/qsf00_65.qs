<Root>
{
	<Quest>
	{
		Desc	=	"Deliver the liquid medicine to the spy who is isolated in the Tiger Forest Goblin camp"
		GiveUp	=	"1"
		Id	=	"65"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Unprepared Paladin"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Glory of Ahshutat be with you! I don't know which temple you belong to, but you are a holy warrior. Glad to meet you. I am glad to see a Paladin and I feel like Iegees spirits are cheering. 





   I'm glad you're here.  I have a favour to ask you.  If you are the Paladin who can sacrifice and serve, then you can listen to my request? I have a friend named William Sheliband who is the Iegees Scout who got isolated. A pigeon came flying in. 





   He and I used to communicate by sending each other pigeons.  If you want to bring him out, you will need liquid medicine.  Can you help my friend who is hiding in Tiger Forest Goblin camp?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"9"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20224"
					}
					<CondNode2>
					{
						Class	=	"16"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, I'll do it in the name of Paladins."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"59"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then Ashutat's glory be with you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you truly a Paladin?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Unprepared Paladin"
			Desc2	=	"Hmm? You are? How did you find me? Are you a lord? You are great. Nobody knows my whereabouts. 





  I sent a pigeon to my friend, but I wasn't sure it survived. You came all the way here to give this to me?  May God's blessings be with you. Thank you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"59"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20183"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"49"
					Exp	=	"111"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"59"
				}
			}
		}
	}
}
