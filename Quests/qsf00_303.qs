<Root>
{
	<Quest>
	{
		Desc	=	"흉폭한 다크슬라임을 처치하고 다크슬라임의 진액 4개를 구해오시오."
		GiveUp	=	"1"
		Id	=	"303"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"수습연금술사의 부탁5"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"음.. 먼저.. 붉은 시약을 붉은 시단으로 만들고..
여기에 실명초와 박쥐날개가루를 갈아서 넣고... 또...
검붉은 시약을 넣고 5분간 기다리면.. 아!! 기다리면서 배고프면 먹을려고 준비한 검은연꽃.. 이건 내가 먹고 힘을 내야지..냠냠..

어라.. 이상하네.. 생각한 순서가 맞다면 청색연기가 피어올라야 하는데..

아하!! 용사님.. 제가 깜빡하고 준비하지 못한 재료가 있군요. 이전에 검은 연꽃 근처에 출몰하던 다크 슬라임 기억하시죠? 다크 슬라임의 진액이 필요해요. 가능하면, 가장 힘쎈놈들의 진액이 필요하니.. 

흉폭한 다크슬라임에게서 다크슬라임의 진액 4개만 구해주시겠어요?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20297"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"46"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"302;"
					}
				}
				<Branch0>
				{
					Desc	=	"나도 배고픈데..일단은 진액을 구해오도록 하죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"용사님 정말 정말 귀찮게 해서 죄송해요. 다녀오세요~~"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"4"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"229"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"흠.. 금방 먹은건 검은연꽃?? 당신이 배고플때 먹기위해서 나를 위험한일을 시켰단 말이오? 이젠 안되겠오."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"배가 고프면 실험이 잘 안되서.. 이제 다 됬어요.. 제발..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"수습연금술사의 부탁5"
			Desc2	=	"이 물약은 용사님의 도움으로 실험에 실험을 거듭해서 만들어낸 영약이에요. 그동안 도와주신 노고에 보답하는 저의 작은 성의이니 필요하실때 사용하세요. 전 이제 이 조제법을 연금술협회에 등록해야겠어요. 저도 이젠 정식연금술사가 될 수 있을거에요. 고마워요..용사님."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20297"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"13877"
					Fame	=	"0"
					GP	=	"34"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169034"
					SItem1	=	"169033"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"4"
					ItemIdx	=	"229"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
