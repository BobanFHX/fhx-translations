<Root>
{
	<Quest>
	{
		Desc	=	"Deliver the proof to the Alexia,  Rog's wizard."
		GiveUp	=	"1"
		Id	=	"21"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Ruin's Piece (Alexia)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey there, if you look around Rog castle you'll see Alexia, the king's Wizard taking a walk.  Can you go to him and show this item?  I think you can get some information about this item from him. 





 I can't go because I have to keep my post.  Since you brought this item anyway, you should see to it to the end, don't you think so? Hee hee, if you are Rog's warrior then you should finish it, right?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20194"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"3"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"20;"
					}
				}
				<Branch0>
				{
					Desc	=	"Leave it to me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"This matter is not something you should ignore.  Please hurry."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"171"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am bit busy."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You are a young person with few words."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Ruin's Piece (Alexia)"
			Desc2	=	"Huh? Who are you? Favour from guard captain? Let me see that item.  Hmm?  What is this? I need to see it more closely.  Hmm If this item is what I think it is, then this is no small matter. I hoped that this item would turn out to be, but why does it happen to come out in such a bad place? Anyway I thank you for bringing it to me."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"171"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20206"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"9"
					Exp	=	"43"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"171"
				}
			}
		}
	}
}
