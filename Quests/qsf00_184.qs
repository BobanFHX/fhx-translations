<Root>
{
	<Quest>
	{
		Desc	=	"Woolpen Dewonb is asking for 8 Grey Dusty Wolf."
		GiveUp	=	"1"
		Id	=	"184"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Grey Dusty Wolf Hunt"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey you, can you help me? I am a miner who lives in the Shadow Mine town.  I want to go home, but there's grey dust wolf roaming around the area. 





 Can you kill 8 grey dust wolf for me so that I can go home?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20262"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure I'll help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Thank you."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"8"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"675"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, but I am already busy with something."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Really? Can you rethink your decision?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Grey Dusty Wolf Hunt"
			Desc2	=	"Ah~ thank you very much. I can now safely go home."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20262"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"87"
					Exp	=	"3072"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
