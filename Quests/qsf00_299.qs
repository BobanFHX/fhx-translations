<Root>
{
	<Quest>
	{
		Desc	=	"작은 붉은 슬라임에게서 붉은 시약 5병을 구해오시오."
		GiveUp	=	"1"
		Id	=	"299"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"수습연금술사의 부탁1"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"용사님은 슬라임을 잡아 보신적이 있으세요? 그 물컹물컹하고 허물허물대는 생명체란... 정말이지 어디서 그런 생물이 나온걸까요.. 사실 전 연금술 협회에 소속된 수습연금술사입니다. 
실험중인 조제법이 있어 이것저것 재료를 모아야 하는데. 보시다시피 저는 슬라임을 상대할 만큼 용감하지도 재능이 있지도 않아요. 용사님이 저를 좀 도와주신다면 생각한 조제법을 금방 완성 할 수 있을텐데...어떠세요.

잠시 시간을 내어 도와주실래요?

일단, 작은 붉은 슬라임 시약 5병만 구해주시겠어요?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20297"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"38"
					}
				}
				<Branch0>
				{
					Desc	=	"어떤 조제법인지 궁금해지는데...일단 잠깐 도와드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"227"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"아.. 정말 고맙습니다. 고맙습니다."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"직접 구하지 못한다면, 수많은 실험에 쓰일 재료를 어떻게 충당할려고..직접 하세요..직접"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"......"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"수습연금술사의 부탁1"
			Desc2	=	"감사합니다. 용사님.. 이제 한가지 재료는 구했군요.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20297"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"4605"
					Fame	=	"0"
					GP	=	"10"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"227"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
