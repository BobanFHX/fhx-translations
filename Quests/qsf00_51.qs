<Root>
{
	<Quest>
	{
		Desc	=	"You must hear the story from Michael Kan who is guarding Black Door, about the Orc Settlement."
		GiveUp	=	"1"
		Id	=	"51"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Guard's report"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm...about 10 years ago there was a plan to sweep out the Orcs which were threatening the Blue Gorge. I thought it was somewhat secure now. I think the Orcs have started to grow in numbers. I heard that they have been growing strong and more aggrasive. 





  I heard news from the guards who were being posted at the Black Door.  Over there they said that Orcs menace has became much more severe.  I think Kevin Delion the captin is asking anyone to join the punitive force, do you want to join them? All you have to do is go and see Capt. Kevin Delion at the Black Door."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20195"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"7"
					}
				}
				<Branch0>
				{
					Desc	=	"I guess I'll try"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"46"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Show your bravery to him."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I need to think about this"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Well you're not being forced to go...."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Guard's report"
			Desc2	=	"Huh? Aren't you the punitive forces reinforcement? That's great, because they were getting much worse lately. .



 Anyway, Rosei Johnson will tell you more about it in detail. I just thank you for your bravery"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"46"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20181"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"83"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"20000"
					Desc	=	"응? 너는 오크 토벌단에 지원한 지원병인가? 그렇지 않아도 잘되었네. 요즘들어서 그녀석들이 워낙 자주 날뛰어서 말이야."
				}
			}
		}
	}
}
