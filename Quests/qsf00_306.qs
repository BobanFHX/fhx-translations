<Root>
{
	<Quest>
	{
		Desc	=	"촌장의 편지를 크리스티 메이커에게 전해주시오."
		GiveUp	=	"1"
		Id	=	"306"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"늙은눈 프리먼 듀의 부탁"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"난.. 우리 손자들에게 이렇게 이야기 하네. 난세에 영웅이 태어나는것이 아니라. 영웅은 난세에서 태어나는것이라고..이말은 곧 힘든 환경에서도 노력과 의지만 있다면 누구든 영웅이 될 수있다는 뜻이기도 하지..

여기, 종이에 크리스티에게 전해줄 말을 써줌세.. 이 편지를 크리스티에게 어서 전해주게나. 그럼 무운을 비네.."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20299"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"51"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"305;"
					}
				}
				<Branch0>
				{
					Desc	=	"왜 갑자기 무운을 빌어주시죠? 어쨋든.. 편지 정도야 전해주도록 하죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"어서 가보시게나.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"230"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"전 다른 볼일이 급해서....다른사람에게 시키세요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"허허.. 이러면 안되는데.. 자네가 꼭 전해 줘야만 하네.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"늙은눈 프리먼 듀의 부탁"
			Desc2	=	"아.. 촌장님이 주신 편지군요.. 직접 가져다 주셔서 감사합니다. <편지를 읽으며.. 고개를 끄덕인다..>"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"1"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20298"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"2442"
					Fame	=	"0"
					GP	=	"6"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"230"
				}
			}
		}
	}
}
