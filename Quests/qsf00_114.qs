<Root>
{
	<Quest>
	{
		Desc	=	"Kill Skull Lord and bring Deadman¡¯s Piece."
		GiveUp	=	"1"
		Id	=	"114"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Last Piece"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Now let me see what you brought. Ah this is this isn¡¯t the piece made by Cain¡¯s Curse - this is new.  I've never seen it before Mmm I'm sorry. I was just thinking to myself and I was mumbling. 





  Sorry, but this is my first time looking at this item. I don't think this is from an individual, but from a group. 



 Hmm Spirit Ruler, can you help me? There is a monster which is connected to this, and it is around here. Can you capture him and see if he has the same pieces? If he does can you bring it to me? It is called Skull Lord."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"5"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20237"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"16"
					}
					<CondNode2>
					{
						Class	=	"512"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"113;"
					}
					<CondNode4>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I will bring Deadman's piece."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"102"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It may be too much to battle it."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Is there another way?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"(He simply swings his head from side to side)"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Last Piece"
			Desc2	=	"With this I can find out for sure. As soon as I am done with this research, I am planning to head to Rog. I need to tell them the seriousness of this matter. Normally the politicians at Rog would not easily be moved even with this evidence."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20237"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"66"
					Exp	=	"1510"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150007"
					SItem1	=	"151000"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"102"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
