<Root>
{
	<Quest>
	{
		Desc	=	"집시 홉고블린 지도자 10마리와 집시 홉고블린 로드 10마리를 처치하라."
		GiveUp	=	"1"
		Id	=	"308"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"추가 수색작업"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"이제 비적단의 세력이 많이 약화되었어요. 힘들겠지만, 좀더 타격을 주어야만 해요. 이번에는 집시 홉고블린 지도자,로드를 10마리씩 처치해주세요. 이번일이 끝나면, 모리스씨의 행방을 확실히 파악할 수 있을듯 해요."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20298"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"53"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"307;"
					}
				}
				<Branch0>
				{
					Desc	=	"문제없습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"믿음직한 용사님.. 너무 든든해요."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"10"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1400"
							NPCIdx1	=	"1405"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"이젠 너무 지쳤어요. 나중에 도와드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"조금이라도 늦으면 모리스씨는..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"추가 수색작업"
			Desc2	=	"역시 믿음직 하군요. 용사님에 대한 소문이 요즘 집시촌 뿐만아니라 이디오스대륙에 자자해요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20298"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"22130"
					Fame	=	"0"
					GP	=	"55"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
