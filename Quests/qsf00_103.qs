<Root>
{
	<Quest>
	{
		Desc	=	"Catch 10 Fog Wolves."
		GiveUp	=	"1"
		Id	=	"103"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"A Fog Wolf Hunt"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm... He found out so quickly that the dead were revived.  Even the general and I just figured out that fact.  As expected, you are worthy to represent knights.  I don't know what I was thinking by sending a rookie.  I think the general recognises him a little.  



 However.. I still can't trust your ability.  I am going to give you a very simple test.  It is not an unusual thing.  There are Fog Wolves that are very quick around this area.  I want you to catch 10 of them.  Let me see whether you can do it or not."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
					<CondNode2>
					{
						Class	=	"2"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"60;"
					}
				}
				<Branch0>
				{
					Desc	=	"You won’t underestimate me any more."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"345"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I am not underestimating you, but it is very rough."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I quit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...  Is he the one who is recognised by the general?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"A Fog Wolf Hunt"
			Desc2	=	"Oh... You did it.  Now I am convinced of your ability.  It is good that you are here, since we needed more resources.  So many undead have just came in groups."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"66"
					Exp	=	"1670"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
