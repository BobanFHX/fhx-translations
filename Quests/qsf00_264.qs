<Root>

{

	<Quest>

	{

		Desc	=	"Töte den Zwergork-Schamanen und bring seinen Kopf zu Fabian Schneider"

		GiveUp	=	"1"

		Id	=	"264"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Die Paladinausbildung"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Du willst ein Paladin sein, der zusammen mit den Rittern mit reinem Herzen und Kraft den Zauberern hilft und sie beschützt.

Du hast deine spirituellen Kräfte zu nutzen um unsere Krieger zu schützen und um ihnen zu helfen. Ich weiß Du musstest durch beträchtliche Ausbildungen gehen, aber nicht so Anspruchsvolle wie ich es erwarte. 



Wenn Du die Aufgabe bestehen und Deinen eigenen Weg gehen willst, dann gehe und töte einen Zwergork-Schamanen und bring mir seinen Kopf."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20288"

					}

					<CondNode1>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"50"

					}

					<CondNode2>

					{

						Class	=	"16"

						CondNodeType	=	"20002"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich mache es."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"10000"

							ItemCnt0	=	"1"

							ItemCnt1	=	"0"

							ItemCnt2	=	"0"

							ItemCnt3	=	"0"

							ItemIdx0	=	"207"

							ItemIdx1	=	"-1"

							ItemIdx2	=	"-1"

							ItemIdx3	=	"-1"

							SvrEventType	=	"1"

						}

						<YActNode1>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Alleine kämpfen ist immer schwer und hart."

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich mache es nicht."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Kein Fortschritt, Kein Gewinn."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Die Paladinausbildung"

			Desc2	=	"Mein Gefühl war richtig. Ich wusste dass Du es schaffst."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"3"

				<CondNode0>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"1"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20288"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"13"

					Exp	=	"17137"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"1"

					ItemIdx	=	"207"

				}

				<YActNode2>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

