<Root>
{
	<Quest>
	{
		Desc	=	"Deliver lucky ring to Lilia Poster."
		GiveUp	=	"1"
		Id	=	"3"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Symbol of heart"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I don't know what to say, huh huh. I've been in love with Lilia ever since I met her. I am sorry, but could I ask you another favour? 



 Since I am not good at writing letters, could I ask you to deliver this? It is my most precious ring. If you give it to her, she will know how I feel."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20000"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"2;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll gladly honour your love for each other."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"4"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I know I'm rushing it, but would you come to my wedding? Huh, huh!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have more important things to do."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I will look for someone else for this favour."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Symbol of heart"
			Desc2	=	"What is this? My goodness, it is the most beautiful ring I have ever seen! I think this means he feels the same way. Thank you with all my heart, friend. If it wasn't for you, I don't know if I would have ever had this opportunity. 





 I pray that all you do will turn out well."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"4"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20202"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"18"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"4"
				}
			}
		}
	}
}
