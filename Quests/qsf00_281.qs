<Root>
{
	<Quest>
	{
		Desc	=	"난폭자, 배신자. 탈주병, 탈주기사를 각각 10명씩 해치우시오."
		GiveUp	=	"1"
		Id	=	"281"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"탈주범 처리"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"팔마스 가문이 괴멸당한 이유는 현재 한창 조사중이지만, 마계와 내통한 간악한 세력의 소행임이 분명하네. 그러나, 팔마스 기사단의 훈육사관인 나 티터스 모렐은 공작님의 살해소식보다 긍지와 신념을 가져야 할 팔마스 기사단원들중에 공작님을 배반하고 도망친 녀석들이 있다는 사실이 더 분하네. 

나는 팔마스 공작님의 미망인과 식솔을 보살펴야해서 잠시도 출정을 나갈 수가 없네.그런데, 자네의 소문을 듣자하니 요근래 보기드문 정의감과 신념으로 명성이 알려지고 있더군. 

어디.. 자네에게 탈주병 처리에 전권을 위임할테니 본보기로 팔마스 난폭자, 팔마스 배신자, 팔마스 탈주병, 팔마스 탈주기사 10명씩만 처단해 주지 않겠나?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20290"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"30"
					}
				}
				<Branch0>
				{
					Desc	=	"이런 무책임한 놈들을 봤나, 걱정마시고, 저에게 전권을 일임해주십시요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"10"
							KillCnt2	=	"10"
							KillCnt3	=	"10"
							NPCIdx0	=	"890"
							NPCIdx1	=	"905"
							NPCIdx2	=	"935"
							NPCIdx3	=	"955"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"역시 소문만큼이나 용감하구만, 다녀오시게. 아참, 저기 분주히 보급품 조달을 하고있는 테일 타터스도 탈주범들이 훔쳐나간 보급품 회수에 골머리를 앓던데 알아보게나."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"팔마스 공작님의 살해와 관련해서 2개의 기사단이 조사중인것으로 알고있는데. 그쪽에 맡기시죠?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"허허.. 요즘 젊은것들은 하나같이 정의감이 없네 그려.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"탈주병 처리"
			Desc2	=	"역시.. 듣던대로 용감한 젊은이로구먼.. 이 무기와 방패는 우리 팔마스의 긍지가 담겨있네.. 잘 사용 하시게나."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20290"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"4348"
					Fame	=	"0"
					GP	=	"99"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"153003"
					SItem1	=	"158003"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
