<Root>
{
	<Quest>
	{
		Desc	=	"Bring 7 mineral copper ingredients to make a mace to Dorika Hood."
		GiveUp	=	"1"
		Id	=	"61"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Hero With Strength"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"It looks like you are a strong hero. If you boast like that, who will call you the warrior? What? Am I picking a fight? Yeah, I'm picking a fight, do you think you can call yourself a true warrior? I only see you as flashy and nothing else. 





   I guess you are humble after all. I think you are different from others. Not only are you acting tough, you are willing to learn as well.  Good, 





  if you want to hear my story then bring me the main ingredients of 7 mineral copper to make a mace.  If you get me these ingredients then I'll tell you what you are lacking.  Mineral copper is given out by Tiger Forest Warrior Goblins at the top of the Horn Tiger mountain range."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20006"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						Class	=	"1"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"Ha, I'll see you after."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"7"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"56"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Let me see what you are made of."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't go to places like that."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ha, I knew it"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Hero With Strength"
			Desc2	=	"Hmm you did well. It's pretty heavy but you managed to bring it back. ha ha ha I finally see someone I like ha ha ha."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20006"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"51"
					Exp	=	"738"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"7"
					ItemIdx	=	"56"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
