<Root>
{
	<Quest>
	{
		Desc	=	"Aquire a medicine for wounds for Alex's hand."
		GiveUp	=	"1"
		Id	=	"35"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Favour from injured"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hello? Phew! My name is Alex and I also work for Jackson like Tom.  Pant..pant..I was being robbed by those Orcs, and I was being chased by Orc and one them threw an axe which left me a big wound.  Arrg...arrg...





 If you are on your way to Kinen spring can you help me. Near the Kinen Spring lot of medicine men are around there.  They have a good medicine for wounds like mine.  Arrg...here is my money can you get some medicine from one of the medicine man? QST35_BRC0_TOB0_ATTtitle  I was actually on my way there."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20215"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"6"
					}
				}
				<Branch0>
				{
					Desc	=	"I was on my there anyway."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"34"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Kinen Spring has been known to be clean and pure."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"This isn't the way I wanted to go."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you don't want to go then you must be on your way."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Favour from injured"
			Desc2	=	"What? You came to buy medicine for rubbing? Young person, you probably didn't hear the rumour about this place?  Then I'll explain it to you.  This place used to be one of the cleanest and purest waters anywhere.  Now thanks to evil black wizard's curse, water became foul and strength was cursed. 





 You probably can't get the medicine which you seek."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"34"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20216"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"42"
					Exp	=	"71"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"34"
				}
			}
		}
	}
}
