<Root>
{
	<Quest>
	{
		Desc	=	"Töte die großen Sumpffledermäuse und sammle 10 Flügel."
		GiveUp	=	"1"
		Id	=	"239"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Merkwürdige Sammlung (2)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Eines der Objekte die ich unbedingt haben muss, sind die Flügel der großen Sumpffledermaus. Die Fledermaus ist stärker als die normale Fledermaus, deshalb sind die Flügel was besonderes.
			
			Wenn Du mir 10 Flügel von dieser Fledermaus bringen könntest würde das meine Sammlung sehr bereichern. Es soll Dein Schaden nicht sein."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20284"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"47"
					}
				}
				<Branch0>
				{
					Desc	=	"Sicher, kein Problem."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"202"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Wenn Du sie siehst, wirst Du verstehen warum ich sie sammle.."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich habe was anderes vor."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Urgh.. traurig.. Du verstehst nicht wie viel Spaß es macht seltene Dinge zu sammeln.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Merkwürdige Sammlung (2)"
			Desc2	=	"Oh... das sind die Flügel einer großen Sumpffledermaus. Sie sind ganz anders als die Flügel von normalen Fledermäusen."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20284"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"72"
					Exp	=	"13877"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"202"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
