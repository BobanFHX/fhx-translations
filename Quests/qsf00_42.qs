<Root>
{
	<Quest>
	{
		Desc	=	"You must free 7 cursed Nieas."
		GiveUp	=	"1"
		Id	=	"42"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Tears of Nieas"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Sniff..sniff..this lake is now lost of it's strength. Do you think this lake is still beautiful? To me it is filled with dark spirit. 





   I can hear my friends drown in darkness and crying out.  They need to be freed from dark strength.  My friends are connected to the place and can't find any freedom.  Do you think you can free my friends from their curse? 





  Only way to free my friends is to end their lives so that they can return to their natural place.  Wouldn't you be a help to find their freedom?  At least 7 would do."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20220"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"8"
					}
				}
				<Branch0>
				{
					Desc	=	"Spirit shouldn't be sad, wait here."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"7"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"165"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah! the sound of sadness from the cursed ones."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry but I have my own things to take care of."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"This type of attitude is a true human attitude."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Tears of Nieas"
			Desc2	=	"Thank you, now they are smiling upon you.  I thank you for your act of bravery."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20220"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"47"
					Exp	=	"548"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
