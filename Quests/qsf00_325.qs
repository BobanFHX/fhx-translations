<Root>
{
	<Quest>
	{
		Desc	=	"팔마스 탈주병에게서 팔마스 기사단 갑옷 5개를 회수해 주시오."
		GiveUp	=	"1"
		Id	=	"325"
		NumOfQUNode	=	"2"
		Repeat	=	"1"
		Title	=	"전쟁 보급품 회수 - 갑옷[반복]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"기사단 재창설을 위해 지원자에게 보급해야될 갑옷이 턱없이 부족한 실정이네. 

자네가 패잔병들을 처리하고 팔마스문장이 박혀있는 팔마스 기사단 갑옷을 회수해 주겠나?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20291"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"45"
						MinVal	=	"30"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"284;"
					}
				}
				<Branch0>
				{
					Desc	=	"맡겨주세요. 그런일쯤이야."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"217"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"고맙네 고마워. 팔마스 가문의 영광이 함께하기를."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"쉽게말해 시체에서 옷을 벗겨오라는 것인가요? 그런일은 꺼림직 하군요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"어허, 도둑맞은 물건을 찾아오랬지. 누가 도둑질을 하라고 했나.허허"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"전쟁 보급품 회수 - 갑옷[반복]"
			Desc2	=	"고맙네 고마워. 팔마스 가문의 영광이 함께 하기를."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20291"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"5"
					Exp	=	"4936"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"217"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
