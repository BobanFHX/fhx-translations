<Root>
{
	<Quest>
	{
		Desc	=	"Deliver the letter to Luke Savage"
		GiveUp	=	"1"
		Id	=	"2"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Ardent love"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh what should I do...? I am very sorry to bother you, but could you do me a favor? I think I have fallen in love, but I'm not sure if he feels the same way. I came to Rog to tell him, but I was too scared. 





  I know it's rude to ask, but would you deliver my love letter to Luke Savage, the weapon merchant? He is standing right over there."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20202"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"1;"
					}
				}
				<Branch0>
				{
					Desc	=	"I was on my way to the weapon merchant anyway."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"3"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Thank you. I can't do anything until I get a response from him."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, I do not meddle with other people's affairs."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah, you are a heartless person!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Ardent love"
			Desc2	=	"What? Lilia? She sends me this? That's unusual, huh huh. Let me see. Hmm... Huh huh. Looks like she is getting more shy as she gets older, huh huh. I am sorry that I keep laughing, but I am happy. 





  Thank you for helping out with these trifling matters. Please take this, it's a gift from my heart."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"3"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20000"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"7"
					Exp	=	"18"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"3"
				}
			}
		}
	}
}
