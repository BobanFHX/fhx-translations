<Root>
{
	<Quest>
	{
		Desc	=	"기형 야만 트롤전사를 처치하고 가죽 5개를 구해오시오."
		GiveUp	=	"1"
		Id	=	"319"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"바람막이 대작전"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"떠돌이 모래골렘을 처치하면서 바람이 좀 잠잠해지는 듯 했지만, 어느새 다시 바람이 매섭게 불어오는군요. 죽이면 다시 살아나는 골렘들을 처치만 해서는 해결이 안되니, 아에 바람을 막을 만한 물건이 있으면 좋겠습니다. 사막에 나가보시면 트롤놈들이 덩치가 커서 그 가죽을 벗기면 넉넉하게 바람막이로 쓸 수 있을듯 한데.. 아참.. 기형 야만 트롤전사라는 놈이 특히 트롤특유의 악취가 안난다던데.. 그놈들 가죽을 좀 구해다 주시겠습니까? 5개 정도면 집안 곳곳을 막고도 충분할 것 같은데.. 부탁드립니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20301"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"49"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"318;"
					}
				}
				<Branch0>
				{
					Desc	=	"바람을 막아보는것도 좋은 방법이죠. 다녀오겠습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"고맙습니다. 에취..에취.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"238"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"바람을 막는다고 바람이 불지 않나요? 괜한 고생 시키지 마세요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"아이고 나죽네...에취 에취"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"바람막이 대작전"
			Desc2	=	"트롤가죽은 질기고 튼튼한데다 매끈매끈해서 좋군요. 맘에 들어요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20301"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"17137"
					Fame	=	"0"
					GP	=	"21"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"238"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
