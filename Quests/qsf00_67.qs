<Root>
{
	<Quest>
	{
		Desc	=	"You must meet Karon Rockez in Rog Castle and pass on official documentation of co-operation."
		GiveUp	=	"1"
		Id	=	"67"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Favour for Paishul"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Sorry, but I need to travel far. It looks like you are a Holy Knight that does not belong to any temple. It doesn't matter where you go, it would be helpful for you to belong to a temple. I suggest you should visit many different temples. 





 When you go near Rog Castle gate you will see a Dajamaroo worshiper. He is a communicator between Dajamaroo and Arketlev worshippers. Meet with him and give this document of co-operation. 





 Right now it's only a minor problem, but it can shake Rog later on. Then I'll leave this favour to you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20224"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						Class	=	"16"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"66;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes, I'll be back."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"61"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It's not too long a journey, but it might get boring."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I still have things to do here."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm... I think you are not quite ready to be a Paladin."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Favour for Paishul"
			Desc2	=	"Holy Paladin, what can I do for you? Really? 

 This is from Paishul? 





 Ah, I understand this document. Thank you for this."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"61"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20211"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"26"
					Exp	=	"147"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"61"
				}
			}
		}
	}
}
