<Root>
{
	<Quest>
	{
		Desc	=	"Kill 10 Striped Spiders for Norah Chamberlain."
		GiveUp	=	"1"
		Id	=	"14"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Striped Spider"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Man, I thought I was in big trouble this time. As I was travelling through Belziev's Ruins, I felt something creeping up on me... As I turned around I almost had a heart attack. Two disgusting, hairless spiders were crawling right next to me! Oh, but they're not just any spiders, they were the size of a small child! I was so scared I fainted and ruined my brand-new robe...





 I need to go home but I'm too afraid to go anywhere near the ruins... Could you please get rid of those spiders for me?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"3"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20031"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, I'll be right back."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"55"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ha ha ha! You are such a good person, thank you very much!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I think i'll leave it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hah! If you don't want to go, then don't go."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Striped Spider"
			Desc2	=	"Did you take care of those disgusting spiders? Thank you, you might have saved me another ruined robe... Take this. Someone gave it to me, but I don't need it. You might like it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20031"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"35"
					Exp	=	"195"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"157000"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
