<Root>
{
	<Quest>
	{
		Desc	=	"민병대장 로엔 하이스를 찾아가십시요."
		GiveUp	=	"1"
		Id	=	"286"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"민병대장 로엔 하이스"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"팔마스 가문이 괴멸당한 이후로 마물들과 탈주병들이 출몰하는 팔마스는 치안상태가 엉망입니다. 

몇일전에는 켈러스씨 둘째딸 깍쟁이 수잔이 마물에게 쫒기고 있는데도 어느 누구하나 도와주는 사람이 없이 숨어서 보고만 있더군요. 저 역시도 마물들과 싸워서 물리칠 힘이 없기에 발을 동동 구르면서 지켜만 봐야했습죠. 소뭄을 듣자하니 로엔 하이스라는분이 민병대를 조직하고 도망중인 팔마스 도주자들과 마물들을 잡아들이고 있다고 하던데, 지원자가 부족하여 힘들어 하신다고 하더군요. 

언제쯤이면, 팔마스에 다시 평화가 찾아올지.. 걱정이군요. 혹시, 팔마스를 재건하는 대업에 동참하지 않으시겠습니까? 민병대장 로엔 하이스님은 팔마스 서북쪽 중앙에서 모종의 음모를 꾸미고 있는 팔마스 도주병들의 처리를 위해 출정중이십니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20292"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"34"
					}
				}
				<Branch0>
				{
					Desc	=	"팔마스 재건이라.. 도와드려야죠. 당연히"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"241"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"감사합니다. 정말 감사합니다. 팔마스의 영광이 함께 하시길.."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"팔마스 재건은 팔마스 시민이 알아서 하셔야죠. 그럼 바이바이"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"아.. 팔마스는 더 이상 희망이 없는 것인가…"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"민병대장 로엔 하이스"
			Desc2	=	"팔마스 도주자와 마물의 처리를 돕고 싶다고? 어서 오시게나, 우리 팔마스 민병대는 자네같은 젊은이의 도움이 절실히 필요하네. 팔마스의 재건은 그대의 손에 달려있네."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"241"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20293"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"1845"
					Fame	=	"0"
					GP	=	"9"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"241"
				}
			}
		}
	}
}
