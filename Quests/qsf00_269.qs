<Root>

{

	<Quest>

	{

		Desc	=	"Töte 1 Zwergork-Commander, 5 Zwergork-Schurken, 5 Zwergork-Boten und 5 Zwergork-Jäger im Ork-Lager im Nordwesten."

		GiveUp	=	"1"

		Id	=	"269"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Eine echte Ausbildung (Kleriker)"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Du bist besser als ich dachte! Allerdings, wenn Du ein echter Kleriker sein willst, musst Du eine Weltreise machen und kontinuierlich Siege bringen. Wenn Du unbedacht agierst, gefährdest Du Dich und Andere. 



Mit deinem Talent kannst du das Schlachtfeld beherrschen; schließe Dich Deinen Gefährten an und töte 1 Zwergork-Commander, 5 Zwergork-Schurken, 5 Zwergork-Boten und 5 Zwergork-Jäger. Bringe die nötigen Siege."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"4"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20288"

					}

					<CondNode1>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"50"

					}

					<CondNode2>

					{

						Class	=	"128"

						CondNodeType	=	"20002"

					}

					<CondNode3>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"268;"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich werde siegen."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Dieser Sieg wird Dich stärker machen."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10000"

							KillCnt0	=	"1"

							KillCnt1	=	"5"

							KillCnt2	=	"5"

							KillCnt3	=	"5"

							NPCIdx0	=	"1465"

							NPCIdx1	=	"1470"

							NPCIdx2	=	"1455"

							NPCIdx3	=	"1420"

							SvrEventType	=	"0"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Im Augenblick nicht."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Denk an die Sicherheit bevor Du Dich in die Schlacht begibst."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Eine echte Ausbildung (Kleriker)"

			Desc2	=	"Gratuliere, jetzt bist Du ein echter Kleriker."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20288"

				}

				<CondNode1>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"0"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"95"

					Exp	=	"29314"

					Fame	=	"0"

					GP	=	"6"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"169017"

					SItem1	=	"169018"

					SItem2	=	"169019"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

