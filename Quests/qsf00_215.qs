<Root>
{
	<Quest>
	{
		Desc	=	"Bringe die Medizin zu Grace Agatha."
		GiveUp	=	"1"
		Id	=	"215"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Die Herzmedizin"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hm... hier ist die Medizin. Ich weiß nicht ob sie es trinkt, aber ich hoffe es hilft ihr.



Viel Glück!"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20278"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"214;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"34"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich bin dir dankbar."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Nichts zu danken."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"182"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich werde es später machen."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du willst es später machen? Was meinst Du?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Die Herzmedizin"
			Desc2	=	"Ist das wirklich die Medizin? Ah.. meine Mutter kann jetzt wieder gesund werden. Vielen Dank."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"182"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20277"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"45"
					Exp	=	"1955"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"101038"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"182"
				}
			}
		}
	}
}
