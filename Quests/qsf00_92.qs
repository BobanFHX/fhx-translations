<Root>
{
	<Quest>
	{
		Desc	=	"Collect 7 wings of Small Bloodsucking Bats at Fog Graveyard."
		GiveUp	=	"1"
		Id	=	"92"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Finding Antidote"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Stop!  How dare you murder Rog¡¯s guardian?  Donny, hey Donny are you alright? Who are you? What? You were asked a favour? Don't lie to me. 





 Your penalty is death. Hmm, what? You are going to do what you can to save him? After that you want me to forgive you? Are you insane? What makes you think I can trust you?  Don¡¯t move ~sniff~ wow this is oblivious well water? Man it¡¯s big trouble 





 Time I¡¯m not saying I trust you. Go get 7 bloodsucking bat¡¯s wings. This is used as an antidote when you are bitten by an oblivious spider. "
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20191"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"91;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll come right back."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"7"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"83"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"There is only one way out of here.  You have to come back here anyway.  If what you are saying is true, you had better go and get them quickly."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am your enemy."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I knew it.  You are oofh has poison gotten into me? My strength!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Finding Antidote"
			Desc2	=	"You, you got it? Oh, my breath, my breath - I need it first ~gulp gulp~ ha ha."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20191"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"61"
					Exp	=	"1217"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"7"
					ItemIdx	=	"83"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
