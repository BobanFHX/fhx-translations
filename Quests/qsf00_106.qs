<Root>
{
	<Quest>
	{
		Desc	=	"You should find Tom Valency from the ground of the dead"
		GiveUp	=	"1"
		Id	=	"106"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"A Temple Trainee"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Since it is an urgent matter, we should at lease move our temple.  Dear paladin, if you want to find what you should do, look for a temple trainer.  It will be the fastest way. 



 A temple trainer is some one who has discipline and aims for the ultimate goal.  The nearest of the temple trainers is at the ground of the dead.  Find Tom Klency who is a temple trainer who comforts the dead and ask him what a paladin has to do.  This is my recommendation."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20006"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"12"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"65;"
					}
					<CondNode3>
					{
						Class	=	"16"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I’ll depart right now."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"95"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"...I believe you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am quitting."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"....."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"A Temple Trainee"
			Desc2	=	"K... you are?? Ah... you are a liberal paladin.  I am sorry.  I was attacked by an unexpected creature due to my carelessness.  I’ll get better."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20233"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"95"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"168"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"95"
				}
			}
		}
	}
}
