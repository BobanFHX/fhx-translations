<Root>

{

	<Quest>

	{

		Desc	=	"Töte 1 Zwergork-Commander, 5 Zwergork-Schurken, 5 Zwergork-Boten und 5 Zwergork-Jäger im Ork-Lager im Nordwesten."

		GiveUp	=	"1"

		Id	=	"261"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Ein echtes Training (Krieger)"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Du bist besser als ich dachte, aber wenn Du ein echter Krieger werden willst, musst Du eine Weltreise machen um Deinen Geist zu stärken. Wenn Du verantwortungslos handelst, wirst Du Dich und Deine Gefährten in Gefahr bringen. 



Ich werde Dir zeigen, wo Du mit Deinem Talent kämpfen kannst; Geh mit deinen Gefährten und töte 1 Zwergork-Commander, 5 Zwergork-Schurken, 5 Zwergork-Boten und 5 Zwergork-Jäger. Bringe mir den Sieg."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"4"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20288"

					}

					<CondNode1>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"50"

					}

					<CondNode2>

					{

						Class	=	"1"

						CondNodeType	=	"20002"

					}

					<CondNode3>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"260;"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich komme bald zurück."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Dieser Sieg wird Dich stärker machen."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10000"

							KillCnt0	=	"1"

							KillCnt1	=	"5"

							KillCnt2	=	"5"

							KillCnt3	=	"5"

							NPCIdx0	=	"1465"

							NPCIdx1	=	"1470"

							NPCIdx2	=	"1455"

							NPCIdx3	=	"1420"

							SvrEventType	=	"0"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Habe jetzt keine Zeit."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Bereite dich sorgfältig auf den Kampf vor."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Ein echtes Training (Krieger)"

			Desc2	=	"Gratuliere ... Du bist nun ein echter Krieger!"

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20288"

				}

				<CondNode1>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"0"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"95"

					Exp	=	"29314"

					Fame	=	"0"

					GP	=	"6"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"169017"

					SItem1	=	"169018"

					SItem2	=	"169019"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

