<Root>
{
	<Quest>
	{
		Desc	=	"Hunt Lycanthropes and 20 Lycanthrope Thieves east of Dust field."
		GiveUp	=	"1"
		Id	=	"175"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Belzev Convoy 5 (Lycanthrope)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I will not stand by and watch us be trampled. This makes me very angry and others feel the same way.  We made this without anyone's help, it was done by us alone and with a lot of effort.  I will not let these devils come into our land. 





  From now on you will find and bring your fellow Belzev convoy men. According to grandmother Joshua there should be about 20 Lycanthropes and Lycanthrope Thieves.  Don't let them get away."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20198"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"26"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"174;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I will send your anger to them."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"1"
							KillCnt1	=	"20"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"776"
							NPCIdx1	=	"755"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Please send them my anger."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It looks like you have to do this with help of soldiers."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You really think so?  I guess that would be wise."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Belzev Convoy 5 (Lycanthrope)"
			Desc2	=	"I see it was a difficult battle, but we managed to stop them.  Your help has revived our pride in our town.  Thank you very much."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20198"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"24"
					Exp	=	"3315"
					Fame	=	"0"
					GP	=	"2"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150008"
					SItem1	=	"152005"
					SItem2	=	"154004"
					SItem3	=	"156009"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
