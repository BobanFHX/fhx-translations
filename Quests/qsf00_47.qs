<Root>
{
	<Quest>
	{
		Desc	=	"Deliver Wiseman's bag to Aegis' Blanko"
		GiveUp	=	"1"
		Id	=	"47"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"God's shield"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I know you just got back from Aegis, but I must ask you to go back.  Blanko needs to know this truth.  Can you do this? 





 The Symbol of Rog Aegis is at stake, and somebody needs to stop it. Please hurry."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20222"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"46;"
					}
				}
				<Branch0>
				{
					Desc	=	"I can't sit here while all this is going on."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"44"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Right please hurry."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I need to do my work"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"(he is giving you the evil look) ......."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"God's shield"
			Desc2	=	"What? Is this true? This is curse from 50 years ago? Oh great, other guardians that I've sent also didn't return, something must be going on. 





  Anyway your deed was very  great. Thank you for finding out for me, you are a Hero of Rog."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"44"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20195"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"13"
					Exp	=	"128"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"44"
				}
			}
		}
	}
}
