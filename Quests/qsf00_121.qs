<Root>
{
	<Quest>
	{
		Desc	=	"Capture 9 corpse scorpions and 9 bloodsucking bats."
		GiveUp	=	"1"
		Id	=	"121"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Black Stone keeper's request"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I am Black Stone Cemetery keeper Gilron. Man, it has been about 20 years since I started to work here! I am a keeper of the corpses. 





 When those who were living around the fog cemetery became saturated, they started to come to black cemetery as a new living space. Lately there are corpse scorpions and small bloodsucking bats. I can take care of the small ones, but I don't know what to do with their leader. If you are interested would you like to work for me? 





 Bring me proof of your encounters with 9 bloodsucking bats and 9 corpse scorpions, and I'll pay! I am reluctant, but I'll pay you some money. I am old and I don't really want to move."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20236"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"16"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll try it; I am expecting to be rewarded handsomely."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"9"
							KillCnt1	=	"9"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"380"
							NPCIdx1	=	"385"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I can't pay you handsomely, but I can pay you fairly."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"There's nothing to see in the cemetery. Why should I go there?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you don't want to, don't do it. I don't usually ask people for help."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Black Stone keeper's request."
			Desc2	=	"Hmm...You really caught them? Can I trust you? I didn't see it, but I can see by those furs over your clothes that you caught them.  I am still skeptical though..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20236"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"66"
					Exp	=	"1510"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
