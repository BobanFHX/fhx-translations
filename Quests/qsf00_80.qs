<Root>
{
	<Quest>
	{
		Desc	=	"Deliver the letter to Samana Elder Sphiler at Horn Tiger Mountain range"
		GiveUp	=	"1"
		Id	=	"80"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Forest's Samana"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Young Samana I am Sphiler who is living quietly at Horn Tiger Mountain range. I was on my way to Rog and I visited Kiene. Well, I never thought I would see such a sight. 



 I can't believe that those quiet spirits can be cursed this way.  Hey young Samana, I don't know too many details. 





 I cannot wait to give my father a verbal message about this. Take this letter to my teacher and explain the situation. He will know what to do."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20229"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"79;"
					}
					<CondNode3>
					{
						Class	=	"512"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll look for him."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"73"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"For the protection of spirits!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Well, this isn't my way."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I wish you could just stop by there."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Forest's Samana"
			Desc2	=	"What? Letter from Shed? What is this? Is this true? How can this happen? This is a problem, a big problem."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20230"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"73"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"13"
					Exp	=	"128"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"73"
				}
			}
		}
	}
}
