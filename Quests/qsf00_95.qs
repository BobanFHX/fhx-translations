<Root>
{
	<Quest>
	{
		Desc	=	"Deliver David Shimal¡¯s letter to leader James Madio at Black Bridge."
		GiveUp	=	"1"
		Id	=	"95"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Holy Ground¡¯s Guardian Captain"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Fine let¡¯s just say you are telling the truth. That necromancer raised the zombies. 



 To investigate this I need a witness party. Then you need to go to the Black Bridge and tell our leader James Madio about this. 



 This is a way to even out the bad things you have done."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20191"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"94;"
					}
				}
				<Branch0>
				{
					Desc	=	"If this could be my way to be forgiven, then Ill do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"86"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hey go - hurry."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I believe that I already paid for my sins."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hah do you really need me to put you in jail to get your attention?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Holy Ground's Guardian Captain"
			Desc2	=	"What? What you are telling me is true? You mean dead people are walking around? 



 And you seen it with your own eyes? Hmm Shimal¡¯s letter won¡¯t be a lie. And that necromancer - I wonder if he is related to the Iegees letter? Hmm this will be difficult."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"86"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"220"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"86"
				}
			}
		}
	}
}
