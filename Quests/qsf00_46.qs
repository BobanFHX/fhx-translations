<Root>
{
	<Quest>
	{
		Desc	=	"You must find Poisonous plant Kanene at mirror lake"
		GiveUp	=	"1"
		Id	=	"46"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Dirty laboratory"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"This sample you brought is started by a cain the black wizard who turned this place into a blood bath 50 years ago.  There is a power in the cure itself, but with even smallest amount can contaminate a large area. 





  Since the guardian of the lake went to check out the island, I need to ask you to go and get me some sample of any evidence.  It is called Kanene plant in order to use the cure this is most important ingredient.  This plant has no other use other than to use it for the curse. 





   If anyone is collecting them, that person must be putting on a curse.  Only thing that you need to remember is that you can only put them in a jar made with black mud, this will help the plant to stay fresh for long time."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20222"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"45;"
					}
				}
				<Branch0>
				{
					Desc	=	"I understand"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"43"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Please make sure no foolish person will repeat the horriable mistake again."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I need to attend to something else for a while."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you forgetting how important this job is?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Dirty labatory"
			Desc2	=	"Hmm, for sure this is Kanene.  I thought it died out 50 years ago.  Somebody is really trying to put on a curse.  This is no small matter,does this mean that Cain's prediction is coming true?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20222"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"51"
					Exp	=	"738"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"43"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
