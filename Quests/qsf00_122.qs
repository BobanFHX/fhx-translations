<Root>
{
	<Quest>
	{
		Desc	=	"Hunt 5 zombie dogs and 8 walking corpses."
		GiveUp	=	"1"
		Id	=	"122"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Death Ruler [Knight who is worshiping a god]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Dead man's forest was once known throughout the world as one of the beautiful wonders of the lands. 50 years ago, Cain's curse caused this very place to become not more than a land of death. Now this place has become a place where nobody can live. 





 Everyone avoids this place, and it has become one of the most depressing points of interest in the region . I come here to comfort the dead, and hope that this will not happen again.  Do you think it's strange to tell this story to a total stranger? Since you are a soldier, one day we may be either friend or a foe. 





 This may be the first time meeting each other, but I have a feeling that we are going to see more of each other in the future.  I am Jarold, the Daijaru knight who is in charge of the 3rd platoon of Knights. I introduce myself to you because I need your help. As usual I was walking around to comfort the dead wehen I saw shadowy creatures lurking about.  About 20 of them were in a group, then all of the sudden, they split into three groups! One group went to the Fog Cemetery, another to the Black Stone Cemetery. The last group went to the dead man’s forest. I had to choose one to follow, for I am only one person. I followed the group that went to the Black Stone Cemetery. They were summoning the dead, so I thought it best to stop them. I caught 7 of them, while 3 of them got away. 





 I guess they found out I was chasing them, they hid themselves pretty well. One day all of a sudden, undead started to appear. I assume they protected the black magician who yeilds the power to summon the undead. I still need to investigate it while I continue to be rid of them. These undead are really bothering me, so can you assist with their disposal while I further investigate? 





 Can you get rid of 5 zombie dogs and 8 walking corpses? They keep following me..."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20239"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"17"
					}
				}
				<Branch0>
				{
					Desc	=	"Of course I'll help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"5"
							KillCnt1	=	"8"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"405"
							NPCIdx1	=	"415"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You are one great guy, we will be seeing each other again later."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, but I need to do some other work."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I understand, then be on your way."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Death Ruler [Knight who worships a god]"
			Desc2	=	"Hmm....thanks to you I was able to finish my investigation. I was unable to take care of the problem, however."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20239"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"69"
					Exp	=	"1670"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
