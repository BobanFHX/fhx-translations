<Root>
{
	<Quest>
	{
		Desc	=	"Bringe 10 Ketten von den Felsengoblins zu Bastian Remun."
		GiveUp	=	"1"
		Id	=	"248"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Wahre Qual"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Bitte hilf mir. Ich habe aus Gier einen Kopfgeldauftrag angenommen, dabei weiß ich nicht mal wie man Felsengoblins tötet. Ich bin schlecht ausgestattet, aber ich bin so fürchterlich hungrig und brauche Geld für etwas zu Essen.
			
			Ich kenne die Felsengoblins nicht. Bitte bringe mir 10 Ketten die die Felsengoblins tragen."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20286"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"49"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde Dir helfen."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"204"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Danke, ich danke Dir vielmals."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich habe keine Zeit"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Da kann man nichts machen.. Ich werde jemand Anderes fragen müssen."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Wahre Qual"
			Desc2	=	"Super, vielen Dank das Du mir die Ketten gebracht hast. Hier ich habe eine kleine Belohnung für Dich."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20286"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"81"
					Exp	=	"16006"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"204"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
