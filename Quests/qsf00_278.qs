<Root>

{

	<Quest>

	{

		Desc	=	"Töte Wilder Trollkrieger und bringe 10 Zähne zu Tanja Buket."

		GiveUp	=	"1"

		Id	=	"278"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Der Wunsch des Sieges"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Ich kann das nicht ... ah ... komm rein ... ich war so abwesend, dass ich gar nicht merkte das Du da bist. Vor ein paar Tagen traf ich Gaby Tatalsul und sie zeigte mir eine Rüstung. Sie fragte mich, ob ich es versuchen möchte die Trollkrieger damit anzugreifen.

			

			Ich nahm mein Schwert und machte mich auf dem Weg. Das Ende vom Lied war, dass die Rüstung nicht einen Kratzer bekam, aber mein Schwert zerbrach. Ich war schockiert und entschlossen eine Waffe herzustellen die diese Rüstung zerstören kann und ich hatte Erfolg. Diese spezielle Waffe benötigte seltenes Material um sie herzustellen. 

			

			Eine der Komponenten für diese spezielle Waffe ist der Zahn eines Wilder Trollkriegers, doch dieser ist sehr schwer zu bekommen.



Deswegen biete ich dir an, wenn Du mir 10 Zähne besorgst, werde ich eine spezielle Waffe für Dich machen."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20023"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"277;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"52"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich werde sie dir besorgen."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"10000"

							ItemCnt0	=	"10"

							ItemCnt1	=	"0"

							ItemCnt2	=	"0"

							ItemCnt3	=	"0"

							ItemIdx0	=	"213"

							ItemIdx1	=	"-1"

							ItemIdx2	=	"-1"

							ItemIdx3	=	"-1"

							SvrEventType	=	"1"

						}

						<YActNode1>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Vielen Dank. Du wirst der erste Besitzer dieser speziellen Waffe sein."

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich hole sie später."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Und ich dachte du bist nett, aber da habe ich mich wohl geirrt."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Der Wunsch des Sieges"

			Desc2	=	"Jetzt kann ich die Waffen stärker machen als Gaby Tatalsul's Rüstungen."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"3"

				<CondNode0>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"1"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20023"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"47"

					Exp	=	"19538"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"169020"

					SItem1	=	"169021"

					SItem2	=	"169022"

					SItem3	=	"169023"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"10"

					ItemIdx	=	"213"

				}

				<YActNode2>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

