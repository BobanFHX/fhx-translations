<Root>
{
	<Quest>
	{
		Desc	=	"To undo the curse you must collect 7 slime cores"
		GiveUp	=	"1"
		Id	=	"86"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Curse of the Slime"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I know that you brought peace to a few of them, but I want to drive all of them out. So I found out a spell that can drive all of them away. To use this spell there has to be one important ingredient. Spirits are limited and can뭪 use their own strength, so I can뭪 catch those slimes. You need to do a favour for me. 





  Can you catch 7 slimes and then take their cores and bring them to me? I would need 12 cores then I can use it to make sure new slime won뭪 be vicious."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20226"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"13"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"85;"
					}
				}
				<Branch0>
				{
					Desc	=	"I will collect those cores for you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"7"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"77"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I wish you the protection of the spirit."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am too busy to help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Man help me at least a little."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Cures of the Slime"
			Desc2	=	"Ah these are slime cores. With these the water spirits will no longer be vicious. 





  Pray so that all of the water spirits will be cured."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20226"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"1085"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"7"
					ItemIdx	=	"77"
				}
			}
		}
	}
}
