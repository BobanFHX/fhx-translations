<Root>
{
	<Quest>
	{
		Desc	=	"Capture skull warrior and exchange Emily's short sword for the medicine plant she offers as reward."
		GiveUp	=	"1"
		Id	=	"128"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Short Sword for Medicine plant"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Sniff, Sniff...my short sword! Sir, can you help me find my short sword? Please, it was a gift from my lover. It is now missing. 





 Without it I am too afraid to walk around here. It comforts me. All of a sudden I was surprised by the skulls and I left it behind while I was fleeing. 





 When I looked back a skull warrior was weilding it. I beg of you, help me find the sword. I will lose all my hope if I don't have it back."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20240"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"17"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, of course I'll help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I beg you! I really need that sword!"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"111"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Your situation is bad, but..."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Yeah..really...You are too much! Don't you feel bad for me!?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Short Sword for Medicine plant."
			Desc2	=	"Ah! Parow! Finally you return to me again. 





 I was worried sick! I am sorry that I left you there. Was it difficult being with someone else?!



"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20240"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"69"
					Exp	=	"1670"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
