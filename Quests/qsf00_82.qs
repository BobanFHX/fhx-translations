<Root>
{
	<Quest>
	{
		Desc	=	"Collect 5 Giant Silver Wolf teeth at the north side of Blue Gorge"
		GiveUp	=	"1"
		Id	=	"82"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Giant Silver Wolf's Teeth"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ah what to do? At this rate I can't make anymore gloves. Man it's a big problem. Hey, hey - if you have nothing to do, can you help me? You are a soldier right? Can you do me a favour? 





 I was in the process of making hard leather gloves, but the final ingredients, Giant Silver Wolf's teeth, are gone.  I wanted to finish it by today... man... can you go to the north side of Blue Gorge and get 5 Giant Silver Wolf's teeth?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20042"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"12"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure I'll do it for you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"75"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Thank you. I need it right away."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't want to do this now."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh then move out of the way so I can find someone else to do the job."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Giant Silver Wolf's Teeth"
			Desc2	=	"Thank you very much. Thanks to you my work has been reduced. I put a lot of effort into this item."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20042"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"56"
					Exp	=	"961"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"75"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
