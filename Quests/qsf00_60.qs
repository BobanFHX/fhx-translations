<Root>
{
	<Quest>
	{
		Desc	=	"You must deliver the secret document to security commander James Madio in Deadman's land."
		GiveUp	=	"1"
		Id	=	"60"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Into the grim fog"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm...I am interest in you. Anyway, we need to do something right now.  I've been hearing that corpse were walking around at the Deadman's Land. 





  So I sent my messengers to get further information, but they are now all missing. I am very curious to what happened. This is my 4th letter, anyway the path north of Blue Gorge will lead to Deadman's Land's entrance. 





  If you go past it James Madio the security commander will be there. Can you give this letter to him?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20196"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						Class	=	"2"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"59;"
					}
				}
				<Branch0>
				{
					Desc	=	"The name Deadman's Land gives me the shivers"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"55"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It maybe little bit too rough for you right now."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I'll go later"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you testing me?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Into the grim fog"
			Desc2	=	"Who are you? hmm....a human? Phew....my heart is still beating. Who are you? What this is? Aegis commander's letter. I understand, but I can't believe he sent this novice."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"55"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"26"
					Exp	=	"128"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"55"
				}
			}
		}
	}
}
