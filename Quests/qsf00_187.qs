<Root>
{
	<Quest>
	{
		Desc	=	"Find Luigi's real luggage"
		GiveUp	=	"1"
		Id	=	"187"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Miner's Luggage"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Since I can't find the real owner of this luggage, just leave it here and find my real bag for me. If I don't have it, I can't work. 



 I am begging you, please help me find it. Why would I beg you like this?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20252"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"186;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes, I understand I'll help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"3"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"160"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10003"
							ItemCnt	=	"1"
							ItemIdx	=	"159"
						}
						<YActNode2>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Yes, you are the greatest."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"What? Go there again? I can't go anymore."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Come on stop playing around and please help me find it."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Miner's luggage"
			Desc2	=	"That's my luggage, you finally found my real luggage,  Thank you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20252"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"87"
					Exp	=	"3072"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"160"
				}
			}
		}
	}
}
