<Root>
{
	<Quest>
	{
		Desc	=	"Collect 10 Giant Black Wolf's skin"
		GiveUp	=	"1"
		Id	=	"69"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Forest Ranger"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey! Thanks to you I just lost that wolf that I have been watching, and now he is gone. What? Are you an archer too? How can you call yourself an archer and yet you are so noisy. 





 I can't understand these young people.  They have no basic skills. Your future is plain to see. Anyway, thanks to you I just lost all my Grey Wolves - take responsibility for them. 



 Hurry and bring me 10 Giant Black Wolf's skins, I am already mad at you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20225"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"9"
					}
					<CondNode2>
					{
						Class	=	"4"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"Don't yell so much, I'm on my way."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"63"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Man I may be late for my appointment and it is making me nervous."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"You can't blame me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What? What? Are you trying to avoid your mistakes?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Forest Ranger"
			Desc2	=	"What! You brought it? At least you kept your word and didn't run away."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20225"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"49"
					Exp	=	"639"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"63"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
