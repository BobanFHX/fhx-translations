<Root>
{
	<Quest>
	{
		Desc	=	"Deliver to Aegis' elder knight, Thomas Ranker, a letter."
		GiveUp	=	"1"
		Id	=	"32"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Surprise attack warning"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"My attack means that the attacker could know of someone who is studying in the same field as me. He doesn't have the original like mine, but he probably has the hand written copy. 





 As long as people know that I have it, he won't be safe either.  I am going to write a letter to him and warn him about the surprise attack.  That person is a merchant who has a weapon store at the Aegis Fortress, and his name is Thomas Ranker.  Please give this letter to him."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20207"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"5"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"31;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll make sure he gets it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"29"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I'll leave this to you. I suggest you should hurry."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I think now is not a good time."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm just think of it has helping me and think about it again."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Surprise attack warning"
			Desc2	=	"Mmm..this is? Loren's letter. Huh? Someone is going to surprise attack me? 

 Which fool is going to attack me? Let them all come. I will crush them. 





 Hey! Thank you for coming here, thanks!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"29"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20007"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"60"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"157001"
					SItem1	=	"157002"
					SItem2	=	"161002"
					SItem3	=	"161003"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"29"
				}
			}
		}
	}
}
