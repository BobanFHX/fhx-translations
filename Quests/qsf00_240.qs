<Root>
{
	<Quest>
	{
		Desc	=	"Töte Schuppenechsen-Killer und bringe 10 Schuppen mit. "
		GiveUp	=	"1"
		Id	=	"240"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Merkwürdige Sammlung (3)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ah.. ich weiß ich sollte das nicht tun, aber da sind so viele verführerische Objekte die ich im stillen Sumpf sammeln möchte.
			Die glänzenden Schuppen der Schuppenechsen-Killer beherrscht all meine Gedanken. Hey, warum tötest Du nicht einen Schuppenechsen-Killer und bringe mir die Schuppen mit?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20284"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"48"
					}
				}
				<Branch0>
				{
					Desc	=	"Sicher, kein Problem."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"203"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du wolltest auch eine solche Schuppe haben, die sehen wunderschön aus."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Auf keinen Fall."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du verpasst das Schicksal seltene Objekte zu sehen."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Merkwürdige Sammlung (3)"
			Desc2	=	"Oh... das ist die Schuppe eines Schuppenechsen-Killer. Sie ist viel schöner als gewöhnliche Schuppen.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20284"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"75"
					Exp	=	"14920"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"203"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
