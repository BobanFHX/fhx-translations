<Root>
{
	<Quest>
	{
		Desc	=	"You must hunt 20 Lycanthrope Hunters."
		GiveUp	=	"1"
		Id	=	"176"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Dark Hunters"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey...hey...I am sorry but can you help me? Please...phant...! I'll tell you my situation later.  Lycanthrope hunters are hunting me right now. 





  Please kill 20 Lycanthrope hunters for me.  If you can take care of it I think it뭠l  buy me some time."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20257"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"26"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, I will help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"20"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"770"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Thank you then I'll leave it to you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I do not want to meddle with such a situation right now."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you willing to walk away from a man who is about to be killed?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Dark Hunters"
			Desc2	=	"Oh~wow~! Thank you. Phew~! I thought I was done for. 





  My name is Peter, and I work as the Information officer for Rog Yellow Castle.  Since the attack of The Devil뭩 Den our work has become much more dangerous.  I am in charge of Palmas and the Dust Gorge area.  I am supposed to gather information from these places.  I was gathering information and then I discovered something very important.  They have been hunting me ever since.  If it wasn't for you I don뭪 know what would have happened.  Unfortunately I lost the document containing the information at the Dust Orc Tribe camp."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20257"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"92"
					Exp	=	"4642"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
