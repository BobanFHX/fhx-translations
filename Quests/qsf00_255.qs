<Root>
{
	<Quest>
	{
		Desc	=	"Töte 20 Oger."
		GiveUp	=	"1"
		Id	=	"255"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Kopfgeldjagd (4)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Die öffentliche Sicherheit hier in im Land der Giganten ist sehr schlecht, seitdem wir keine Militärkaserne mehr haben. Die Oger rauben Reisende aus und greifen Bürger in Hadun an. Wärst Du daran interessiert, eine Belohnung für das töten von 20 Ogern zu bekommen?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20287"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"51"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde es versuchen."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Töte alle, damit die Reisenden wieder unbesorgt ihrer Wege gehen können."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"20"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1345"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Keine Lust"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Zu schade.. Da muss ich eben mit anderen Kopfgeldjägern sprechen."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Kopfgeldjagd (4)"
			Desc2	=	"Schon fertig? Du bist besser wie ich dachte."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20287"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"26"
					Exp	=	"18314"
					Fame	=	"0"
					GP	=	"2"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
