<Root>
{
	<Quest>
	{
		Desc	=	"Capture Kujo's offspring and bring their eyes to Elia Baskin."
		GiveUp	=	"1"
		Id	=	"17"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Wolf prince"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Let me tell you something about wolves. They are very tough and elusive animals that like to fool humans. The leader of the wolves is called Wolf Prince Kujo. 



  Did you hear about the rumour that Kujo's offspring has been sighted near Rog? I think it was near Blue Eye Lake. The lake near Michael Ruin's sawmill. 



  Their eye are sought after by the nobles for their decorations. I also heard that Elia Baskin, the grocer, is looking for the eyes. He'll probably pay a hefty price..."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20080"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"4"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm, thank you for the information."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Since I told you the information, do you think I can cut in on the action?"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"14"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Well, I'm not interested"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I think I will give it a try myself."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Wolf prince"
			Desc2	=	"What is this? It's a young Kujo's eye? This is very rare. Where did you get it? This must have been very difficult to get. Hee Hee, if you are showing it to me, that means you are willing to sell it? I'll give you good price for it. Hee hee."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20065"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"94"
					Exp	=	"270"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"152001"
					SItem1	=	"158000"
					SItem2	=	"165001"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"14"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
