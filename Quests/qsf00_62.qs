<Root>
{
	<Quest>
	{
		Desc	=	"Bring Killer Wolf's heart to Dorika Hood."
		GiveUp	=	"1"
		Id	=	"62"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Warrior's Worth"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"You are useful, that is why I'm telling you this. What do think is the most important thing about being a warrior? As you know warriors are all about power hitting. If you want to show mighty attack you must show no fear. It's important to defeat the enemy's spirit. 





  I want to test your bravery.  If you go to the north side of Blue Gorge there is a wolf. I've heard that this Killer Wolf's spirit is something else. If you catch him and bring his heart then we'll talk further."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20006"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						Class	=	"1"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"61;"
					}
				}
				<Branch0>
				{
					Desc	=	"You called that a quest?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"57"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Let me see if you're worthy."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I'll do it later."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What kind of warrior is it that doesn't have pride?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Warrior's Worth"
			Desc2	=	"Is this the heart of the Killer Wolf? Let me see, hee hee it's his heart alright. This wolf's heart is different from others because it bleeds black blood."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20006"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"51"
					Exp	=	"738"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"162003"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"57"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
