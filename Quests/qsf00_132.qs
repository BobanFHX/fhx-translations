<Root>
{
	<Quest>
	{
		Desc	=	"Capture 9 Fog cemetery grave robbers."
		GiveUp	=	"1"
		Id	=	"132"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Illegal Intruder"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey, where are you going? Stop, who are you? You can't go in there without permission. What? You are a soldier? Hmm, you look like a soldier, but then again anyone can look like a King nowadays. 



 Sorry, as long as you are not a grave robber. Lately there has been so much chaos, and due to it, grave robbers are being more of a nuisance. I wish I could catch them and put them in Rog's deep dark dungeon. It just makes me mad when I think about it. 





 If you want to enter there is one condition. You have to capture 9 grave robbers. This is the condition if you want to get in. If you finish the job, I'll show you my small appreciation."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20191"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"18"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm...I'll do it then."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"9"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"460"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"These robbers are such a nuisance in this busy times."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It's your problem."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You! you..are you finished? Are you saying you don't want to enter?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Illegal Intruder"
			Desc2	=	"Hmm... You've done a great job. Good job! People usually make excuses such as; 





 no strength, headache etc."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20191"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"74"
					Exp	=	"1839"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
