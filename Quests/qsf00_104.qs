<Root>
{
	<Quest>
	{
		Desc	=	"Hunt the Lord of Skeletons"
		GiveUp	=	"1"
		Id	=	"104"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] A Brawler in the Black-Stone Graveyard"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm... Good... I thought you were just a rookie.  Now I think you are alright.  I am sure there are reasons that the general sent you here, and I am sure he wants you to understand the ultimate goal of being a knight.  As you know, the key word for knights is sacrifice.  The knights¡¯ ultimate power exhibits only when you become a part of all our force¡¯s attack.  There is our enemy. 





 I think there is a very violent enemy making a disturbance at Silent woods south at this time.  I don¡¯t think you can deal with him by yourself.  You should recruit colleagues, and show your ability with them.  Put him under your control.  People there call him the Lord of Skeletons.  Show me your ability."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"5"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"103;"
					}
					<CondNode3>
					{
						Class	=	"2"
						CondNodeType	=	"20002"
					}
					<CondNode4>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes.  I¡¯ll do my best."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"1"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"631"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"A knight can show his ability only when he is with his colleagues."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I¡¯ll come back after I finish up other tasks."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you giving up after what you have done so far?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"A Brawler in the Black-Stone Graveyard"
			Desc2	=	"Mm Now do you understand which way you should take?  Don¡¯t forget that the knight¡¯s ultimate power can be shown only when you put your power together."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"1510"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150005"
					SItem1	=	"158002"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
