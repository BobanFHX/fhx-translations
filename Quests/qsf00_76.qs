<Root>
{
	<Quest>
	{
		Desc	=	"Give healing medicine to Hert Brian who is east of Mirror Lake."
		GiveUp	=	"1"
		Id	=	"76"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Injured in the Forest"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hello traveler, Dajamaroo's blessings will be with you. You are a priest. You are a free spirit priest. It is difficult for a single priest to journey alone. 



 Free priest, we must always take care of others and put our effort into it.  When you understand your God's character then you'll know more details. Meeting you here is the will of Dajamaroo. 



 I was on my way to help injured people at the east of Mirror Lake. I received a letter from elders by pigeon.  Can you give these healing medicines to those who are injured at the east of Mirror Lake? I think this much healing will help them walk."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20228"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"11"
					}
					<CondNode2>
					{
						Class	=	"128"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll follow God's will."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"70"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I can go without worry. I pray that blessings will be upon you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You have a sense of duty which you are not ready for."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Injured in the Forest"
			Desc2	=	"Arrg who are you? Oh... what is this? Thank you. Now I can live again."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"70"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20227"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"13"
					Exp	=	"147"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"70"
				}
			}
		}
	}
}
