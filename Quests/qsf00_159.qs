<Root>
{
	<Quest>
	{
		Desc	=	"Capture 3 Leader of Ahgol Goblin warrior and Ahgol Goblin's shaman king."
		GiveUp	=	"1"
		Id	=	"159"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] God's Knight 4 (Dashmod)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Besides this it looks like there has been lot going on. Especially it looks like Ahgol Goblin who is like monster

 that is attacking people. 



 Lot of the merchants were being hassled by them.  I would like to ask you for a favor. 





 At the near the top of the  Ahgol mountain you can see Ahgol Goblin warriors and surrounded by Ahgol Goblin shaman.  



 Do you think you can capture 3 Aghol Goblin warriors and Ahgol Goblin Shaman?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20247"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"23"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"158;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"For Midland"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah that warrior will not easy to capture."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"3"
							KillCnt1	=	"1"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"660"
							NPCIdx1	=	"655"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"What is the best army doing?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ha ha ha...I have no excuse"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"God's knight 4 (Dashmod)"
			Desc2	=	"Hmm I am troubled by the fact that I was asking too much to a person I hardly know.  Anyway I am glad that you are willing to do it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20247"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"11"
					Exp	=	"2840"
					Fame	=	"0"
					GP	=	"2"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"170004"
					SItem1	=	"170005"
					SItem2	=	"170006"
					SItem3	=	"170007"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
