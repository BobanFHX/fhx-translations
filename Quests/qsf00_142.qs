<Root>
{
	<Quest>
	{
		Desc	=	"Security officer of Midland Roy, you must file a report a strange occurrences at the Midland's deadman's land."
		GiveUp	=	"1"
		Id	=	"142"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Report from Midland"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"You must of heard about the few occurrences that's gone on at deadman's land. Usually nog a lot happens at the deadman's land.  I have been working here for about 8 years and seen a lot, but I never seen so many things happen at once. 





 I also been hearing bad rumors from many different places.  I wish all these happenings would report back to Midland's Roy and the count.  I believe that we would need recruits from Midland.  Realistically speaking, I know they have many reasons for not sending, but if there's big thing happens, Midland will not survive.  Can you pass on this message?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"18"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes sir, of course."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I have much confidence in you."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"122"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry, but I have work to do."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Weren't you going to go to Midland?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Midland report"
			Desc2	=	"This place isn't easy place to get to.  Who are you?  With whose permission did you come? Huh?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"122"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20197"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"36"
					Exp	=	"366"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"122"
				}
			}
		}
	}
}
