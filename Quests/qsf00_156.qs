<Root>
{
	<Quest>
	{
		Desc	=	"Tell the count of Rog Midland that Dashmod's knight has arrived."
		GiveUp	=	"1"
		Id	=	"156"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"God's knight 1 (Dashmod)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm...Devil's den. Even with tight security Devil's den has finally appeared after 1000 years of promise, Dashmod, Dashmod. 





  Huh? What is wrong? Ah~you have heard my private cry.  I am Dashmod knight who worships Dashmod.  My story must have been strange to you. 





  Have you heard history of attack on Edios region.  1000 years ago they were evil that came and took over without mercy.  Even though 1000 years ago with combined strength we judge them, but yet they reappeared once again.  Only thing we can do is to defend with all our strength and wait for help from our god. 



 Thank you for listening to my story.  I was frustrated and I wanted to vent on someone.  Ah, we must tell the count of Midland that we have arrived.  I am sorry, but can you tell guardian Roy Midland? I think I need to know what has been going on in this town, and I do not have time to meet with him."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20247"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"22"
					}
				}
				<Branch0>
				{
					Desc	=	"I understand. I will go instead."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I know I am bothering you, but I ask you for this favor. Blessings of Dashmod are upon you."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"134"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I do not want to interfere."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It's too bad.  I guess I have to look for another person."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"God's knight 1 (Dashmod)"
			Desc2	=	"I see, Dashmod's knight has arrived.  I hope that this Midland will not become battle field. 



  If any harm goes to my people, then I will not forgive devils for it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20197"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"134"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"601"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"134"
				}
			}
		}
	}
}
