<Root>
{
	<Quest>
	{
		Desc	=	"Go to Blue Gorge and capture a Grizzly and bring the head to Branko."
		GiveUp	=	"1"
		Id	=	"84"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Gorge King"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"10 years ago in Blue Gorge there used to be a giant king living there.  Anyone who made him angry would not survive. Lots of the hunters had a difficult time hunting him. 



 I tried to hunt him 5 years ago. Not only was he strong, he was also smart. He was so brilliant that he was better than humans. When I think about the wound that he left me, it still gives me pain. Iegees tried to put up a reward for his capture, but it didnt do any good. 



 Grizzly is a big bear. He is a monster. If you catch it you have to take the head to the guard commander. It wont be easy though."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20008"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"12"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"This might be very interesting."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"76"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What? You¡¯re going to catch it? You are one impatient guy."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I¡¯ll  pretend I didn¡¯t hear that."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I guess it would be very difficult."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Gorge King"
			Desc2	=	"What is this bag with dripping blood? Did you catch a fugitive or something? What is this is that the legendary Grizzlys head? 



 Did you really catch it? Oh you took a gamble. Yeah you took a really dangerous gamble.  Still you caught it. You have great skills. Hee hee hee."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20008"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"212"
					Exp	=	"961"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"167002"
					SItem1	=	"167003"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"76"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
