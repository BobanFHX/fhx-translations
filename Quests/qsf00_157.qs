<Root>
{
	<Quest>
	{
		Desc	=	"At sunrise lake we must tell Count Roy's father that Dashmod's knight has arrived."
		GiveUp	=	"0"
		Id	=	"157"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"God's knight 2 (Dashmod)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Have you heard of the great hero Andre Midland?  Not only he is our best hero from Midland, but he is the best Paladin a holy knight in 300 years of history of Dashmod knight history. 





 That is your father Andre.  Now he gave up everything and simply enjoying fishing and enjoying rest of his life.  His pupil Phil Akeneil. Me? I didn’t have enough talent to follow my father's footsteps.  I guess this is the reality,  Anyway Phil is now here, and my father will be glad. 





   My father is middle of fishing at the middle of  small hill at sunrise lake.  Can you tell him that Phil is here?  I think he will be most glad."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20197"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"22"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"156;"
					}
				}
				<Branch0>
				{
					Desc	=	"I understood, I will tell him."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I will ask you then."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"135"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It doesn't intrigue me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm if you meet him it will be a great help to you."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"God's knight 2 (Dashmod)"
			Desc2	=	"Phil is here? Ho Ho, I thought he will never come near this place, but yet he is here. 





  Of course he had to come.  Yeah...I guess I have to go, I guess I have to see him, heh heh heh."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"135"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20265"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"84"
					Exp	=	"601"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"135"
				}
			}
		}
	}
}
