<Root>
{
	<Quest>
	{
		Desc	=	"You must go back to Blanko with an approval letter."
		GiveUp	=	"1"
		Id	=	"59"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Return to Blanko"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I had great feeling about you when I first saw you.  I like the way you presented your self. When I gave you this test I had a personal grudge.  





 Even if I give you this letter of approval Blanko is not an easy person. For now he will approve your basic knight skills, but if you continue with your strong will, you will get good results. My test is now over.  Take this and go to Blanko."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20196"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"58;"
					}
					<CondNode3>
					{
						Class	=	"2"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"Thank you, I will not forget your heart."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"54"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I will witness the birth of a new knight?"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I will forfit the way of the knight"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Gasp!!..............................."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"return to blanko"
			Desc2	=	"wow..busy, so busy..what? Back already? This is Kevin's approval letter. Ha ha...you got an easy approval from him. 



  He is a great knight but he has a soft heart."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"54"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20195"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"13"
					Exp	=	"128"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"54"
				}
			}
		}
	}
}
