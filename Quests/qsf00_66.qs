<Root>
{
	<Quest>
	{
		Desc	=	"Deliver Shelliban's Activity Report to Rod Paishul in Iegees."
		GiveUp	=	"1"
		Id	=	"66"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Goblin's Activity Report"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I am an Arketlev worshipper sent here to spy on monster's activities. Lately Paishul heard that the Tiger Forest Goblin tribe were getting much too aggressive so he sent me here to spy on them. 







 It was my mistake, I thought they weren't too aggressive and went on my patrol. They were much more aggressive than I expected.  I was attacked several times and I hid myself here. 





  I don't know if there's any relationship between this incident and the rumours which people have been saying. Now I have the medicine, so I should continue my research on Goblins. If you are on your way back, can you give this to Iegees Arketlev's Priest Rod Paishul?  Here is the report that I have so far."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20183"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"10"
					}
					<CondNode2>
					{
						Class	=	"16"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"65;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do it"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"60"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I think that their activity for a year shows me that it has nothing to do with recent incidents."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Why don't you rest and then continue?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you are going back please do this favour for me."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Goblin's Activity Report."
			Desc2	=	"What is this? Isn't this Shelliban's note? What? I didn't know he encountered them.  





 He is pretty smart, so he can handle it.  This is a sign that something big is about to happen."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"60"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20224"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"13"
					Exp	=	"128"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"60"
				}
			}
		}
	}
}
