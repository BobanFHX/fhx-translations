<Root>
{
	<Quest>
	{
		Desc	=	"You have to deliver the holy water to James Madio"
		GiveUp	=	"1"
		Id	=	"101"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Delivering the Holy Water"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Well... Since I¡¯ve gotten a gold coin  5 bottles will do it...  It¡¯s yours.  Whatever you do with it is none of my business, whether you deliver to the general at the Black Tower or not.  Hehehe...  If you bring more gold coins I¡¯ll consider showing you the recipe.  Go away!!!"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20236"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"100;"
					}
				}
				<Branch0>
				{
					Desc	=	"Okay.  You are such a miser."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"5"
							ItemIdx	=	"93"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Go away"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I can¡¯t leave with so little holy water."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh??  You are not taking the holy water??  Good for me!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Delivering the Holy Water"
			Desc2	=	"Um Did you get it?  Shoot... He is such a miser.  He just gave you 5 bottles of the holy water for that many gold coins?  I am just doing this because I don¡¯t have time to go back to the temple.  I will never go back to him again."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"5"
					ItemIdx	=	"93"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"16"
					Exp	=	"250"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"93"
				}
			}
		}
	}
}
