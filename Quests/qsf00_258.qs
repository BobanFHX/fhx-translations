<Root>

{

	<Quest>

	{

		Desc	=	"Töte den Zwergork-Schamanen und bring seinen Kopf zu Fabian Schneider."

		GiveUp	=	"1"

		Id	=	"258"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Ritterausbildung"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Du bist der Ritter, der in der vordersten Linie die Kameraden mit Deinem starken Körper beschützt. 



Ein Ritter zu werden ist nicht so einfach, Du benötigst viel Stamina, es wird einfacher mit der richtigen Ausbildung. Du hast scheinbar schon einiges an Ausbildung erfahren, aber noch nicht genug. 



Wenn Du eine Herausforderung suchst, bring den Zwergork-Schamanen zur Strecke und mir dann seinen Kopf."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20288"

					}

					<CondNode1>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"50"

					}

					<CondNode2>

					{

						Class	=	"2"

						CondNodeType	=	"20002"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich versuch es."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"10000"

							ItemCnt0	=	"1"

							ItemCnt1	=	"0"

							ItemCnt2	=	"0"

							ItemCnt3	=	"0"

							ItemIdx0	=	"207"

							ItemIdx1	=	"-1"

							ItemIdx2	=	"-1"

							ItemIdx3	=	"-1"

							SvrEventType	=	"1"

						}

						<YActNode1>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Ihn zu bekämpfen ist hart und gefährlich."

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich schaffe das nicht."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Jemand ohne gute Ausbildung wird nicht lange überleben."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Ritterausbildung"

			Desc2	=	"Ich kann mich auf mein Gefühl verlassen. Ich wusste, dass Du es schaffst."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"3"

				<CondNode0>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"1"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20288"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"13"

					Exp	=	"17137"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"1"

					ItemIdx	=	"207"

				}

				<YActNode2>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

