<Root>
{
	<Quest>
	{
		Desc	=	"Deliver James Madio's order to the Black Bridge guard."
		GiveUp	=	"1"
		Id	=	"78"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Black Bridge Guard"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Cleric, sorry to bother you, but would you do me this favour? North of Deadman's land you'll see Black Bridge. When you go there you'll find James Madio who is Deadman's Land's Guard Commander. 





 He is a special knight commander who I planted there. Can you give this order to him?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20195"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"12"
					}
					<CondNode2>
					{
						Class	=	"128"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"77;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do my best."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"72"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I pray that the hope of Arketlev will be with you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, I have another mission."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh? Then there's nothing you can do."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Black Bridge Guard"
			Desc2	=	"Who? Commander Blanko's order? Oh, maybe it's time to combine our strength. 



 On the other hand I wasn't looking forward to this moment. However we must give up our lives for the mission."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"75"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"28"
					Exp	=	"168"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"72"
				}
			}
		}
	}
}
