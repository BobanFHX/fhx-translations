<Root>
{
	<Quest>
	{
		Desc	=	"You must bring report from Dust Canton to Midland's Dashmod commander Phil Andre."
		GiveUp	=	"1"
		Id	=	"169"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Report to Midland"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm, all these strange activities from the Orc tribes and wolf men should be reported to our brethren.  Fortunately I've heard that he is in Midland. 





 This is a report of all the strange findings, can you give this to Phil Andre who is the commander of Dashmod in Midland? 



 

 To tell the truth he is from here and he can뭪 simply ignore all the happenings around here."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20198"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"25"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"168;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes, I'll try my best."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I like your attitude."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"146"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"To tell you the truth, it's a bit far."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Is there a problem with the distance? Are you too lazy?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Report to Midland"
			Desc2	=	"Situation is escalating in Dust gorge.  This is terrible. We can't just stay here in Midland.  Honestly though, I am tired of moving around."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20247"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"146"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"44"
					Exp	=	"866"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"146"
				}
			}
		}
	}
}
