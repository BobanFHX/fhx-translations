<Root>
{
	<Quest>
	{
		Desc	=	"난폭한 다크 슬라임을 10마리를 제거하라."
		GiveUp	=	"1"
		Id	=	"302"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"수습연금술사의 부탁4"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"용사님이 검붉은 시약을 구하시러 가신동안, 전 검은 연꽃을 캐러 갔었어요. 그런데. 검은연꽃을 발견은 했지만.. 근처에 서식하는 난폭한 다크슬라임 때문에 엄두가 나지 않더군요. 용사님.. 제발 도와주세요.

용사님이 난폭한 다크 슬라임 10마리정도만 제거해 주시면.. 그사이에 제가 검은연꽃을 채취해 올게요. 도와주실거죠? 헤헤"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20297"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"42"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"301;"
					}
				}
				<Branch0>
				{
					Desc	=	"힘들지만 어쩔수 없지.. 도와드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"와..용사님 멋쟁이!!"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1150"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"이젠 지쳤어요. 더 이상은 힘들어서.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"이제 곧 조제법을 완성하는데.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"수습연금술사의 부탁4"
			Desc2	=	"이렇게 빨리 구해오시다니.. 검붉은 시약 5병 ..맞네요. 바로 이거에요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20297"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"10858"
					Fame	=	"0"
					GP	=	"27"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
