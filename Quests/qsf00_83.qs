<Root>
{
	<Quest>
	{
		Desc	=	"Capture 5 Giant Brown Bears and 5 Giant Black Bears."
		GiveUp	=	"1"
		Id	=	"83"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Father's Revenge"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I am very sad. I don't know how to comfort my father's spirit. My father was a craftsman for over 30 years at Iegees.  He used to tan the leather which people brought to him. Lately he would complain about people not bringing in the high quality leather. He would say that he could get better leather if he caught them himself. One day he got into a fight with one hunter. 





  They had a bet, and so my father went to Blue Gorge. If I were there I would have stopped him. I didn't know what went on and I was just selling stuff here. I heard that he was attacked by a bear and was killed. They couldn't even find his body. I am very sad. I hate that hunter, but I hate that Giant Bear even more. Would you take revenge for me? Go to Blue Gorge and hunt 5 Giant Brown Bears and 5 Giant Black Bears."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"12"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20041"
					}
				}
				<Branch0>
				{
					Desc	=	"I will revenge your father."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"5"
							KillCnt1	=	"5"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"255"
							NPCIdx1	=	"260"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Sniff, sniff. I never got be a great son to my father."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It looks like it's not something I can do."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah I see."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Father's Revenge"
			Desc2	=	"Ah I heard the news. Some soldier wiped out the bears in the Blue Gorge. 





 When I heard the news I was filled with joy. Thank you, thank you very much."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20041"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"56"
					Exp	=	"961"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
