<Root>
{
	<Quest>
	{
		Desc	=	"팔마스 경비대장 렉터 쇼우를 처단하고 단서를 획득하라."
		GiveUp	=	"1"
		Id	=	"288"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"민병대의 이름으로"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"자네가 구해온 팔마스 수정구를 이용해 로그성과 교신해보니 괴멸된 팔마스 기사단에서 경비대장을 맡고 있던 렉터 쇼우란 자가 팔마스 공작 살해에 가담했을 가능성이 있을것이라고 여러경로에서 보고되었다고 하는구만. 
렉터 쇼우가 팔마스 공작살해에 가담을 했는지 진위여부를 조사하기 위해 아케트라브 기사단과 다자모르 기사단이 경쟁적으로 움직이고 있지만, 로그 황실에 우리 팔마스 민병대의 이름을 알릴 수 있는 절호의 기회를 놓쳐버릴 수는 없네. 

어서 서두르게나, 팔마스 민병대의 이름을 드 높일수있는 절호의 기회일세. 팔마스 경비대장 렉터 쇼우를 잡아오게. 분명히 완강하게 저항하고 반격할테니 준비를 단단히 하도록 하게. 만약, 발생하는 모든 후일은 책임을 질테니 즉결 처형을 거행해도 괜찮네. 그리고, 렉터 쇼우녀석이 팔마스 공작의 살해에 가담한 단서는  부디 얻을 수 있기를 바라네.."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20293"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"34"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"287;"
					}
				}
				<Branch0>
				{
					Desc	=	"조금 서두르시는듯 한데.. 경비대장 렉터쇼우.. 잡아드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"220"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"역시, 믿음이 가는 친구로구만. 믿고 기다리겠네."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"팔마스 공작 살해사건은 아케트라브"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"덩굴채 굴러온 기회인데, 그냥 버리려 하다니. 실망일세."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"민병대의 이름으로"
			Desc2	=	"렉터 쇼우의 안주머니에서 꼬깃꼬깃 접혀진 양피지를 발견했다고? 어디.. 어서 건내 주시게."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20293"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"69"
					Exp	=	"7000"
					Fame	=	"0"
					GP	=	"30"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"220"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
