<Root>
{
	<Quest>
	{
		Desc	=	"You must go to Klorida Pinkle."
		GiveUp	=	"1"
		Id	=	"193"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Militant Commander"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh yeah, that is that. When I told others about your bravery, the militant commander Klorida Pinkle wanted to see you. So do you want to go and see him?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20112"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"192;"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure I'll go and see him"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"He is in the solider office."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"165"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It might be more trouble than its worth."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you don't want to go, don't go."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Militant Commander"
			Desc2	=	"Mmm.are you the brave solider who went and had revenge on the Hobgoblins?  I'm glad you came, you look very strong as well as brave."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"165"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20089"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"767"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"165"
				}
			}
		}
	}
}
