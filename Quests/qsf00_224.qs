<Root>
{
	<Quest>
	{
		Desc	=	"Töte 20 Dunkler Schleim"
		GiveUp	=	"1"
		Id	=	"224"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Dunkles Hindernis"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ah, es ist eine schwere Aufgabe... wie gehen wir vor um ihn zu finden?  Hmm... ich hörte schon von Deinen Taten vom Offizier des Akedrav Ordens. Er schreibt mir ich soll von Dir Hilfe annehmen. Aber ich sollte erstmal Deine Fähigkeiten testen.



Der Dunkle Schleim taucht oft vor unserem Hauptquartier auf und stellt ein großes Hinderniss für unsere Leute dar. Wenn Du Dich um den Dunklen Schleim kümmerst, bin ich im Stande Dir zu trauen. Töte 20 Dunkle Schleim zum Beweis Deiner Fähigkeiten."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20281"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"223;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"38"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich zeige es Dir."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ich teste Dich, weil ich Deine Fähigkeiten sehen will. Bitte sei mir nicht böse..."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"20"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1050"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Mir egal."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Gut, dann erwarte auch nicht das ich Dir traue."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Böses Hindernis"
			Desc2	=	"Oh, Du hast die Dunklen Schleime sehr schnell besiegt! Super Du scheinst ein fähiger Abenteurer zu sein."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20281"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"28"
					Exp	=	"7706"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
