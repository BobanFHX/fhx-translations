<Root>
{
	<Quest>
	{
		Desc	=	"You must collect 8 Dark Stone bats."
		GiveUp	=	"1"
		Id	=	"131"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Forest Spirit"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"In this forest, there is an unexpected spirit.  There are also bats called Dark stone bats, but they are difficult to spot. The surprising thing is that, they're wings can help people recover strength. They're wings are considered top materials in these parts. That is why I want to collect them as much as I can while I'm here. I guess you can say this is my side job. 



 They only live around here, you can't find anywhere else in Idios. Don't even think about capturing it yourself and selling it. There are lot of corrupt merchants around Midland. They don't buy from first timers like you. Of course I'm different. If you want...get 8 of them and I'll give you a good price for the stack."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20241"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"18"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"8"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"114"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Great, I knew you would. I can tell if a person is willing or not."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, maybe next time."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm..I guess I mis-judged you...Alright. Be on your way then."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Forest Spirit"
			Desc2	=	"Hmm.. these look like medium quality wings. I wanted top quality wings. I can only give you half price for this. Really..really! Why don't you take this amount?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20241"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"71"
					Exp	=	"1839"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"8"
					ItemIdx	=	"114"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
