<Root>
{
	<Quest>
	{
		Desc	=	"Hunt 7 Shadow Orc Patrols."
		GiveUp	=	"1"
		Id	=	"116"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Shadow Orc Hunter"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Mmm if you go a little further up, you need to be very careful.  A little further up is the town of Devil's Children.  Hmm... sorry to delay you, but I was concerned about you. I am a hunter who has hunted Dark Orcs for 15 years. 





 There is a reason for me being this way. I used to make my living by hunting wolves and bears.  15 years ago I hunted a large bear and while I was coming back I saw my wife and my daughter being slaughtered by Shadow Orcs.  It was one of the direst feelings ever.  My heart was crying blood because I was so upset. 



 Since then, whenever I see Shadow Orcs, my blood begins to boil. Can you understand my feelings? What? You can easily kill Dark Orcs? Ha ha ha you do not know how strong they are. Fine, do you want to bet with me? If you bring me 7 Shadow Orc Patrols, then I'll give you a gift.  Wanna try?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20238"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll go with your pain in my heart."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"7"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"396"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hee hee I wonder if you can pull it off."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, but I can't.  I'll see you later."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You do not know how fun it is to bet."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Shadow Orc Hunter."
			Desc2	=	"Oh~ho you do have some skill.  You are no fluke, great... great - even though I lost I feel good."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20238"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"64"
					Exp	=	"1359"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
