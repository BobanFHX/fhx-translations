<Root>
{
	<Quest>
	{
		Desc	=	"Bringe die Gargoylehaut zu Schneider Schmidt!"
		GiveUp	=	"1"
		Id	=	"219"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Die Handschuhe"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Kennst Du Manasteine? Sie sind die Energiequelle der Golems. Jeder Golem hat ein Herz aus Manastein, wenn ich solch einen Stein habe, kann ich einen Golem erschaffen. Allerdings kann ich den Stein nicht mit der Hand berühren. Die Energie der Manasteine ist zu stark. Menschen können ihm nicht standhalten, aber die Haut von Gargoyles kann ihrer Kraft standhalten.



Kannst Du die Haut des Gargoyles dem Schneider Schmidt bringen und ihn sagen er solle daraus Handschuhe machen."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20279"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"218;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"35"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich mach es."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Gut.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"185"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich mach es nicht."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Diener? Wie kannst Du es wagen so etwas zu sagen? Ich zahle immer!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Die Handschuhe"
			Desc2	=	"Ah.. was ist das.. Gargoylehaut? Ich soll dir daraus Handschuhe machen? Warte eine Minute."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"185"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20264"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"48"
					Exp	=	"1526"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"185"
				}
			}
		}
	}
}
