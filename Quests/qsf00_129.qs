<Root>
{
	<Quest>
	{
		Desc	=	"Collect 8 Black Stone scorpion legs"
		GiveUp	=	"1"
		Id	=	"129"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Black Stone Scorpion leg"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Although Midland is a port city, it's also well known for its history in medicine.  Because the city is surrounded by sea, and the mountains produce high quality medicine plants. The plants are also affected by various weather that also produce different types of plants. 





 I am the medicine man who brings medicinal materials to Midland's medicine man. I was here to capture medicine materials, but the creatures are now much stronger. They were easy to catch with a trap before, but now my trap doesn't even work. I was in despair, then you walked by. What I need is Black Stone scorpion legs. If you can get me 8 of them I'll pay you handsomely."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20241"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"17"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure...I'll do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"8"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"112"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ha ha! I am glad that we can do a business."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have other jobs to do."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...You are missing out on a good opportunity!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Black Stone Scorpion legs"
			Desc2	=	"Hmm...you don't look like a novice soldier! You take care of the job with efficiency."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20241"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"69"
					Exp	=	"1670"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"8"
					ItemIdx	=	"112"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
