<Root>
{
	<Quest>
	{
		Desc	=	"Töte 10 Kampfratten-Anführer und 10 Kampfratten-Patriarch."
		GiveUp	=	"1"
		Id	=	"237"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Im Namen Gottes (3)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Das Imperium beauftragte uns den Kampfrattenstamm auszulöschen. Das Imperium informierte mich, dass alle Kreaturen der Unterwelt auferstehen, falls der Unterweltkristall angegriffen wird. 
			
			Wenn wir sie jetzt nicht töten, werden sie ein großes Hindernis sein, wenn der Dämonenlord zurückkehrt. Oh, wie können wir diese Schlacht gegen die Kampfratten gewinnen... es schmerzt mich dies im Namen Gottes zu tun."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20281"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"236;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"46"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde es versuchen."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Möge der Gott Dazamor immer bei Dir sein."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"10"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1280"
							NPCIdx1	=	"1285"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Das ist rücksichtslos!"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"All das ist für die Zukunft des Kontinents Herrcot."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Im Namen Gottes (3)"
			Desc2	=	"Die Aufgabe war, die Kommandostruktur des Kampfratten-Stammes zu zerstören. Ich bin froh das es Dir gut geht."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20281"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"13078"
					Fame	=	"0"
					GP	=	"5"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169013"
					SItem1	=	"169014"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
