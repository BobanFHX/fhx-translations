<Root>
{
	<Quest>
	{
		Desc	=	"Return with the letter explaining the Dajamaroo's situation to Paishul"
		GiveUp	=	"1"
		Id	=	"68"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Return to Iegees"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"In our Dajamaroo elders meeting we have been discussing the recent incidents. For now we decided that we have to give it more time and careful observation will be required. 





   We will also need more information and then we will decide if we want to work with Arketlev.  This has been decided by our elders.  Please take this to Paishul for me, thank you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20211"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"11"
					}
					<CondNode2>
					{
						Class	=	"16"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"67;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes I'll deliver this."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"62"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Sorry that I couldn't give you the answer you were seeking."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have some business in Rog"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You really don't have to get the answer which you seek right?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Return to Iegees"
			Desc2	=	"Hmm, this is his answer? Unexpected, or should I say that they are truly Dajamaroo? 





 As one who shares the same burden I feel frustrated."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20224"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"62"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"26"
					Exp	=	"147"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"62"
				}
			}
		}
	}
}
