<Root>
{
	<Quest>
	{
		Desc	=	"Collect 10 fingernails of vagabond Lycan Slop and give it to Mario Canton."
		GiveUp	=	"1"
		Id	=	"160"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Kimera's appearance"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh boy scary, scary. The world has become a scary place.  I never seen anything like that creature before.  First time, hey have you ever seen a wolf that walk like a man before? I bet you haven't seen it either. I was passing through west of the gorge to get to the shadow mine's entrance.  I saw a head of a wolf and walking around. I didn't even get near the entrance, I just watched it and ran back down.  Their eyes were like a killer. 



 

 I'm in trouble because I need to go there right away.  Nobody would believe me if I tell others.  You look like someone who has been through some tough situations before.  Even though it might be strong, I think you can handle it.  Well what you think? Do you want to find out if that thing is aggressive or not?  I don't need his finger nails for souvenir, but I want to warn the townspeople about it."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20250"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm...though I don't feel for it yet I'll give it a try."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"137"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Mmm...I really need to get there."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have some place urgent to get to."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Man...what should I do?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Kimera's appearance"
			Desc2	=	"How was it? Do you think I could pass by ignoring them? 





 What? They are aggressive? I could have been killed?  Then what do I do? I don't think I can go on my own, this is bad..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20250"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"87"
					Exp	=	"3072"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"137"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
