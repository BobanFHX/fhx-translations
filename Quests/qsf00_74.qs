<Root>
{
	<Quest>
	{
		Desc	=	"You need 5 Gorge Orc Shaman's staffs"
		GiveUp	=	"1"
		Id	=	"74"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Gorge Orc Shaman's Staff"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"What do you think? It's pretty good medicine huh? A little bit later I can use even higher magic. Don't you want to test out your new strength? I hope you don't have any other intention.  Well if you don't then just look at me and please understand. When you go up Horn Tiger Mountain range you will see a lot of the Orcs live there. 



 They are kind of stupid, but yet they use the strength of magic. They may not be much, but they are skillful magic users. I don't know where they got them, but they have mana trees.  The tree is pretty rare. I think they might have different roots from ours. Anyway can you get me 5 staffs?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20030"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"11"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"73;"
					}
					<CondNode3>
					{
						Class	=	"32"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, I'll do it"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"50"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I leave it to you... ho ho"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am suspicious about your motives."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I wish you would do this for me"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Gorge Orc Shaman's Staff"
			Desc2	=	"Thank you very much for your work."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20030"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"135"
					Exp	=	"845"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"156005"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"50"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
