<Root>
{
	<Quest>
	{
		Desc	=	"골짜기 그리즐리를 사냥하고 곰가죽 10개를 구해오시오."
		GiveUp	=	"1"
		Id	=	"292"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"부서진 목책"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"자네.. 조금만 더 도와주게. 자네의 실력이 좋은듯하니. 이참에 부서진 저기 목책들을 수리해놔야겠네. 아무래도 샐리온 수비병들이 돌아오면, 알어서들 해야 할일이지만, 언제 돌아올지도 모르니, 나 혼자서라도 여기를 지킬려면, 목책을 정비해놔야만 좋을듯 하구만.. 하다못해 곰탱이 녀석들이 다시와도 목책을 방패삼아 대치는 할 수 있을 것이야. 

부서진 저 먹책을 이을만한 끈이 필요한데.. 음 마땅한 재료가 없구만.. 덩치큰 곰탱이 녀석들의 가죽을 벗겨서 끈으로 만들면 아주 튼튼할 듯 한데 말이지.. 

골짜기 그리즐리의 가죽이 좋을듯 하구만, 가서 10개만 구해다 주게나.."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20295"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"36"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"291;"
					}
				}
				<Branch0>
				{
					Desc	=	"이왕 도와드린것이니. 이번까지만 더 도와드리죠.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"자네가 도와줄것을 알고있었네..고마우이.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"222"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"수비병도 없는 마당에 혼자서 뭘 할수있다고.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"너저분하게 부서진것을 두고 있을 수는 없지 않나..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"겁없는 녀석들"
			Desc2	=	"자네. 보기보다 실력이 꽤 좋구만. 좋아..음."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20295"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"7000"
					Fame	=	"0"
					GP	=	"11"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"222"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
