<Root>
{
	<Quest>
	{
		Desc	=	"모리스의 유해를 미들랜드항에 살고있는 부모에게 전달하라."
		GiveUp	=	"1"
		Id	=	"310"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"영혼의 귀향"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"심슨씨 형제는 정말 정이 많고 근면한 사람들이었는데, 그런 성실한 모습에 저 또한 마음이 흔들리고 있던 중이었어요. 사실은, 이번에 모리스씨를 만나면 저의 마음을 고백하고 모리스씨와 행복할 수 있기를 바랬어요. 모리스씨의 부모님은 미들랜드항에 계세요. 저는 차마 그분들을 마음이 아파 뵐 수가 없으니, 용사님께서 모리스씨의 해골유해를 편안하게 모셔다 주실 수 있나요?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20298"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"55"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"309;"
					}
				}
				<Branch0>
				{
					Desc	=	"사랑하는 사람의 죽음도 슬픈데, 유해까지 전달하기 힘드실테죠. 제가 전해드리겠습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"정말 감사드려요. 인자한 나의 영웅.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"231"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"유해는 여기 근처에 묻어주시죠. 저에게는 너무 먼거리군요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"어쩔 수 없죠. 용사님.. 그래도 고마웠어요."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"영혼의 귀향"
			Desc2	=	"아.. 어젯밤 꿈이 사실로... 하나남은 아들놈마져 비적의 잔악함에 희생되어버리고 싸늘한 주금으로 돌아오다니..나는 이제 누구를 의지하면서 살아야 한단 말인가.
차라리 내가 비적단에게 잡혀갔어야 하는데, 자라오면서 남에게 나쁜짓 한번 안한 착한 내아들이 이렇게 돌아오다니..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20300"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"231"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"12462"
					Fame	=	"0"
					GP	=	"31"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"102002"
					SItem1	=	"102003"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"231"
				}
			}
		}
	}
}
