<Root>
{
	<Quest>
	{
		Desc	=	"Capture poisonous spiders to return the Rich Merchant뭩 money bag."
		GiveUp	=	"1"
		Id	=	"87"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Merchant’s Money Bag"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey man can you talk to me for a second? I need to go to Palmas. While on my way there I was attacked by Orcs and I got separated from my companions. All of my companions were separated from each other. They can뭪 come here either. This is the only road and they have to go through this way. 





  I need to rest somewhere. I didn뭪 know that this was such a dangerous place. I saw a small sparkling thing and I got interested. 





  I am not saying that it was a jewel or anything. When I looked at it, I found that it was spiders. When I saw their mouths I saw blackish-red colours, and I knew that they were highly poisonous. I ran as fast as I could, but when I got here I noticed that I had lost my money bag. Maybe I dropped it  while I was running or maybe I dropped them with at the spiders. 





 Maybe they thought it was food and took it. Can you get my money bag from them? I have something better than money with me."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20232"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"13"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm I’ve got it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"78"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I will wait right here, then again where would I go? I can뭪 go far anyway."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don’t really feel like helping you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Merchant’s Money Bag"
			Desc2	=	"My money bag where have you been? 





  Did those spiders leave you on the ground? My precious, come to me, sniff, thank you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20232"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"1085"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"78"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
