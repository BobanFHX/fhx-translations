<Root>
{
	<Quest>
	{
		Desc	=	"Deliver secret document to the James Madio."
		GiveUp	=	"1"
		Id	=	"137"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Deliver secret document"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"It was a difficult job, but I finally unsealed it. It took all my mana to do it, he must have been a powerful magician. 





 It is amazing that there is still powerful black magicians left. That magician can easily wipe out a town. I think things are starting to heat up. Quickly deliver this document to guardian captain."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20233"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"20"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"136;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes...I got it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I hope that things will be done quickly."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"119"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Now I heard the new my desire..."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Did I made things more difficult?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Delivering secret document"
			Desc2	=	"Hmmm, you unsealed it? Right...I'll take a look. Mmm...mmm..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"119"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"19"
					Exp	=	"470"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"119"
				}
			}
		}
	}
}
