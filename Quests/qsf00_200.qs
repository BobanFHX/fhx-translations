<Root>
{
	<Quest>
	{
		Desc	=	"Collect 5 wolf spider shells and give them to Kating Smiths."
		GiveUp	=	"1"
		Id	=	"200"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Spider Hunt"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey I don't mind you not working with me, but don't you think it would be a waste to use your skill only once? Can you use your skills for me one more time? 



  Count Hubbard asked me for a difficult order.  That means this order will be very profitable.  Do you want to know what it is?  It is stuffed spider.  Not only is it difficult to catch them in good condition, their legs and waist are very difficult to stuff. 



  Do you think you can get me 5 of the wolf spider shells?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20264"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"28"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"199;"
					}
				}
				<Branch0>
				{
					Desc	=	"Well it doesn't sound too difficult."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"170"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Of course it's not that difficult.  If you can bring them in good condition it would be great."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Those spiders might be too difficult."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm.. I know that it might be a difficult task, but I don't think it'll be that difficult."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Spider hunt"
			Desc2	=	"Oh~ho, this is it.  I've seen others bringing them and they were in terrible shape.  I think you have a special talent in this area.  Are you sure you don't want to work with me?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20264"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"98"
					Exp	=	"4117"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"170"
				}
			}
		}
	}
}
