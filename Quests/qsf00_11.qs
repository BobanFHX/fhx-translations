<Root>
{
	<Quest>
	{
		Desc	=	"Kill Small Striped Spiders and give Rudy Dagoru 10 shiny treasures."
		GiveUp	=	"1"
		Id	=	"11"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Mother's necklace"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Sniff, sniff, what did I do? Sniff, Sniff... She will kill me when I get home. Wahhh! Sniff, who are you? What is wrong? Well, I was on my way to Sebastian to get my mother's necklace cleaned, but I fell and now it is broken. 



 Sniff, sniff. The necklace was a gift from my father to my mother, and she cherished it more than anything. As she was going to use it for their wedding anniversary, she wanted to clean it. Sniff, sniff... If only I was strong enough to gather some shiny spider's eyes, then Sebastian could repair the broken necklace. Maybe you can help me out? Can you please get me 10 shiny spider's eyes? If you go East from Rog's front gate, you will find Small Striped Spiders that have beautiful eyes. 





 10 Small Striped Spider eyes should be plenty to repair the necklace."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20203"
					}
				}
				<Branch0>
				{
					Desc	=	"Should I try it? I think I will."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"11"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Sniff, sniff.. Thank you so much. Sniff, please.. hurry."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"No, just tell your mother the truth."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Wahhhhh.. Sniff.. My mother will kill me...!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Mother's necklace"
			Desc2	=	"You killed those spiders? You are a hero, thank you very much! I should hurry and tell Sebastian to repair the necklace. Heh, Heh.



 I should reward you, but I don't have much. Take this ring, as a token of my appreciation. The ring is very precious to me so I hope it will serve you well."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20203"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"31"
					Exp	=	"112"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"167000"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"11"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
