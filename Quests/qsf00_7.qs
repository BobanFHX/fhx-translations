<Root>
{
	<Quest>
	{
		Desc	=	"Get rid of troublemakers Black Peanut and Dark Eye at the sawmill."
		GiveUp	=	"1"
		Id	=	"7"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Outcast Goblin"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm, I see you don't mind serving others, citizen. I might have a job for you. Do you mind hunting for some reward? It should be easy for someone like you. 





  There are 2 nasty Goblins who keep disturbing the sawmill, making the workers scared and unable to work. If you head north towards the sawmill, you should find Black Peanut and Dark Eye not far up the road. They are troublemakers, but they are pretty quick. Are you up for the task?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"1"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20194"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"6;"
					}
				}
				<Branch0>
				{
					Desc	=	"Sounds interesting. I'll give it a try."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Good luck, citizen! Be wary of those vile Goblins..."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"5"
							KillCnt1	=	"5"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"30"
							NPCIdx1	=	"35"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't like the sound of that."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I guess I misjudged you. Good day to you."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Outcast Goblin"
			Desc2	=	"You actually did it? I mean, thank you, citizen! The sawmill is finally relieved of those troublemakers. Your clothes look worn-out. I have some clothes lying around. They're a bit old, but I'm sure you can use them. Do you want to try some on?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20194"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"31"
					Exp	=	"112"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"166000"
					SItem1	=	"166001"
					SItem2	=	"166002"
					SItem3	=	"166003"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
