<Root>
{
	<Quest>
	{
		Desc	=	"Bringe das Historische Dokument zu Tom Bäcker; er lebt im südlichen Territorium des Todes."
		GiveUp	=	"1"
		Id	=	"204"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Seelenhandel (2)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ich kann dieses historische Dokument nicht lesen, weil es in historischer Schrift geschrieben wurde...


Tom Bäcker ist ein Gelehrter, der könnte die historische Schrift verstehen. Ich bitte dich, gehe zu ihm und zeige ihn dieses Dokument. Er sollte es übersetzen können."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20275"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"203;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"30"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich gehe sofort."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du bist der beste Mann für diese Angelegenheit."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"175"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich gehe da nicht hin."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du meinst, es wäre zu anstrengend?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Seelenhandel (2)"
			Desc2	=	"Oh, was ist das für ein Dokument? Es ist in einer altertümliche Sprache geschrieben. Es gibt kaum noch jemand der diese Sprache benutzt. Ich werde versuchen es zu entschlüsseln."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"175"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20233"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"36"
					Exp	=	"1453"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"175"
				}
			}
		}
	}
}
