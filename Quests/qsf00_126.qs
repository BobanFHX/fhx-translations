<Root>
{
	<Quest>
	{
		Desc	=	"You must capture the Lich and its 3 guards."
		GiveUp	=	"1"
		Id	=	"126"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Death Ruler [Death Ruler]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Wow man...this strength, where have I felt this kind of strength before? It was at Cleo incident...I knew I have felt this type of strength before! It was the strength of the lich. 





 Who could of...Have you heard of lich before?! The Lich is not an undead, but a living being that has become an undead creature through magic and alchemy. Sometimes black magicians will make the transformation to live forever. When you become a lich, the power of your magic increases, but you loose all your abilities as a human being. So not too many black magicians  cherish the thought of being a lich unless it is a special circumstance. Then who is that lich over there? Who would throw it all away to become the lich? 





 Maybe he wanted to rule all these undead creatures. Anyway, we must get rid of the lich before it moves on. They're poison is so strong that, once they kill a plant, nothing will grow there for about 100 years. I'll gather my friends, why don’t you gather your friends and get rid of it. You have to get rid of that lich and it's three black magicians."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20239"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"19"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"125;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes I understand."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"1"
							KillCnt1	=	"3"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"550"
							NPCIdx1	=	"515"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Come, we must hurry!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"My immunity against the poison is weak."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Man!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Death Ruler [Death Ruler]"
			Desc2	=	"Ah! Did we kill it!? I am filled with his poison...it was a difficult battle. Wow, you were great.  Looks like you're not fully capable, yet your determination, and hard work.....I am impressed. I'll be witness to your successes from above. Ha ha ha!



oh no..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20239"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"85"
					Exp	=	"2019"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"166005"
					SItem1	=	"167004"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
