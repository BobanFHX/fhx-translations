<Root>
{
	<Quest>
	{
		Desc	=	"떠돌이 모래골렘 10마리를 처치하라."
		GiveUp	=	"1"
		Id	=	"318"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"사막의 바람"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"에취..에취.. 정말 날씨가 왜 이러는지....

강렬한 태양이 눈부시게 내리쬐지만, 어디서 불어오는지 모르는 이 차가운 바람에 몸을 제대로 가누기도 힘이 드는군요. 몇일전 떠돌이 모래골렘인가 뭔가하는 녀석이 보이기 시작하면서 이 척박한 모래사막에도 찬바람이 불기 시작했죠. 아무래도 그놈들이 바람을 일으키고 있나본데.. 몸이 너무 아파서 어떻게 하지를 못하겠군요. 용사님이 저 대신 떠돌이 모래골렘 10마리만 처치해 주시겠습니까?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20301"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"49"
					}
				}
				<Branch0>
				{
					Desc	=	"모래 골렘쯤이야..."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"고맙습니다. 에취..에취.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1325"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"골렘을 처리한다고 바람이 불지 않는다니..하하"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"에취..에취.. 그놈들이 맞는데.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"사막의 바람"
			Desc2	=	"휴.. 이제 바람이 좀 잠잠해지는군요. 
역시, 떠돌이 모래골렘이 일으킨 바람이 맞군요.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20301"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"17137"
					Fame	=	"0"
					GP	=	"21"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
