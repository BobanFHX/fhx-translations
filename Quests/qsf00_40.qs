<Root>
{
	<Quest>
	{
		Desc	=	"Get 10 brown bear's paw to Duk Aleliban"
		GiveUp	=	"1"
		Id	=	"40"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Bear front paw dish"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"These bears, unlike before, they are getting smarter. If it were before I would of already had plenty of them, but now I can’t even catch a one bear.  Hey do you want to try this job? You look strong, I think you'll be perfect for this job. 





  I own a restaurant in Rog called "Michael's Bear Front Paw".  My restaurant is well known throughout Rog, but problem is that I need lot of bears to meet the demand.  They aren't as dumb as they use to be. 





   I can't use just any type of bear, but it has to be brown bear's front paw.  I've been trying very hard to keep up with the demand, but I can't keep up.  Would you like to help me catch some?  I think 10 of them would do.  I'll pay you handsomely for them."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20218"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"8"
					}
				}
				<Branch0>
				{
					Desc	=	"Don't forget, you said that you'll pay handsomely. "
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"39"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I believe in your skills"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have something more urgent than money"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm....demand....demand....sniff."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Bear front paw dish"
			Desc2	=	"Oh 10 of them? You are more skillful than you look.  If I gather them in this rate, I don't have to worry about it for a while hee hee."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20218"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"47"
					Exp	=	"548"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"39"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
