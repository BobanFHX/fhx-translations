<Root>
{
	<Quest>
	{
		Desc	=	"Bring evidence of change to Bronko Lailicks"
		GiveUp	=	"1"
		Id	=	"77"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Change in Lake Rough Crocs."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Thank you for saving me. If it weren't for you I would have been dead. I was sent to the Mirror Lake area by my Guard Captain to investigate, then I got a surprise attack. I wasn't able to move. I asked one of the hunters who was passing by to get me a priest, but I was very worried that I might die here. 





  Cough... I am healed a lot, but if I move just a little, it still hurts. I am sorry but can you help me? While I was investigating I found crucial evidence. Can you get this to my captain? He'll be very thankful."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20227"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"12"
					}
					<CondNode2>
					{
						Class	=	"128"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"76;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do what you ask of me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"71"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Cough... Dajamaroo's blessing be with you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have another job to do."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I'll do it after my rest."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Change in Lake Rough Crocs"
			Desc2	=	"Brane's verbal message? What's this? This is more evidence. I see Belzev's followers will be reborn?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20195"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"71"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"14"
					Exp	=	"168"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"71"
				}
			}
		}
	}
}
