<Root>
{
	<Quest>
	{
		Desc	=	"기형 타란툴라의 거미다리털 10개를 구해오시오."
		GiveUp	=	"1"
		Id	=	"320"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"보온재료 조달"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"에취.. 정말 왜이리도 춥지. 
바람을 막았지만, 뼈속까지 스며드는 이 한기는 온몸이 얼어 붙는듯 하네요.. 몇일전 지나가다 보았는데, 거미다리에 복슬 복슬한 털은 정말 따뜻해 보이더군요. 기형 타란툴라의 다리털을 구할 수 없을까요? 다리털을 구해만 주시면 가보로 전해온 정말 좋은 물건을 드리죠. 얼어 죽을판에 가보를 간직해서 어디다 쓰겠어요. 정말 좋은 물건이니 기대해도 좋습니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20301"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"49"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"319;"
					}
				}
				<Branch0>
				{
					Desc	=	"그 다리털을 몸에 칭칭 감고 있을려구요? 어쨌든 구해드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"고맙습니다. 에취..에취.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"239"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"하하..그게 효능이 있을리가...."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"몸이 아프다고 정신도 혼미한줄 아시오..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"보온재료 조달"
			Desc2	=	"정말 고맙소. 이제 좀 따뜻해지는군요. 

자.. 여기 약속한 우리 집안의 가보를 받으시오."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20301"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"17137"
					Fame	=	"0"
					GP	=	"21"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169035"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"239"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
