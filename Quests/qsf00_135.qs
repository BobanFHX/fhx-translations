<Root>
{
	<Quest>
	{
		Desc	=	"You must deliver a sealed document to the priest of Arketlav at Fog cemetery; Tom Belin."
		GiveUp	=	"1"
		Id	=	"135"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Canceling the seal"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"The problem is that we can't look at the document. Realistically if there's a secret document, that means that there is a some kind of dark magicians around. This means that low ranking magician didn't speak all lies. Unfortunately there is only one in my care that can open this sealed document...





 The only way to open this is to see the high priest, but fortunately awhile ago Tom Balin came around deadman's holy land. I heard that he is at Fog cemetery. Can you go to him and ask him to unseal this? I believe he will do it for you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"19"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"134;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I will go right away."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"117"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hurry!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I would like to quit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you scared?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Canceling the seal"
			Desc2	=	"Hmm? What? You want me to cancel this seal? This is from the guard captain? Let me take a look. 





 This is no ordinary magic that sealed it. No ordinary wizard can open it. 





 This is a pretty high spell, but the only way to open it is to use strength. You must be careful or else you can lose the whole document if you apply too much strength."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"117"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20233"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"18"
					Exp	=	"415"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"117"
				}
			}
		}
	}
}
