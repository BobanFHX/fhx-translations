<Root>
{
	<Quest>
	{
		Desc	=	"Gehe zu Sandra Patosi um Essen zu holen."
		GiveUp	=	"1"
		Id	=	"250"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Essen holen"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hast Du alle Hobgoblins selber gefangen? Du bist gut.. Ich hoffe Du kommst irgendwann mal wieder um Dir wieder ein Kopfgeld abzuholen.
			
			Hm?? Essen? Du kannst Essen bei Sandra Patosi. Es scheint so, als hättest Du keinen Essenbeutel. Hier ist einer für Dich und nun gute Reise."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20287"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"249;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"49"
					}
				}
				<Branch0>
				{
					Desc	=	"Danke, sehr freundlich."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du bist Willkommen. Ich danke Dir dafür, dass Du die Goblins um die Stadt herum gejagt hast."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"205"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich brauche es nicht."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Bist Du dir sicher? Dann komm später wieder."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Essen holen"
			Desc2	=	"Brauchst Du etwas zu Essen?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"205"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20077"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"54"
					Exp	=	"2521"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"205"
				}
			}
		}
	}
}
