<Root>
{
	<Quest>
	{
		Desc	=	"Daron Poot wants 10 Amulet of Wind"
		GiveUp	=	"1"
		Id	=	"191"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Gloves of wind"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Dust Gorge is a place where no living things could survive, however the monsters are numerous here.  50 years ago at Blue Gorge there were battle plan to destroy all the Orcs.  Many Orc were blended with the native Orcs living there.  This brought a unique opportunity for them to harvest different skills by combining all the different types of Orcs. 





  One of the unique skills is they can make an Amulet of Wind.  I do not know how it is made, but those who have them, such as Orc Archers develop the ability to shoot arrows faster.  Awhile ago when I was in battle with the Orcs, one of the soldiers brought me one.  He brought it because it looked strange.  With it I made special gloves.  I called them the Glove of Wind, they were quite a useful glove.  If you want them as well, you need to bring 10 Amulet of Wind to me and I'll make you one."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20048"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"25"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm..I am intrigued"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It is quite an intriguing story."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"10"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"164"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I really don't need it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You might need it. It is a high quality item."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Glove of Wind"
			Desc2	=	"Did you bring them? For my labor I'll take five of them, hee hee, I only need five to make one.  I can't make it for free, just wait here"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20048"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"90"
					Exp	=	"3315"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"163007"
					SItem1	=	"163008"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"164"
				}
			}
		}
	}
}
