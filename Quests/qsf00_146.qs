<Root>
{
	<Quest>
	{
		Desc	=	"Must deliver official certificate to Captain Jack."
		GiveUp	=	"1"
		Id	=	"146"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Delivery of certificate."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"To tell you the truth I had to count it has a source.  It was a devil's writing which didn't appear for few hundred years.  I am saying, it wasn't written by a copy cat. 





  This is written on sheepskin by creature named Demon who is second lieutenant, and had enough mental capacity to write it. can you understand why I was so excited when I saw it?  Anyway, if you have time come and see me again, 





 I'll tell you in more details.  This is a certificate that we are keeping the document.  I hope this will clarify things for you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20246"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"21"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"145;"
					}
				}
				<Branch0>
				{
					Desc	=	"Thank you for your help."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"125"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"No thank is necessary...matter of fact I should apologize once again to that friend when see him."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"DCelivery of certificate"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmmm...he is not taking my apology warmly."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Delivery of certificate."
			Desc2	=	"What? That really happened? Hmm...I didn't even know and I was so mad that I gave an order to kill him. 





    I must immediately call of the order. Anyway, I think you are a decent man for listening to his sob story and helping him."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"125"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20244"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"532"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"125"
				}
			}
		}
	}
}
