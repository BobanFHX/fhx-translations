<Root>
{
	<Quest>
	{
		Desc	=	"Elia Baskin wants you to bring her 15 Small Black Bear's teeth."
		GiveUp	=	"1"
		Id	=	"18"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Small Black Bear's teeth"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hee hee hee... Looking for work? I think we can help each other out. I am a merchant who sells rings, necklaces, handbags... I have quite a few valuable things, hee hee. 





  Lately there has been a huge demand for brooches by local women. Of course, they only want the best quality. Have you heard about Nymph Lake? On the northern side you should find some Small Black Bears. They are quite rough, but their teeth are so smooth... 





  I'm pretty sure I could make some beautiful brooches with those teeth. Could you bring me some? About 15 of them would do."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"5"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20065"
					}
				}
				<Branch0>
				{
					Desc	=	"I see. I'll get them for you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"15"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"15"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I'll make sure you won't regret this."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't really care for it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I guess brooches aren't your thing..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Small Black Bear's teeth."
			Desc2	=	"Hee hee hee... Well done! This should be enough, right? Oh, and take this shield as well. I don't need it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20065"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"302"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"158001"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"15"
					ItemIdx	=	"15"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
