<Root>
{
	<Quest>
	{
		Desc	=	"Capture 8 Gorge Orc Warrior Leaders."
		GiveUp	=	"1"
		Id	=	"63"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Orc Warrior Leader"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ok, I admit that you have the skills of a warrior. Skills aren't everything; if you want to be a true warrior you need deeds that complement it.  Behind the Iegees, there is a well known fact that there are lot of monsters at Horn Tiger mountain range. 





   Especially, Orcs roam around as if that's their territory.  We got rid of lot of the Orcs after the sweep operation but there are a lot of Orcs still roaming around.  There is one type that matches well with our warriors.  





 If you look at them they have a lot of fighting spirit.  Go now, you should and try to capture the Orc Warrior Leader. Feel the blood of the warrior by capturing 8.  When you hunt them you'll feel warrior's blood boiling in you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"11"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20006"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"62;"
					}
					<CondNode3>
					{
						Class	=	"1"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll show you the spirit of the warrior."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"8"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"235"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"However it won't be easy."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Do I need to continue doing this?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you don't want to I won't ask you to anymore."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Orc Warrior Leader"
			Desc2	=	"What do you think? Don't you feel the blood boil? Although they are different from us, they can have the same blood flowing in them."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20006"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"54"
					Exp	=	"845"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
