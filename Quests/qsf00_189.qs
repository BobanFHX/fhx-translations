<Root>
{
	<Quest>
	{
		Desc	=	"Give 15 Fingernails of Vagabond Werewolf to Canton."
		GiveUp	=	"1"
		Id	=	"189"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Condition for Great Sword"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey look, have you heard of this rumor? Rumor has it that there is a wolf man that's roaming around that is called Vagabond Werewolf.  They were legendary creatures that used to help humans. 





 I've heard that their fingernails are used to make a Great Sword.  After they were wiped out, making that sword was out of the question. 



 My wish is to make one of those swords, can you get 15 of those Vagabond Werewolf's Fingernails for me? I'll make two and give one to you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20014"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"25"
					}
				}
				<Branch0>
				{
					Desc	=	"If it is for a Great Sword then it might be worth it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Of course it'll be worth your trouble."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"15"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"162"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I do not need a Great Sword."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I can give you something other than a great sword."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Condition for Great Sword"
			Desc2	=	"Oh~ look at the luster of these nails.  Don't you think it's great? Just think about what it can do to the sword once I add the minerals to it.  I don't think you will see anything like it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20014"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"90"
					Exp	=	"3315"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"150009"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"15"
					ItemIdx	=	"162"
				}
			}
		}
	}
}
