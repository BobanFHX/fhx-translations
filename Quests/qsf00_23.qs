<Root>
{
	<Quest>
	{
		Desc	=	"Deliver Alexia's letter to Loren Patosi who lives near King's Falls."
		GiveUp	=	"1"
		Id	=	"23"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Ruin's Piece (Loren Patosi)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"The evidence that was found here is whats normally called a piece of soul. See these here, they haven't been found for over 50 years. Truthfully, I heard about this, but never saw it before.  This was made by an evil creatures's spirit, the method and how to create is still a mystery. 





 Unfortunately I am sure this is the piece of soul. To be sure we should get this to the history professor Loren.  She is currently studying at the King's Fall's. In here is the evidence you brought and the my report, can you deliver this to the professor?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20206"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"6"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"22;"
					}
				}
				<Branch0>
				{
					Desc	=	"King's Fall? Sure I'll do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"19"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I think someone is trying to revive the thousand year old curse.

 This is bad, real bad."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I have some work to do elsewhere."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh, I believed in you."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Ruin's Piece (Loren Patosi)"
			Desc2	=	"What? Alexia? Hmm, Let me see it. I would like to serve you with some drink, I know the travel must have been very difficult."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"19"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20207"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"11"
					Exp	=	"71"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"19"
				}
			}
		}
	}
}
