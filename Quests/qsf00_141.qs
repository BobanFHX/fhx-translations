<Root>
{
	<Quest>
	{
		Desc	=	"Helton the robot is expecting 5 bottles of grey rakewolf's blood"
		GiveUp	=	"1"
		Id	=	"141"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Rakewolf's blood."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Brave soldier, can you please help me?  I am a student of blue ocean alchemist academy from Midland. 





 Of course it has been only 3 years of studying alchemy. But school's assignment need to have animal's blood.  My subject was called study of Rakewolf's blood.  So I need to collect some sample, I am not knight or wizard it's hard to collect their sample.  I am sorry but can you collect 5 bottles of small gray rakewolf's blood, I will reward you.  





 They usually live around summer wind gorge."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20242"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"21"
					}
				}
				<Branch0>
				{
					Desc	=	"Of course I'll help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"121"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I am glad to meet a brave warrior like you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, but I have place to go."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh.....yes...sorry...I guess I asked too much."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Rakewolf's blood."
			Desc2	=	"Wow...you found them all.  Wow...so much...I think these will be enough samples.  Thank you very much for helping me."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20242"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"79"
					Exp	=	"2408"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"121"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
