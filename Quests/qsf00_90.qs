<Root>
{
	<Quest>
	{
		Desc	=	"You must get 9 small oblivious poison sacks to Harold Pakins."
		GiveUp	=	"1"
		Id	=	"90"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Oblivious Well Water"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Phew~ so hard they are increasingly difficult because they are getting stronger in tolerating magic. What? Someone actually comes out here? Who are you? What business do you have here? This place is for people like me who just want to forget everything and rest.





 I guess I have nothing to say if youre here for the same reason. What can I say, you coming here is some kind of fate. It must be. Hey would you catch some of the oblivious spiders for me? I need their poison sack to make me oblivious well water. Dont ask me why I am so miserable. 





 Living is one giant pain. If you get me 9 poison sacks, then Ill pay you handsomely for them."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"13"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20234"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, it¡¯s not that hard."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"9"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"81"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Phew~!! Then I¡¯ll leave it up to you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Hmm, do you really need that poison?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hah, you do not know what faith can bring."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Oblivious Well Water"
			Desc2	=	"Heh heh, well done, good job.  Now I can finally be done with my difficult journey.  I can now finish it much quicker. Thanks!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20234"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"1085"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"9"
					ItemIdx	=	"81"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
