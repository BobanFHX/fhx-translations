<Root>

{

	<Quest>

	{

		Desc	=	"Töte 20 Streunende Troll"

		GiveUp	=	"1"

		Id	=	"252"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Kopfgeldjagd (1)"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Es gibt keine Kaserne in Land der Giganten, dadurch ist die öffentliche Sicherheit in Gefahr. Daher haben wir für vorbeikommende Abenteurer und Kopfgeldjäger Belohnungen, wenn sie uns helfen. 



Die Streunenden Trolle attackieren regelmäßig Hadun, weil sie wissen, dass es keine Soldaten in der Stadt gibt. Das Kopfgeld für 20 Stück von ihnen ist sehr hoch. Bist Du interessiert?"

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"2"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20287"

					}

					<CondNode1>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"50"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich werde es versuchen."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Sei vorsichtig. Sie sind schwer zu töten, ganz so, als hätten sie zwei Leben."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10000"

							KillCnt0	=	"20"

							KillCnt1	=	"0"

							KillCnt2	=	"0"

							KillCnt3	=	"0"

							NPCIdx0	=	"1310"

							NPCIdx1	=	"-1"

							NPCIdx2	=	"-1"

							NPCIdx3	=	"-1"

							SvrEventType	=	"0"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Nein Danke."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Wirklich? Es ist wohl besser sich nicht der Gefahr auszusetzen, trotz des Kopfgeldes."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Kopfgeldjagd (1)"

			Desc2	=	"Hast Du es wirklich geschafft? Du bist sehr gut."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20287"

				}

				<CondNode1>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"0"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"13"

					Exp	=	"17137"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

