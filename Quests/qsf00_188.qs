<Root>
{
	<Quest>
	{
		Desc	=	"Deliver the luggage to Midland's Bay Alex."
		GiveUp	=	"1"
		Id	=	"188"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Strange Luggage Delivery"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"This is the luggage that I brought.  You tried to open this luggage while I was gone. When I tried to open it, I would get the chills and my body would shake, and goose bumps would rise.  That's why I couldn't open it.  I couldn't shake this strange and eerie feeling. 





 I think this would be too much for regular a human being to handle.  When you go to the Midland's Arketlev elder named Bay Alex.  He is a great man.  He even listens to people like me.  If you give this to him, I think he'll know what do with it. Can you do this favor for me?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20252"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"187;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yeah, I'll deliver to him."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"161"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I'll ask you to hurry please."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't think I can make it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Really? Should I throw it away?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Strange Luggage Delivery"
			Desc2	=	"Ah~ that miner from Dust Gorge?  Right, He told you to get this luggage to me? Let's take a look. 





 What? What is this strange feeling?  No normal man could bear this.  From the looks of it you are not a normal man. 





I'll take care of it, good job."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"161"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20246"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"1732"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"161"
				}
			}
		}
	}
}
