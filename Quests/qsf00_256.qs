<Root>

{

	<Quest>

	{

		Desc	=	"Töte 20 Oger Krieger."

		GiveUp	=	"1"

		Id	=	"256"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Kopfgeldjagd (5)"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Du bist wirklich gut. Wie wäre es diesmal mit Oger Krieger? Diese Oger peinigen Reisende und Bürger von Hadun. Die Belohnung ist auch sehr hoch... interessiert?"

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20287"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"255;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"55"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich werde es versuchen."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Sie sind stärker als alle Anderen, pass gut auf Dich auf."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10000"

							KillCnt0	=	"20"

							KillCnt1	=	"0"

							KillCnt2	=	"0"

							KillCnt3	=	"0"

							NPCIdx0	=	"1415"

							NPCIdx1	=	"-1"

							NPCIdx2	=	"-1"

							NPCIdx3	=	"-1"

							SvrEventType	=	"0"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich will nicht."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Komm zurück, wenn Du deine Meinung geändert hast."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Kopfgeldjagd (2)"

			Desc2	=	"Du bist sie aber schnell losgeworden... Großartig!"

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20287"

				}

				<CondNode1>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"0"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"80"

					Exp	=	"23501"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

