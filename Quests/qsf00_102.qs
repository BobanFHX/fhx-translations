<Root>
{
	<Quest>
	{
		Desc	=	"Catch the Lord of Abnormal Skeletons and bring his skull."
		GiveUp	=	"1"
		Id	=	"102"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Lord of Abnormal Skeletons"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"As I heard it, normally skeletons don’t have perception.  There should be something that controls them such as a black wizard or, at least, it should be a Skeleton King or Lord of Skeletons. 





 I don’t know how they came out here from the graves, I am sure that there should be a commander.  If I kill him, other skeletons will be weaker than now.  It is a very important task.  I’ll ask you to perform this task.



 You are in my favour because of how you get the job done.  Bring his skull as evidence that you killed him.  If you do, I’ll forgive you whatever happened before, and there can be something better."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"16"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"101;"
					}
				}
				<Branch0>
				{
					Desc	=	"Okay... I’ll do my best."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"94"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It seems it could be a burden for you to do the task by yourself."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It’s at your discretion.  I can’t do it any more."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you sure??  Okay... from now on we are not related any more."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Lord of Abnormal Skeletons"
			Desc2	=	"Um... is he the one?  He looks so horrible.  Well done.  Now we can slow down their front.  It’s been very helpful."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"66"
					Exp	=	"1510"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169001"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"94"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
