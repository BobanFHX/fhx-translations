<Root>
{
	<Quest>
	{
		Desc	=	"Find 6 lost chests from Vicious Goblin"
		GiveUp	=	"1"
		Id	=	"25"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Sawmill owner's trouble"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh now what to do all my items that I am supposed to deliver have been stolen.  I'm in big trouble such big trouble.  Vagabond Goblin patrol probably took them, I'm sure of it. 





 This isn't first time, and it is starting to anger me.  I want to do a job.  Can you get me my lost chests from vicious Goblins near the Sawmill."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"2"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20208"
					}
				}
				<Branch0>
				{
					Desc	=	"You look very busy, hurry up."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"6"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"22"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You must get all 6 of them, if you loose them I can't deliver them."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am not worthy for this mission."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh no, what should I do.  Time is running out."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Sawmill owner's trouble"
			Desc2	=	"Ah you found them.  You are truly great.  Thinking about not finding them gives me the shivers. 

 My products are the best in all of Rog.  This is for your troubles."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20208"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"40"
					Exp	=	"169"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"163000"
					SItem1	=	"163001"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"6"
					ItemIdx	=	"22"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
