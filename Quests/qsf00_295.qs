<Root>
{
	<Quest>
	{
		Desc	=	"긴꼬리쥐족 부장을 잡아 단서를 조사하라."
		GiveUp	=	"1"
		Id	=	"295"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"긴꼬리쥐족의 음모"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"긴꼬리쥐족은 원래 사악한 녀석들이라.. 고요의습지를 지나가는 여행객을 습격하는 일은 흔한일이지만, 비단쥐일족은 성격들이 온순해서 여행객을 습격하는일은 없었는데.. 점점 비단쥐 일족도 난폭해지고 있다는군요. 그리고. 요 몇일간 여행객들이 말하길, 분명히 긴꼬리쥐 일족으로 보이는 녀석들이 비단쥐족에게 녹색덩어리와 반짝이는 크리스탈을 건내는걸 목격했다는군요. 음.. 그 녹색 덩어리가 바로 용사님이 구해오신 녹색치즈였군요.. 

좀더 조사를 해봐야겠지만, 분명히 어떤 음모가 있는듯해요. 긴꼬리쥐족 부장을 조사해 보면 단서가 나올듯하니 서둘러 주세요."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20296"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"41"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"294;"
					}
				}
				<Branch0>
				{
					Desc	=	"이런 쥐..녀석들.. 어서 가서 단서를 찾아오도록 하겠습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"226"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"서둘러 주세요."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"쥐들도 모르게 일을 꾸민다는데.. 단서를 어떻게 구하겠어요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"수고비를 많이 드린다는데도, 아, 발목은 삐었고.. 이일을 누구에게 부탁하나.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"긴꼬리쥐족의 음모"
			Desc2	=	"이것은 긴꼬리쥐족 부장이 상부에 보고하는 비단쥐족에 대한 분석 보고서군요. 이런 문서를 구해오시다니,  역시.. 믿음직하군요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20296"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"10174"
					Fame	=	"0"
					GP	=	"50"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"226"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
