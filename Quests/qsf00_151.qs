<Root>
{
	<Quest>
	{
		Desc	=	"You must defeat devil's den forest spider's back skin."
		GiveUp	=	"1"
		Id	=	"151"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Windy gorge of peace"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I have read your report that you brought to me. If we didn't help them, the situation would have been much more difficult. I have sent 1 Barbarian battalion to archeological site. I would love to send you more help, but Midland's situation isn't any better. 





   I have heard that Arketlav and Dizamaroo's knights were headed toward Midland, and I also know that some already came to Midland.  I believe that situation is speeding up.  Flower of my brother's tombstone not yet been dead, and yet here I am encounter with this situation…I believe that Belzev's army was summoned at the giant's territory. 





 I have heard that giant's territory is now occupied by Belzev.  I guess this will bring effect to entire region.  Here in  Midland I can see evidence already, lot too long ago at Windy gorge the spiders there had phylactery of sort on their back. 



  If my calculation is correct that means those spiders can only be devil's forest spider from 1000years ago.  I would like you to go there and check it out for yourself.  I want you to bring me its skin on its back."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20197"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"21"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"150;"
					}
				}
				<Branch0>
				{
					Desc	=	"I will start right away."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I got it."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"130"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Man~another bothersome work."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you ignoring the Rog's threat?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Windy gorge's peace"
			Desc2	=	"Hmm...the symbol is just like in the book. 





  Ahh...Then the rumors from above is real? Truly Belzev has appeared?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20197"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"79"
					Exp	=	"2408"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"130"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
