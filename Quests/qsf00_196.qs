<Root>
{
	<Quest>
	{
		Desc	=	"Capture 10 Werewolf Hunters and report it back to Orpen Dewaven."
		GiveUp	=	"1"
		Id	=	"196"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Gorge's Surprise Attack"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Are you going up there?  Then you must be careful. It's very dangerous up there.  Werewolf packs are attacking those that are traveling in the area. 





 That is why it's been a bit rough.  Even the famous army of Rog was destroyed. 





 You have to be especially careful of Werewolf Hunters. Oh yeah, when was it? There was a contact with yellow castle.  They are offering a reward for anyone who can capture 10 Werewolf Hunters, if you are up to it, you should try."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20262"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"27"
					}
				}
				<Branch0>
				{
					Desc	=	"Mmm..10 of them are easy."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"800"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Wow! I guess you are strong, I can't wait to see the results."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I just have to go"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I hope you have a safe travel."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Gorge's Surprise Attack"
			Desc2	=	"Ohyou really did it. You are a great warrior.  I knew there was something different about you when I first saw you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20262"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"95"
					Exp	=	"3837"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
