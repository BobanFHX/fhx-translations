<Root>
{
	<Quest>
	{
		Desc	=	"Deliver wrist band to Elizabeth Ruye"
		GiveUp	=	"1"
		Id	=	"29"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Wrist band delivery"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey! I need to stay here and wash these silk.  I have a favor to ask you. Can you give this wrist band to my mom so that my mother won't worry about me."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20210"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"5"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"28;"
					}
				}
				<Branch0>
				{
					Desc	=	"You are using me too much, but fine I'll do it"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"26"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh!I need to go and quickly wash these."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"You are very rude, you do it yourself."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh..why? Come on help me. Are you going to just leave me? Are you bothered by me?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Wrist band delivery"
			Desc2	=	"I didn't know he was such a good kid.I thought he was very immature.  My kid is all grown up now. Raising a child this moment makes me the happiest ever. Thank you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"26"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20209"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"60"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"162001"
					SItem1	=	"162002"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"26"
				}
			}
		}
	}
}
