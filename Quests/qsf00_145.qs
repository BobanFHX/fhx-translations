<Root>
{
	<Quest>
	{
		Desc	=	"I must deliver the note of authentication to Alex at the Midland bay."
		GiveUp	=	"1"
		Id	=	"145"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Proof of innocence"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"To tell you the truth, I wasn't avoiding captain on purpose, just that I was late and couldn't bump into him. Listen to my story. There is a man I know who lives at the shrine at Arketlav. 



 He was an expert on ancient writings and had an expert opinion about them.  He was loud and we would often have a friendly chat. So I decided to go and see him.  When I show it to him, he immediately called me devil’s spy and called knights, and they put me in a dungeon below. 



    I later found out that it wasn't a treasure map, but a devil's document written in devil writings.  For 3 months I had to endure all types of torture, afterwards, they let me go. I guess they thought that I wasn't a devil's spy or they were too busy with something else.  They were furious when they captured me and they were furious when they let me go. 





   They also told me that they couldn't give back the document.  They told me that they will be keeping it at the Arketlav shrine, and told me that if I needed a proof that it is in the shrine, they would give me a note of authentication.  I wanted the note of authentication, but unfortunately the man was not in Midland.  It is obvious that I'll be thrown into the water or disappears somewhere if I try to get the note of authentication.  That is the reason why I am here stuck. Could you give him this note of authentication and tell my story for me?  Please show this proof to the captain. I must now return."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20245"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"21"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"144;"
					}
				}
				<Branch0>
				{
					Desc	=	"Sorry about that, I'll take care of it for you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"124"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Thank you very much, if you can take care this matter for me, I'll be greatly appreciated."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I do not have that much honor in me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Sniff, Sniff...what am I suppose to do?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Proof of innocence."
			Desc2	=	"So you are saying that you need note of proof? I guess I have done much wrong to him. 





 But you know, thing that he brought was not ordinary.  Phew..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"124"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20246"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"532"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"124"
				}
			}
		}
	}
}
