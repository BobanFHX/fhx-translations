<Root>
{
	<Quest>
	{
		Desc	=	"Bringe den Bericht zum Offizier des Dazamor Orden."
		GiveUp	=	"1"
		Id	=	"236"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Im Namen Gottes (2)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Die Befehle kamen vom Rog Imperium. Es sieht so aus, als ob der Dazamor Orden den Kampfrattenstamm auslöscht, aber lass uns hoffen das es nicht so ist. Bringe bitte den Bericht zum Offizier des Dazamor Orden."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20275"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"235;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"45"
					}
				}
				<Branch0>
				{
					Desc	=	"Ja, mache ich."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Viel Glück."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"200"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich mache das später."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Wie kannst Du "später" sagen? Die Angelegenheit ist dringend!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Im Namen Gottes (2)"
			Desc2	=	"Gute Arbeit. Ich bin gespannt welche Art Instruktionen das Imperium uns gibt?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"200"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20281"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"34"
					Exp	=	"2523"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"200"
				}
			}
		}
	}
}
