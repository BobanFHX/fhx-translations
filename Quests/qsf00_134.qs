<Root>
{
	<Quest>
	{
		Desc	=	"You must get the black magician's secret document from a grave robber who is lurking around the Fog cemetery."
		GiveUp	=	"1"
		Id	=	"134"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Black magician's secret document"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"A couple of days ago, a guy proclaimed to belong to the black magician group that escaped. He wanted protection from us and in exchange he would tell us secrets. We do not know who he is and what he has done. 





 The problem is that he isn't a high ranking magician, so he couldn't tell us much that was in secrecy. According to him, there is a big plan that could decide the faith of Idios. The thing is, there isn't much time. 





 At first, I wanted to ignore such out ragious claims, but I guess he lost the secret document to grave robbers while he was coming here. If you can get it, he said that he could convince us with the proof of his claims. Can you find that secret document from one of the grave robbers?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"19"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes...I will take care of it right away."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"116"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Right...when I looked into his eyes I knew this wasn't going to be some normal matter."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Mmm...those guys look tough."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...I must look for someone else."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Black magician's secret document"
			Desc2	=	"Is this the secret document? This document is tougher than it looks. If you open it with strength, it looks like it's going to ruin it. Man...You worked so hard to get it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"74"
					Exp	=	"2625"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"116"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
