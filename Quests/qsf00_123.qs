<Root>
{
	<Quest>
	{
		Desc	=	"Deliver Jarold's letter to Nick McDold the Black Bridge guard."
		GiveUp	=	"1"
		Id	=	"123"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Death Ruler [Assistance required]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I don't think we can do anymore with just the two of us. We need to request more help. One of the Black Bridge guard is Daijaru's priest named Nick McDold. 





. I'll stamp my insignia in this letter, can you deliver it to him?  He will take care of the rest."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20239"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"17"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"122;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes! I'll deliver it quickly."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"108"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Right, please do it as soon as you can."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"What is the point of delivering it?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What are you babbling about?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Death Ruler [Assistance required]"
			Desc2	=	"What, isn't this the stamp of Daijaru's knight? It is even in a red color. This means it is a 2nd class warning. I got it. 





 I'll take care of the rest."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"108"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20189"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"17"
					Exp	=	"323"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"108"
				}
			}
		}
	}
}
