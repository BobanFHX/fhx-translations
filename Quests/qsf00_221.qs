<Root>
{
	<Quest>
	{
		Desc	=	"Bringe die Gargoylehandschuhe zu Vincent Falken."
		GiveUp	=	"1"
		Id	=	"221"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Die Gargoylehandschuhe"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm... in Ordnung es ist fertig... Es war ein bisschen schwierig; das Leder war so seltsam.. aber der Bärenzahn den Du mir gegeben hast, hat mir großartig geholfen..



Hier nimm ihn."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20264"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"220;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"36"
					}
				}
				<Branch0>
				{
					Desc	=	"Danke schön."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Es war nicht schwer.. nichts zu danken..."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"187"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich nehme es später."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Willst das ich die Handschuhe für Dich aufbewahre?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Die Gargoylehandschuhe"
			Desc2	=	"Oh.. endlich hast Du die Gargoylehandschuhe?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"187"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20279"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"52"
					Exp	=	"1352"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"187"
				}
			}
		}
	}
}
