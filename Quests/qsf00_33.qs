<Root>
{
	<Quest>
	{
		Desc	=	"You need to find lost items stolen by vagabond orc thief. 

 Lost items: Jackson's brand new table, Jackson's money bag, horse feed."
		GiveUp	=	"1"
		Id	=	"33"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Favour from servant"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hi, hello, are you on your way across this Gorge? I am a servant of Aegis' Jackson's merchant, and I was on my way to do his errand. I was on my way from Rog Imperial Castle to buy some items for Jackson, but I was robbed in the middle. 





 Sniff...Can you please help me? It looks like thieves are Vagabond Orcs, a weakling like me can't bring back those items.  Sniff...sniff...sniff..please help me."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"6"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20213"
					}
				}
				<Branch0>
				{
					Desc	=	"It not too difficult of a task."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"1"
							ItemCnt2	=	"1"
							ItemCnt3	=	"0"
							ItemIdx0	=	"30"
							ItemIdx1	=	"31"
							ItemIdx2	=	"32"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Wonderful adventurer! I will never forget this favour."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry, I am busy."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"(he has look of panic) 

 Sniff....sniff...sniff"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"favour from servant"
			Desc2	=	"wow, sir I truly thank you. I am so thankful. Thank you very much, thank you. I will never forget about this favour."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"5"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20213"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"42"
					Exp	=	"389"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"30"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"31"
				}
				<YActNode3>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"32"
				}
				<YActNode4>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
