<Root>
{
	<Quest>
	{
		Desc	=	"Bringe die Kristallstücke zu Vincent Falken dem Alchemisten."
		GiveUp	=	"1"
		Id	=	"226"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Die Kristalle"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ich weiß nicht welchen Nutzen dieser Kristall hat, es sieht so aus als könnte man ihn als Magiepulver benutzen, aber ich bin mir nicht sicher...



Ich habe gehört das Vincent Falken, der Meister der Alchemisten nach Andora kommt. Zeig es Vincent Falken ihn die Stück und frage ihn wie man die einsetzt."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20281"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"225;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"39"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich gehe zu ihm."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"4"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ich weiß das ich Dir glauben kann."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"190"
						}
						<YActNode2>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"191"
						}
						<YActNode3>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"192"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Vielleicht später."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ich kann Dir nicht wirklich glauben..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Die Kristalle"
			Desc2	=	"Hmm? Was ist das... Was..? Magisches Pulver kommt aus diesem Kristall..ich muss diesen Kristall gründlich untersuchen."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"4"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"190"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20279"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"56"
					Exp	=	"2523"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"190"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"191"
				}
				<YActNode3>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"192"
				}
			}
		}
	}
}
