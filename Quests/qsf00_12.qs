<Root>
{
	<Quest>
	{
		Desc	=	"Capture 8 Vicious Goblins and report to Billy Folks."
		GiveUp	=	"1"
		Id	=	"12"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Vicious Goblin"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Those darned little troublemakers!



 Hey, you! I've seen you around. Are you looking for work? Then I have a job for you. Have you heard about those Vicious Goblins? 





 Usually, Goblins live in groups and don't come near any living creatures. But these Goblins must have been kicked out of their group because apparently they are causing all sorts of trouble. Michael Ruin told me that they are terrorizing travellers around his sawmill.



There is a nice reward for anyone who can chase away those little troublemakers. Maybe you can take care of them?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"2"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20180"
					}
				}
				<Branch0>
				{
					Desc	=	"Sounds like a job for me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"8"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"65"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then go and give those dirty Goblins what they deserve!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I'd rather not touch those dirty Goblins."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"What? Are these the kind of heroes protecting the streets these days? Afraid of getting their hands dirty..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Vicious Goblin"
			Desc2	=	"Ha ha.. You did it! I thought this would be too much to handle. Here is your reward. I think it will fit you well."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20180"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"33"
					Exp	=	"169"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"165000"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
