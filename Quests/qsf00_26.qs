<Root>
{
	<Quest>
	{
		Desc	=	"You must find Sawmill owner's child near the Blue Eye Lake."
		GiveUp	=	"1"
		Id	=	"26"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Missing child"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Man, where is he?  He must be hungry.  Hey! Have you seen a child playing around here? He went outside this morning and I can't find him. 





  It might be that he could of gone to north side of Blue Eye Lake due to his curiosity. Can you help me find him? Ah! what should I do with this problem child. I just don't know. 





 When he was going outside he was riding something small.  When you see it, he is probably near it.  He does this a lot.  When you see him, can you make sure to tell him that his mom is looking for him?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20209"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"2"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"25;"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure I'll help you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"23"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Seriously, where is he?  He is upsetting me."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Kids are like that"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Man what should I do? Do I need to go out there myself?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Mission child"
			Desc2	=	"What really? My mother is looking for me? Oh no I'm in a big trouble. What should I do?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"23"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20210"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"35"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"23"
				}
			}
		}
	}
}
