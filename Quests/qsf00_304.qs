<Root>
{
	<Quest>
	{
		Desc	=	"집시촌의 촌장 프리먼 듀에게 원군을 요청하시오."
		GiveUp	=	"1"
		Id	=	"304"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"비적단의 횡포"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"거기 지나가시는 용사님.. 잠시만 이야기를 들어주세요. 저는 저기 위쪽 집시촌에서 수정노리개를 제작하는 사람입니다. 여인의 몸으로 가진건 보잘것 없는 손재주 하나 밖에 없어.. 수정노리개를 만들어 미들랜드항과 교역하면서 살고있죠.. 그런데, 오늘은 이상하게도 미들랜드항에서 한달에 한번 물건을 가지러 오는 짐마찬꾼 모리스 심슨씨가 오지 않아.. 여기까지 나와서 기다리고 있는 중이었죠. 

그런데, 멀리서 말움음소리와 짐마차 바퀴소리가 들려.. 왜이리 늦게 오느냐고 타박하려는 순간 짐마차는 여기를 지나 집시 홉고블린 소굴로 가버리더군요. 짐마차꾼 모리스씨가 앉아서 말을 몰아야 할 자리에는 집시 홉고블린이 말을 몰고 있었어요.. 흑흑..
분명, 이곳으로 물건을 가지러오던 짐마차는 집시 홉고블린 비적단 패거리에게 습격을 당한게 분명해요. 불과 두달전에도 리커드 심슨씨가 집시 홉고블린에게 습격당한 이후로 동생인 모리스 심슨씨가 대신 짐마차를 운행했었는데.. 이제 더 이상은 비적단의 횡포를 참을 수가 없어요. 

저는 모리스씨의 행방을 살피기 위해 짐마차를 추적해야하니.. 집시촌으로 가시는 길이시면, 프리먼 촌장님에게 원군을 요청해주세요. 부탁드립니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20298"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"51"
					}
				}
				<Branch0>
				{
					Desc	=	"어렵지 않은일이군요. 기꺼이 도와드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"감사합니다. 어서 서둘러 주세요. 모리스씨의 생명이 걸려있을지도 모르니.."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"242"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"비적단의 습격이라니.. 괜히 휘말려서 위험한일에 관여하고 싶지 않군요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"시국이 어수선 하다고 하더니. 인심이 이렇게도 나빠진것인가.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"비적단의 횡포"
			Desc2	=	"자네는 여기 사람은 아니구먼.. 여행객인가?? 아니면...."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20299"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"0"
					ItemIdx	=	"242"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"4884"
					Fame	=	"0"
					GP	=	"12"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"242"
				}
			}
		}
	}
}
