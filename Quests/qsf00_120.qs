<Root>
{
	<Quest>
	{
		Desc	=	"Collect 5 small Black Stone Bat's wings and 5 Small Black Stone Scorpion's tails."
		GiveUp	=	"1"
		Id	=	"120"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Grave Keeper's Hobby"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"What business do you have here in this creepy and scary place? What? Heeh heeh, you must really have nothing to do. You shouldn't go on any further - beyond this point it's full of monsters that you can't even imagine. 





 If you really like to look, you have to help me with my hobby. In there, it is full of small Black Stone Bats and Small Black Stone Scorpions. They are tough - even when I was young they were tough to handle.  As time passed, I began to appreciate their beauty, especially bat's wings and scorpion's tails. I use them to make materials. 





 I haven't been in there lately and I am running out of the materials. There are too many nuisances in there.  Since you wanted to go in anyway, if you get me 5 Black Stone Bats wings and 5 Small Black Stone Scorpion's tails, I'll pay you handsomely for them."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"16"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20236"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll do it as a favour."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"5"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"106"
							ItemIdx1	=	"107"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Heh heh, then I'll leave it up to you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I do not go into graveyards."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then just forget it, there are a lot of people who want to do my favour."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Grave Keeper's Hobby"
			Desc2	=	"Hey you got them? Oh you really brought them. I'll be busy toying around with these. 





 Good job. Anyway, you came back alive.  I bet you were a bit bothered by them. Hee hee why don't you rest here? There are still a lot of small graves out there."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"4"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20236"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"66"
					Exp	=	"1510"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"107"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"106"
				}
				<YActNode3>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
