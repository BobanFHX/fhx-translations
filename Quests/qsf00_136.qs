<Root>
{
	<Quest>
	{
		Desc	=	"You must collect 16 different deformed blood sucker bat at north of Calm forest."
		GiveUp	=	"1"
		Id	=	"136"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Key to unsealing"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"To unseal this I need couple of materials. I have most of them...but to get rid of the seal I am missing one important material. 





 Normally we do not use it to seal so we don't have it. When you go north of death forest, you'll see different types of deformed blood sucking bat. 





 They don't react to magic like zombies, but also shows resistance to it. To unseal it I need that blood sucking bat. I will need about 17,18...no about 16 would do. 





 They live in north of Calm forest, if you go there quickly you can get them."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20233"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"20"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"135;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll be back."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"16"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"118"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I worry because they are not easily spotted."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Too far."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then I cannot unseal it."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"key to unsealing."
			Desc2	=	"Oh...you brought it. Alright, let me get started. 





 This should do this and that...mumble..mumble..huh, it's hard again...again this should...blah blah..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20233"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"76"
					Exp	=	"2871"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"16"
					ItemIdx	=	"118"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
