<Root>
{
	<Quest>
	{
		Desc	=	"You must bring wolves to the Palmas taxidermist Cutting Smiths."
		GiveUp	=	"1"
		Id	=	"199"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Palmas' Taxidermist."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I don't think I can take all of these wolves, here are three, this should get you great price if you take them to the taxidermist in the Palmas. Hee hee¡¦





 to tell you the truth I don't like this kind of job.  And that taxidermist is my brother.  You can't turn down your own brother when he asks a favor.  I have 5 and you have 3 and that makes total of 8, 





 go on and see my brother, maybe he'll give you a job."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20263"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"28"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"198;"
					}
				}
				<Branch0>
				{
					Desc	=	"Really? Thank you"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"3"
							ItemIdx	=	"169"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"No thanks is necessary, I should be thanking you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I already have enough to do, I don't want anymore."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah~ really? Then there's nothing I can do."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Palmas Taxidermist"
			Desc2	=	"Hmm really? My brother gave these to you as a reward? Ha ha, unlike him to be so generous.  Anyway your skill of catching animals is outstanding.  What do you say? Do you want to work with me? Hee hee, if you don't want to then there's I can do anything about it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"3"
					ItemIdx	=	"169"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20264"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"24"
					Exp	=	"1243"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"3"
					ItemIdx	=	"169"
				}
			}
		}
	}
}
