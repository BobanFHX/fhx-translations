<Root>
{
	<Quest>
	{
		Desc	=	"Töte 10 Kleine Sumpfspinnen."
		GiveUp	=	"1"
		Id	=	"245"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Spinnenjagd (1)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Du bist kein richtiger Jäger, mit nur einer Fähigkeit für nur eine Tierart .. Ein richtiger Jäger fängt jedes Tier, egal welches. Eine Beute mögen die meisten Jäger nicht, das sind die Spinnen. Das Gefährlichste an Spinnen, sind ihre Netz und ihr Gift. 



Als erstes töte 10 Kleine Sumpfspinnen aus Stillen Sumpf. Schritt für Schritt werde ich Dir dann die Spinnenjagd erklären."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20285"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"40"
					}
				}
				<Branch0>
				{
					Desc	=	"OK, mach ich"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Nimm Dich in acht vor dem Spinnennetz."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1100"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich mag nicht."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Zu schade.. Du versteckst Deine Talente.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Spinnenjagd (1)"
			Desc2	=	"Das war die einfachste Spinnenart, ich dachte mir schon, dass Du es schaffen wirst."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20285"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"32"
					Exp	=	"8888"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
