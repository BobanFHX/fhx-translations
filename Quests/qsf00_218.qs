<Root>
{
	<Quest>
	{
		Desc	=	"Töte einen Gargoyle und bringe die Haut zu Vincent Falken!"
		GiveUp	=	"1"
		Id	=	"218"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Die Haut eines Gargoyles"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hm.. wie kann ich sie bekommen.. wie kann.. ah? Wann bist Du hier angekommen? Ich habe Dich gar nicht bemerkt. Ich bin schwer beschäftigt, ich frage mich wie ich eine Haut eines Gargoyles bekommen kann.



Ah.. Du siehst nach einem guten Kämpfer aus. Kannst Du mir eine Haut besorgen? Ich werde Dich dafür belohnen."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20279"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"35"
					}
				}
				<Branch0>
				{
					Desc	=	"Gargoyle? Das ist leicht."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"20"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"185"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Sag das nicht. Sie sind sehr stark.."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Das ist zu schwer."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Das hätte ich jetzt nicht gedacht, aber wie du meinst."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Die Haut eines Gargoyle"
			Desc2	=	"Oh, Du hast die Haut besorgt, danke. Hier Deine Belohnung, das sollte reichen."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20279"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"18"
					Exp	=	"6259"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"20"
					ItemIdx	=	"185"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
