<Root>
{
	<Quest>
	{
		Desc	=	"You need to bring 12 silk bundles from Brown Stripe spider to make mother's present"
		GiveUp	=	"1"
		Id	=	"28"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Present for mother"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"The reason why I came here is not to play, but to find my mother's gift.  My mother's birthday will be in couple of days. Charles who works at Sawmill told me that Brown Striped Spider's silk can be used to make clothes. 





 But it was really so big and scary that I couldn't go near it. Can you do me a one more favor?  I need 12 bundles of Brown Stripe Spider's silk.  Please?  I beg you!  I want to be a good son, I don't want to be a kid forever, but a good son."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"4"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20210"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"27;"
					}
				}
				<Branch0>
				{
					Desc	=	"Ha ha, you are a good kid, I'll do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"12"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"25"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Oh! you understood me, you are so cool."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I think your thoughts are enough."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Sniff, sniff, you truly are not going to help? Help please."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Present for mother"
			Desc2	=	"(the boy is truly happy) 

 Oh thankyou!!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20210"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"47"
					Exp	=	"270"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"12"
					ItemIdx	=	"25"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
