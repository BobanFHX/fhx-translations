<Root>
{
	<Quest>
	{
		Desc	=	"You must deliver 5 Giant Corpse Scorpion Legs and 5 Midland's Windy Forest Hairy Spider Legs to Elane Bucks."
		GiveUp	=	"1"
		Id	=	"181"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Strange Insect Scientist"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Mmm? Strange.very strange. How come one is so different from the other? I understand that depending on the surroundings it can adapt, but I was here just last year and they were similar. Strange? 





  I am Elane Bucks, I study insects.  Lately some of the insects are dramatically changing.  Can you help me? 





  If you look in Deadman's Land there is Giant Corpse Scorpions.  Also in Midlands Summer Windy Gorge there are Windy Forest Hairy Spiders. Can you bring 5 legs from each insect?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20259"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"21"
					}
				}
				<Branch0>
				{
					Desc	=	"Mmm? That sounds like fun."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"5"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"156"
							ItemIdx1	=	"157"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Yeah, it'll be fun, go ahead and get them for me."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't really want to."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Really? Then forget about it."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Strange Insect Scientist"
			Desc2	=	"Did you bring them? Quickly, let me see them.  I wanted to study them more in detail, so that I could tell whether strange changes are from the environment or some new phenomenon."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"4"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20259"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"76"
					Exp	=	"2871"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"156"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"157"
				}
				<YActNode3>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
