<Root>
{
	<Quest>
	{
		Desc	=	"capture killer croc and bring killer croc's head."
		GiveUp	=	"1"
		Id	=	"155"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] God's Knight 4 (Arketlav)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"That's that, little while ago I received a news, killer croc that has been known as devil's den destroyer appeared near sunrise lake. They were vicious killer croc that lived near devil's den lake. In the past they were let loose at the drinking water to dry them.  Lately within the sunrise black croc there is a rumor that killer croc is among them. 





 To tell the truth I have never personally seen them, but I am referencing it from the history book..  From those who seen them say that they look similar to sunrise croc.  People probably couldn't tell. While I gather my men, please take care of them for me. Kill them and bring their head to me. 





 Everything else I'll take care of it. Rumor has it that it is pretty strong, maybe it might be too much for you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20248"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"23"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"154;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I understood."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I am telling you that there is a limit as to what one man can handle."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"133"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Hmm..I think this is too much for me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm..really? I see, I should look for another person?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"god's knight 4 (Arketlav)"
			Desc2	=	"Good~ is he that big? His head is big as a mountain.  I can’t believe you captured him. 



  I almost gathered all my men.  Only thing that left is to teach them a lesson.  Although it has been short while I thank you for your help. 



  I hope to see you later. Ha ha ha."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20248"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"11"
					Exp	=	"2840"
					Fame	=	"0"
					GP	=	"2"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"170000"
					SItem1	=	"170001"
					SItem2	=	"170002"
					SItem3	=	"170003"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
