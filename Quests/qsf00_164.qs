<Root>
{
	<Quest>
	{
		Desc	=	"You must get information from Marcos and then give it to Dust Canton."
		GiveUp	=	"1"
		Id	=	"164"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Return to Dust Gorge."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Because of your sample I now have proof of their existence.  Great job, 



 this is a book that contains information all about the Lycanthropes weakness, strength and ways to defeat them.  All you have to do is give this to the Dust Canton, then rest will be taken care of."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20251"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"163;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes I’ll take it to him right away."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"141"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Yes, yes do a good job for me."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Other people will come, ask one of them."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you playing with me?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Return to Dust Gorge."
			Desc2	=	"Oh. They are called Lycanthrope, wait and see I'll finish them off.  Ok thanks."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20198"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"141"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"767"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"141"
				}
			}
		}
	}
}
