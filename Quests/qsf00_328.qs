<Root>
{
	<Quest>
	{
		Desc	=	"거대 마녀 거미 20마리를 잡으시오."
		GiveUp	=	"1"
		Id	=	"328"
		NumOfQUNode	=	"2"
		Repeat	=	"1"
		Title	=	"거미사냥꾼[반복]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"오!!

거미사냥의 달인이시군요. 다시 한번 거미사냥 하실려구요?

좋죠... 이번에는 20마리입니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20285"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"247;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"50"
						MinVal	=	"46"
					}
				}
				<Branch0>
				{
					Desc	=	"알겠습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"이제까지 본 거미보다 사냥하기 힘들 겁니다."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"20"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1215"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"더 이상 배우고 싶지 않군요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"마음의 준비가 된다면 다시 찾아오세요."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"거미 사냥꾼[반복]"
			Desc2	=	"역시.. 대단하시군요. 그 많은 거미들을 잡아오다니.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20285"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"68"
					Exp	=	"13078"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
