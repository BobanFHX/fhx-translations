<Root>
{
	<Quest>
	{
		Desc	=	"Get rid of Cain's decendents and 3 black magicians hiding at Horned tiger mountain range."
		GiveUp	=	"1"
		Id	=	"48"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Devil's regret"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Do you know who dark magician Cain is? Cain was the follower of the Devil king Believ.  Believ was the one who almost eliminated Rog a 1000 years ago. He only existed in history and he just wanted to destroy and he wanted to raise a new world after him. Those who are not happy with this world are trying to summon him once again. 





 Cain is also the same type as well. He came as a holy man but went as an evil creature. We'll talk more about it later. Right now we need to make sure Cain's decedents will not make havoc again. As soon as the Aegis knights are ready they will head to the get rid of them, and I would like you to join them. 



According to the report, they must have sniffed out that we were onto them. They moved to a small island at the horned tiger mountain range. Where ever he has retreated to, you must go and find him and get rid of him."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20195"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"11"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"47;"
					}
				}
				<Branch0>
				{
					Desc	=	"Phew...This will be difficult."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"3"
							KillCnt1	=	"1"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"220"
							NPCIdx1	=	"230"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"It looks like the Aegis incident is about to come to an end. According to the patrol, he is a very strong. It might be too difficult to defeat him by yourself and you may need help."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It is still bit tough for me to handle."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you trying to cop out at the last moment?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Devil's regret"
			Desc2	=	"I think Loren's warning has been taken care of. I think we should strengthen zombie defense for awhile. 





 you did a great job."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20195"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"35"
					Exp	=	"845"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"166004"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
