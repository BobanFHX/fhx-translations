<Root>
{
	<Quest>
	{
		Desc	=	"Take Daijaru's commands to Jarold."
		GiveUp	=	"1"
		Id	=	"124"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Death Ruler [ command and execution]"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I think the guard captain here knows what's going on, and I think he is doing all he can to solve the issue.  I just spoke with Daijaru. Arketlav already knew and moved according to it. Arketlav's guys are a bit impatient, they move pre-emptively. 





 They already turned the land upside down because of this insignificant occurance. I don't think this time that will be the case. Even the Daijaru elder thinks that this isn't something to be taken lightly. They want as much information as they can get.  They were showing sensitive reactions to inquiries. 





 We need to wrap up the problem and report back. This is the command from the elder via teleport. Give this to the Jarold."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20189"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"17"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"123;"
					}
				}
				<Branch0>
				{
					Desc	=	"I'm on my way."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"109"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then...take care of it for me."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Let me rest and then go."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Are you tired?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Death Ruler [ command and execution]"
			Desc2	=	"What, Arketlav already knows about it? I guess I'm late, Arketlav already got to it before we did. 





 They were careless, but we were unable to find it out first because we didn't pay attention. The Daijaru elder meeting was simply depressing.. anyway good job."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"109"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20239"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"17"
					Exp	=	"323"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"109"
				}
			}
		}
	}
}
