<Root>
{
	<Quest>
	{
		Desc	=	"Joshua is asking you to kill 10 Small Shadow Bats."
		GiveUp	=	"1"
		Id	=	"185"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Small Shadow Bat"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey young man, this mining town region has never been abandoned like this before.  No vegetation would grow here, drinking water was hard to get due to all the rocks near the town.  300 years ago a count named Cram Midland found out that there were lot of minerals buried here.  After that numerous people came trying to earn their riches, but the Rog government would not allow them to settle here, telling them that this was inhabitable place.  Since then we don't get any support from them. 





  People still try to live as best as they can.  One thing that still bothers us today is the drinking water.  Whenever it rains we would use that water for our drinking water, but Small Shadow Bats started to live here and contaminated the water.  Even today they are a menace to our drinking water. 





   Edgar and Edmont are abandoned mines, there you can find Small Shadow Bats living there.  If you get 10 bats then I think we can be safe for awhile."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20256"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes of course sir, I'll do what you asked."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"680"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ho, ho, ho, thank you Youngman"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Well, isn't there an army in this town?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmmm, the only army that exists is the militant army."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Small Shadow Bat"
			Desc2	=	"What you caught them? Great, I knew that you were good for something like this. You took away my worries, ho, ho, ho, thank you Youngman."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20256"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"87"
					Exp	=	"3072"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
