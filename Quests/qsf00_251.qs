<Root>

{

	<Quest>

	{

		Desc	=	"Bringe das Essen von Sandra Patosi zu Bastian Remun."

		GiveUp	=	"1"

		Id	=	"251"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Essenlieferung"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Warte ... Ich hole das beste Essen, das ich habe!



Hier ... Bring ihm das! Der arme ist bestimmt schon am verhungern."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20077"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"250;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"49"

					}

				}

				<Branch0>

				{

					Desc	=	"Danke."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Nicht der Rede wert. Guten Appetit."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10002"

							ItemCnt	=	"1"

							ItemIdx	=	"206"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich hole es später."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Sag Bescheid, wann immer Du essen möchtest."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Essenlieferung"

			Desc2	=	"Essen! Vielen Dank ... Ich bin fast verhungert."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"20005"

					ItemCnt	=	"1"

					ItemIdx	=	"206"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20286"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"0"

					Exp	=	"2896"

					Fame	=	"0"

					GP	=	"0"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"1"

					ItemIdx	=	"206"

				}

			}

		}

	}

}

