<Root>
{
	<Quest>
	{
		Desc	=	"You should bring a core that is an agent to spread the curse.  It is in the body of the Skeleton Lord after you kill him"
		GiveUp	=	"1"
		Id	=	"111"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Spread of Curse"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I’ve heard news recently that an unknown group revives the dead.  I don’t know why, but the curse will be more effective if the dead are revived. 



 Especially, someone told me that the Lord of Skeletons has something amplifying the power of the curse after it is revived.  If you don’t defeat him, it’s more likely the poison will spread.  Wizard... Your power is strong to extinct the curse.  Go ahead and defeat him and bring me the cursing core."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"5"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20237"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"16"
					}
					<CondNode2>
					{
						Class	=	"32"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"110;"
					}
					<CondNode4>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Hm... it sounds very serious.  I’ll defeat it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"100"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I am looking forward to seeing you again."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am not even aware of this area."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Do you think that’s an excuse?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Spread of Curse"
			Desc2	=	"Wow... He is a lot more enormous than I thought.  If he is more activated... Hew.. I don’t even want to think about it. 



 Good job.. I think it was very hard to defeat him.  Are you the wizard I expected?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20237"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"66"
					Exp	=	"1510"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"156003"
					SItem1	=	"156006"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"100"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
