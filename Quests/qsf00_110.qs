<Root>
{
	<Quest>
	{
		Desc	=	"You should gather 9 teeth from Small Zombie Dogs, and go to Divelo Digriz."
		GiveUp	=	"1"
		Id	=	"110"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Source of a Curse"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Recently, it frequently happens that the spirits of water become violent or unable to be banished due to an unknown curse.  I’ve searched for any way to fix it, but I couldn’t find a way.  Not long ago, I heard that poison of death is spreading over at the ground of the dead.  I think the poison contaminates our spirits of water.  We are about to be overcome by other dark clouds even before we get away from the influence from Kainen Spring.





 If the spirits of the water become worse than now, it'll put the whole of Rog in jeopardy.





 It doesn’t matter whether it was a dog or a wolf.  Please gather 9 teeth from whatever became a zombie, and deliver to Divelo Digriz who is a researcher at the Fog Grave Yard.  He’ll give you accurate information and the solutions."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20226"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"75;"
					}
					<CondNode3>
					{
						Class	=	"32"
						CondNodeType	=	"20002"
					}
				}
				<Branch0>
				{
					Desc	=	"I’ll hurry and go."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"9"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"99"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I’ll leave it to you."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I haven’t finished my task here yet."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Please, come again as soon as you finish your task here."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Source of a Curse"
			Desc2	=	"Who?  Hm... Request from the Queen of Water Spirits.  Huh... How could it be?...  I was also investigating around here because things are so unusual.  Frankly, I don’t have a definite answer.



 The only thing that I am sure of is that the poison from the dead influences the spirits.  I don’t think we can find a solution right away."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20237"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"61"
					Exp	=	"1217"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"9"
					ItemIdx	=	"99"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
