<Root>
{
	<Quest>
	{
		Desc	=	"다크 슬라임 10마리 그리고, 습지 붉은 슬라임 10마리를 사냥하시오."
		GiveUp	=	"1"
		Id	=	"300"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"수습연금술사의 부탁2"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"용사님은 슬라임을 잡아 보신적이 있으세요? 그 물컹물컹하고 허물허물대는 생명체란... 정말이지 어디서 그런 생물이 나온걸까요.. 사실 전 연금술 협회에 소속된 수습연금술사입니다. 
실험중인 조제법이 있어 이것저것 재료를 모아야 하는데. 보시다시피 저는 슬라임을 상대할 만큼 용감하지도 재능이 있지도 않아요. 용사님이 저를 좀 도와주신다면 생각한 조제법을 금방 완성 할 수 있을텐데...어떠세요.

잠시 시간을 내어 도와주실래요?

일단, 작은 붉은 슬라임 시약 5병만 구해주시겠어요?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20297"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"38"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"299;"
					}
				}
				<Branch0>
				{
					Desc	=	"실력없는 연금술사 인줄 알았더니..담즙을 묻혀오도록 하죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"10"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1090"
							NPCIdx1	=	"1095"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"흠뻑 묻혀오세요."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"그런 더러운 담즙을 묻히고 오라니.. 다른 사람을 찾아보시죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"......"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"수습연금술사의 부탁2"
			Desc2	=	"이런 힘든일을 기꺼이 해주시다니.. 너무 고마워요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20297"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"8888"
					Fame	=	"0"
					GP	=	"22"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
