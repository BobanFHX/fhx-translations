<Root>

{

	<Quest>

	{

		Desc	=	"Jage die Felsenspinne und bringe mir 10 Beinhaare"

		GiveUp	=	"1"

		Id	=	"276"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Spezielle Zutat"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Das Land der Giganten birgt so viele verrückte Monster und normale Rüstungen können ihnen nicht stand halten. Deshalb forsche ich seit einigen Jahren nach einer speziellen Rüstung. Ich habe die Beinhaare der Felsenspinne gefunden. Sie festigten das Metall und die Rüstung aus diesem Metall ist doppelt so stark wie eine Normale.



Also, mein Angebot ist Folgendes: Wenn Du mir 10 Beinhaare der Felsenspinne bringst, werde ich Dir eine spezielle Rüstung machen."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"2"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20053"

					}

					<CondNode1>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"50"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich besorge sie."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"10000"

							ItemCnt0	=	"10"

							ItemCnt1	=	"0"

							ItemCnt2	=	"0"

							ItemCnt3	=	"0"

							ItemIdx0	=	"211"

							ItemIdx1	=	"-1"

							ItemIdx2	=	"-1"

							ItemIdx3	=	"-1"

							SvrEventType	=	"1"

						}

						<YActNode1>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Vermutlich wirst Du dann die stärkste Rüstung in der ganzen Gegend haben."

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ist mir zu schwer."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Gut, ich verstehe.. Ich werde einen Anderen finden müssen.."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Spezielle Zutat"

			Desc2	=	"Oh ... das ist mehr als genug für ein paar Rüstungen."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"3"

				<CondNode0>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"1"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20053"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"13"

					Exp	=	"17137"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"10"

					ItemIdx	=	"211"

				}

				<YActNode2>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

