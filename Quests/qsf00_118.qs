<Root>
{
	<Quest>
	{
		Desc	=	"Capture 6 Shadow Orc Warrior Leaders and 8 Dark Orc Messengers."
		GiveUp	=	"1"
		Id	=	"118"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Until the End of My Life"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey you, while I was betting, I was watching you. You are much stronger compared to me. No matter how long I hunt them, my anger will never leave me. 





. Whenever I dream about that event, it torments me. The next day my mind is full of anger when I hunt them.  It's funny to ask someone else to get my revenge, but if you have any sympathy for me you can capture Shadow Orc Warrior Leaders and Dark Orc Messengers. 



 Truthfully, whenever I am hunting Dark Orcs, when they show up, I turn tail and run.  It shames me to say that to you. Please capture 6 Shadow Orc Warrior Leaders and 8 Dark Orc Messengers."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20238"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"18"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"116;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I'll show them your anger."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"6"
							KillCnt1	=	"8"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"450"
							NPCIdx1	=	"455"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hee hee thanks."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Revenge only breeds more revenge."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Do you think you can understand my feelings?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Until the End of My Life"
			Desc2	=	"Ahh that feels good.  It was great when I saw them fall - 15 years of weight is lifted from me. Thank you, truly I thank you. I think I can fall asleep peacefully tonight."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20238"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"71"
					Exp	=	"2391"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
