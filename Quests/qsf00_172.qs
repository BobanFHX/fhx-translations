<Root>
{
	<Quest>
	{
		Desc	=	"Bring back 5 Item Checklists from Lycanthrope."
		GiveUp	=	"1"
		Id	=	"172"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Belzev Convoy 2 (Item Checklist)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"According to the record, from what I can tell the Lycanthropes might be meddling with some dark force or magic. I think they are receiving some type of orders from whoever is controlling them. 



   I think Kogai was trying to get the order and that is why he was attacked.  I was wondering if you could capture the Lycanthrope that has the order, and bring it back to me?  I was thinking about 5 of them will do just to be sure, what do you think, can you handle it?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20198"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"25"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"171;"
					}
				}
				<Branch0>
				{
					Desc	=	"I guess I'll try"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"149"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then be on you way."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"well...it looks a bit out of my league."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...really? I understand."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Belzev Convoy 2 (Item Checklist)"
			Desc2	=	"Is this it? It looks very unprofessional.  This is made with special sheepskin and some form of strange writings.  Hmm...I wonder what kind of sheepskin this is? It has a special pattern and texture as well."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20198"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"90"
					Exp	=	"3315"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"149"
				}
			}
		}
	}
}
