<Root>
{
	<Quest>
	{
		Desc	=	"Töte 10 Hobgoblin-Mörder und 10 Hobgoblin-Kriegsherren im Norden von Andora!"
		GiveUp	=	"1"
		Id	=	"210"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der Verräter Andoras (5)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Wir Templer sammelten auch Informationen über den Verräter die neueste Information ist, dass er sich bei den Kobolden versteckt, aber das es Edward ist.. ich bin verwirrt...



Wir sollten ihn töten bevor der Teufel wieder aufersteht lässt. Sobald die Anführer Hobgoblins tot sind, werden sie führerlos sein. Wenn Du 10 Hobgoblin-Mörder und 10 Hobgoblin-Kriegsherren tötest, können wir die anderen leichter überwältigen."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20275"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"209;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"36"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde sie töten."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hoffentlich, bist Du auch zuverlässig!"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"10"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"995"
							NPCIdx1	=	"1000"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Das ist mir zu schwer."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Mann.. Du bist ein Feigling!"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der Verräter Andoras (5)"
			Desc2	=	"Danke für Deinen Einsatz.. Wir werden sie nun auslöschen."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20275"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"9573"
					Fame	=	"0"
					GP	=	"3"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169003"
					SItem1	=	"169004"
					SItem2	=	"169005"
					SItem3	=	"169006"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
