<Root>
{
	<Quest>
	{
		Desc	=	"Capture the Ahgol Goblin spy and find pearl of dimension."
		GiveUp	=	"1"
		Id	=	"149"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Goblin spy"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"All the sheepskin is complete, and I translated part of it. 

To brain wash Ahgol Goblin, devil's spy must of hidden amongst the Goblins. 



 Goblins are already aggressive and if they are brain washed by devil it would make things out of control. 



 I think you need to get rid of him.  He is the source of our information.  Hee hee hee...can you do it? Huh?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20246"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"22"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"148;"
					}
				}
				<Branch0>
				{
					Desc	=	"I take full responsibility for what I started."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"128"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Right, of course, however if he can become like Goblin, he is pretty skillful man..."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"This incident seems strange...I don't like it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...really stupid."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Goblin spy"
			Desc2	=	"What the? Is this from him? What is it? I this strange feeling that I have seen this before, and I do not like this feeling. 





   Anyway you've done well.  I'll call you later if anything else happens.  Therefore Ahgol Goblin is taken care of?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20246"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"82"
					Exp	=	"2619"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"128"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
