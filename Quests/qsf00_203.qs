<Root>
{
	<Quest>
	{
		Desc	=	"Gehe nach Andora und finde ein Hinweis warum die Familie Palmas getötet wurde. "
		GiveUp	=	"1"
		Id	=	"203"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Seelenhandel (1)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hast du irgendwelche Informationen über die Auslöschung der Familie Palmas? Es kann nicht einfach gewesen sein diese Familie auszulöschen.. es muss einen bestimmten Grund geben, doch leider kenne ich ihn nicht und es ist nicht leicht ihn raus zu finden.


Kannst Du mir helfen? Ich vermute die Andora Diebe haben damit was zu tun. Bitte gehe zu ihnen und suche nach Hinweisen warum die Familie getötet wurde."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20275"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"30"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde es versuchen."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"175"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Der Duke Palmas wird Dir vom Himmel aus dankbar sein."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich habe keine Zeit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hu.. wie kannst Du meine Bitte ignorieren? Du bist sehr unhöflich."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Seelenhandel (1)"
			Desc2	=	"Oh, ich danke Dir wirklich sehr. Du hast geschafft was hunderte von Rittern nicht geschafft haben."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20275"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"2"
					Exp	=	"4636"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"175"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
