<Root>

{

	<Quest>

	{

		Desc	=	"Töte den Zwergork-Schamanen und bring seinen Kopf zu Fabian Schneider."

		GiveUp	=	"1"

		Id	=	"262"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Das Schützen Training"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Angesichts Deiner natürlichen Schnelligkeit auf dem Schlachtfeld bist Du ein Bogenschütze. 



Als Bogenschütze solltest Du eine große Ausdauer haben, weil Du diese für das Schiessen brauchst. Ich weiß, Du musstest durch beträchtliche Ausbildungen; aber nicht so anspruchsvolle wie ich sie erwarte. 



Wenn du diese Aufgabe bestehen willst, und Deinen eigenen Weg gehen kannst, dann töte einen Zwergork-Schamanen und bring mir seinen Kopf."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20288"

					}

					<CondNode1>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"50"

					}

					<CondNode2>

					{

						Class	=	"4"

						CondNodeType	=	"20002"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich nehme die Aufgabe an."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"10000"

							ItemCnt0	=	"1"

							ItemCnt1	=	"0"

							ItemCnt2	=	"0"

							ItemCnt3	=	"0"

							ItemIdx0	=	"207"

							ItemIdx1	=	"-1"

							ItemIdx2	=	"-1"

							ItemIdx3	=	"-1"

							SvrEventType	=	"1"

						}

						<YActNode1>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Kämpfe erneut allein; das ist immer schwer und hart."

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich mache es nicht."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Keine Verbesserung, kein Gewinn."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Das Schützen Training"

			Desc2	=	"Ich habe mich nicht in Dir getäuscht. Ich wusste Du würdest es schaffen."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"3"

				<CondNode0>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"1"

				}

				<CondNode1>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20288"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"13"

					Exp	=	"17137"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"10003"

					ItemCnt	=	"1"

					ItemIdx	=	"207"

				}

				<YActNode2>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

