<Root>
{
	<Quest>
	{
		Desc	=	"You must bring convoy order to Duston Canton."
		GiveUp	=	"1"
		Id	=	"174"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Belzev Convoy 4 (Joshua's Bag)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Mmm...let me see? I think like you said this is a order.  It is written in magic, and those who are under the control of The Devil's Den do not need to read them.  They simply have the order cast into their head. 





  Well what it says here is that refined silver and Valieum will be moved, and Lycanthrope Franz will give an order to protect it.  About 20 of them will be there.  It looks like they will be traveling East of Dust field. 



  I wrote the information down and this is where I put the original and translated documents, in this bag.  Bring this to Duston.  Although he is bit thick, his desire to protect his town is second to none."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20256"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"25"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"173;"
					}
				}
				<Branch0>
				{
					Desc	=	"I understand, thank you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"151"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Well don't thank me, I am glad to have your help."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I will do it later."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...ok then, I'll see you later."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Belzev Convoy 4 (Joshua's Bag)"
			Desc2	=	"You mean they will be moving all the minerals that all our Dust gorge miners worked so hard to get? 





  Arrg...I will not allow them to get away with this.  If they think that they could get away with this, they are sadly mistaken."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"151"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20198"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"22"
					Exp	=	"866"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"151"
				}
			}
		}
	}
}
