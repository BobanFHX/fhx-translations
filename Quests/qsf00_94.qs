<Root>
{
	<Quest>
	{
		Desc	=	"Collect 5 Deformed Zombie skins."
		GiveUp	=	"1"
		Id	=	"94"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Deed Done by Necromancer"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm anyway what were you talking about? You seen the dead walking around? Did you see some mirage? A necromancer revived them? Tell me if that makes sense. Sometimes captured grave robbers told me about that nonsense. Are you doing the same? 





 What? You want me to go and check it out for myself? Hah fine then. If you really saw them, bring me 5 of those walking dead.  Then Ill talk to you again."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20191"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"93;"
					}
				}
				<Branch0>
				{
					Desc	=	"I¡¯ll show you the proof."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"85"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Heh if you¡¯re trying to lie to me, you¡¯ll be sorry."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Fine, why don¡¯t you take the time and check it out for yourself?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Heh you must be trying to bury this matter."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Deed Done by Necromancer"
			Desc2	=	"~Sniff~ what is this smell? What the this must be the skin of the undead. 



 It is true that there are walking dead at the graves.  I¡¯m getting the shivers. I must figure something out."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20191"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"61"
					Exp	=	"1217"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"85"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
