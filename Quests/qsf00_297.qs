<Root>
{
	<Quest>
	{
		Desc	=	"긴꼬리쥐일족 수장 15마리를 처치하라."
		GiveUp	=	"1"
		Id	=	"297"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"해충 박멸"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"용사님이 암살자와 장로를 처치하러 가신동안.. 마법통신병을 통해 로그황실과 연락을 했습니다. 로그황실에서는 비단쥐족의 상태와 긴꼬리쥐족의 음모를 이미 파악하고 있는 상황이었습니다. 그리고, 황제의 명으로 고요의 습지에서 생활하는 모든 설치류의 종족으로 부터 시작되는 역병을 막기 위해서 쥐족말살을 명하셨습니다. 

긴꼬리쥐족은 용사님이 어느정도의 수를 줄여주었으나, 수장녀석을 죽여야만 그 수가 확연히 줄어들듯 합니다. 마지막으로, 긴꼬리쥐족 수장 15마리를 처치하고 돌아와 주십시요. 이에 상응하는 보상으로 보답하겠습니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20296"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"44"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"296;"
					}
				}
				<Branch0>
				{
					Desc	=	"오호.. 상응하는 보상이라..귀가 솔깃해지는군요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"15"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1195"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"시오니스 대륙을 위협하는 세력을 처치해주시는 대업에 솔선수범해주시는 모습이 너무 늠름하시군요."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"이번만은 정말 저에겐 힘든일이군요. 정중히 거절합니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"늘어만 가는 설치류를 더 이상 방치해서는 안되는데... 이일을 어쩐다."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"밝혀지는 음모"
			Desc2	=	"역시, 대단한 실력이시군요. 용사님 정도의 실력이라면.. 수장을 잡아들이는 일쯤은 쉬울듯 하군요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20296"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"10858"
					Fame	=	"0"
					GP	=	"54"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
