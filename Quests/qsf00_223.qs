<Root>
{
	<Quest>
	{
		Desc	=	"Bringe den Brief vom Offizier von Akedrav zum Offizier von Dazamor."
		GiveUp	=	"1"
		Id	=	"223"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Edwards Aufenthaltsort"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh Mann... wir haben Edward verloren! Den Schurken, der den Seelenvertrag mit dem Teufel geschlossen hat! Es sieht so aus als sei er zum stillen Sumpf geflüchtet, während wir die Goblins jagten. Aber er kann sich nicht ewig verstecken.



Hier ist der Auftrag an Dazamor vom Sumpf. Wenn Du diesen Brief zum Offizier des Dazamors Ordens bringst, werden die sich darum kümmern. Der stille Sumpf liegt auf der Nordseite der Stadt."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20275"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"37"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"210;"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde ihn liefern."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Es ist dringend. Beeil Dich!"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"189"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich habe keine Lust."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Willst du nicht die Auferstehung des Dämonen Lord verhindern?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Edwards Aufenthaltsort"
			Desc2	=	"Hmm.. das ist ein Brief vom Offizier der Akedravs Ordens?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"189"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20281"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"52"
					Exp	=	"2383"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"189"
				}
			}
		}
	}
}
