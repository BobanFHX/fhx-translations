<Root>
{
	<Quest>
	{
		Desc	=	"Find the traitor Roy Philter from Sunrise Lake."
		GiveUp	=	"1"
		Id	=	"144"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Traitor Roy"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey, spirited Youngman, you look like you have lot of spirit.  Would you be interested in my quest? Youngman like you would be interested in this lucrative tale from me.  As you can see, I lived pretty rough when I was young. I used to be called Captain Jack, I had band of men of my own who worked under me. 



   It's only a tale from the past. I wanted to talk about a trip I took at the land of the giants. Was it last year? I took a trip around the content for my retirement with my men. Then I found a unusual piece of paper at the land of the giants. You can't dismiss the sense of sea people. 



  In that paper it was filled with ancient writings and there were also a map, then all of a sudden it gave me a feeling that this was it.  To me it was a this will lead to treasures.  While I was perplexed by the strange writings, one of my men named Roy Philter told me that he could read it and translate it.  I haven't seen him for couple of months after that incident. 





  I guess he wanted to keep those treasures for himself. My men looked everywhere for this guy, but couldn't find him.  Someone told me that he saw him somewhere at the Sunrise Lake area. If you help me capture him, I make sure to give you handsome reward for you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"22"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20244"
					}
				}
				<Branch0>
				{
					Desc	=	"Well...it's an intriguing tale."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Wasn't it? Good luck."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"123"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Do you think I'll believe that tale?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Have you been fooled all your life? Huh?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Traitor Roy"
			Desc2	=	"huh? Oh no I have been found! No, no...I am not a traitor, spare my life...please"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"123"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20245"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"532"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"123"
				}
			}
		}
	}
}
