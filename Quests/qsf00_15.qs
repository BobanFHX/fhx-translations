<Root>
{
	<Quest>
	{
		Desc	=	"Gather 3 Small Brown Bear hearts for Edmond Cage."
		GiveUp	=	"1"
		Id	=	"15"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Bear's heart"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"How am I supposed to get these hearts...? If anything happens to my daughter.. my daughter.. sniff, sniff! 



  My beautiful daughter has become severely ill. I'm afraid she is dying... The alchemist told me that Small Brown Bear hearts have the power to heal all illnesses. I asked some strong warriors to help me out, but they just turned their heads. I'm afraid we are running out of time. And even if you could catch them, their heart is so fragile that it will immediately become useless. Only the freshest hearts will do.



  Are you brave enough to gather 3 Small Brown Bear hearts? The bears are usually around Nymph Lake, not far from Belziev's ruins. Please help me out and save my daughter. I will be forever grateful."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20037"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"4"
					}
				}
				<Branch0>
				{
					Desc	=	"I will save your daughter."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"3"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"13"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Please take care. My daughter's life is in danger..."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry, but I have other business to take care of."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ah! Is there any brave warrior who can save my daughter?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Bear's heart"
			Desc2	=	"You did it?! You really came back to save her? Wow! sniff, sniff, sniff! Thank you. It feels like my dream has come true. My daughter is saved! I truly thank you with all my heart."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20037"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"37"
					Exp	=	"270"
					Fame	=	"0"
					GP	=	"3"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"161000"
					SItem1	=	"161001"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"3"
					ItemIdx	=	"13"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
