<Root>
{
	<Quest>
	{
		Desc	=	"Töte die Sumpffledermäuse und sammele drei Kristallstücke."
		GiveUp	=	"1"
		Id	=	"225"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Kristallstücke"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Entschuldige die Zweifel an Dir, aber in der letzten Zeit laufen hier so einige rätselhafte Gestallten rum.

Wir töten seit einiger die Sumpffledermäuse um Kristallstücke zu bekommen. Diese scheinen sehr wertvoll zu sein. Leider sind meine Männer mit der Verteidigung gegen die Monster beschäftigt.



Könntest Du Sumpffledermäuse töten und uns 3 kleine Kristallstücke besorgen."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20281"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"224;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"38"
					}
				}
				<Branch0>
				{
					Desc	=	"Ja, ich werde es tun."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"1"
							ItemCnt2	=	"1"
							ItemCnt3	=	"0"
							ItemIdx0	=	"190"
							ItemIdx1	=	"191"
							ItemIdx2	=	"192"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ich glaube an Dich.. aber bitte sei vorsichtig."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Keine Zeit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Was für eine arrogante Person, verweigert meine Bitte.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Kristallstücke"
			Desc2	=	"Sind das die Kristallstücke? Ich muss sie gründlich untersuchen."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"5"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20281"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"28"
					Exp	=	"7706"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"191"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"190"
				}
				<YActNode3>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode4>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"192"
				}
			}
		}
	}
}
