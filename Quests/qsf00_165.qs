<Root>
{
	<Quest>
	{
		Desc	=	"Collect 3 rough Valieum Stones."
		GiveUp	=	"1"
		Id	=	"165"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Gorge's Violated Man."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Today is not a lucky day.  Besides me 4 or 5 people were attacked.  Midland,s mines use to have one of the few Valieum mines in Edios. Due to over mining, it's now just a shell of what it used to be.  There are still some people left around the mine, you can see a few miners go in and out of that place. 



 Even I found 3 Valieum stones that are as big as my head.  Out of nowhere Dust Orcs came and hit me in the back of the head and took my stones! Dust Orcs do not know how to use these stones.  They can't differentiate a worthless stone from a valuable stone.  But lately they started steal these stones. 



 Maybe they learned how to cut them or something. My daughter could have had a wedding with those stones. They were great quality stones. Can you catch those Dust Orcs and then return those 3 Valieum stones to me?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20252"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"23"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Those evil thieves, I'll bring them back to you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"3"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"142"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Please return them to me."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"It looks like it's not a job for me."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"If you say so, then I will not ask you any further."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Gorge's Violated Man"
			Desc2	=	"Oh you found them, you found the right ones.  These are the stones I lost.  Because of you my daughter will now be able to get married. Thank you."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20252"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"84"
					Exp	=	"3692"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"3"
					ItemIdx	=	"142"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
