<Root>
{
	<Quest>
	{
		Desc	=	"Deliver a letter that has spirit's pieces to Deadman's Holy Land's Debelo Degrez."
		GiveUp	=	"1"
		Id	=	"113"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Cursed Forest's Researcher"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I have assurance about myself, but I'm not really sure yet. I think it's better to listen to the professionals.  I think you should go to Deadman's Holy Land. 





 If you go to Deadman's Holy Land, there is Debelo Degrez who is an expert in chemistry and a researcher. He is usually at the southern part of the Cursed Forest. I think you can get further information. I never seen any pieces like this before. Let me leave this up to you."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20230"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
					<CondNode2>
					{
						Class	=	"512"
						CondNodeType	=	"20002"
					}
					<CondNode3>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"81;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes, I’ll get the advice from him."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"101"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then be careful."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"You should find out more about it Speeler."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"That’s impossible, I can only do so much."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Cursed Forest’s Researcher"
			Desc2	=	"Ahh I think something happened at Blue Gorge. Even here at Deadman’s Holy Land, there are things that we can't understand happening. I don't know if the same things are happening all over the continent."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20237"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"101"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"15"
					Exp	=	"220"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"101"
				}
			}
		}
	}
}
