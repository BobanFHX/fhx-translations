<Root>
{
	<Quest>
	{
		Desc	=	"흉폭한 골짜기 곰 20마리를 제거하시오."
		GiveUp	=	"1"
		Id	=	"291"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"겁없는 녀석들"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"나는 저기 언덕너머에 살고있는 샘슨이라고 하네. 저 목책너머로 가면, 샐리온 감옥으로 가는 길이지.

아. 샐리온 감옥은 이디오스 대륙에서 범죄자와 이교도들을 격리수용하는 곳이지. 그런데 말일세, 보시다시피 여기를 지키던 샐리온 수비병들은 어디로 가버린건지 몇달동안 보이지도 않고, 한달에 한번 샐리온과 팔마스를 교역하던 짐마차 상단도 보이지를 않는구만.. 어찌되었든, 지금은 산속에서만 서식하던 곰녀석들이 인적이 뜸해지니 여기까지 내려와서 먹을 것을 찾고있어서 조만간 우리집 근처까지 올까봐 내심 두렵기도 하구만..

자네가 나를 도와서 이 곰탱이 녀석들을 좀 쪼차내주지 않을텐가? 일단 저기 보이는 흉폭한 골짜기 곰 20마리만 없애버리게나.."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20295"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"32"
					}
				}
				<Branch0>
				{
					Desc	=	"마땅히, 바쁜일도 없으니.. 도와드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"20"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"945"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"고맙네."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"곰도 먹고 살아야죠. 조금 돌아서 다니세요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"사방이 곰탱이들인데 어디로 돌아서 다니란 말인가..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"겁없는 녀석들"
			Desc2	=	"자네. 보기보다 실력이 꽤 좋구만. 좋아..음."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20295"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"5572"
					Fame	=	"0"
					GP	=	"11"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
