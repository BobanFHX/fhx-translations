<Root>
{
	<Quest>
	{
		Desc	=	"You must capture Dust Orc Magician and Dust Orc Warrior Commanders, and then find the 3 bundles of documents."
		GiveUp	=	"1"
		Id	=	"177"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Finding Lost Document"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"They were following me so closely that I had to throw my bundles of documents. And the Dust Oaks took them. The truth is that I should be guarding that information with my life, but if I die all the secrets will be buried with me. I usually work alone so I don't have a companion.  They just went after me and didn't care about the documents. 



 I guess they received orders to kill me no matter what.  Anyway, even if we stopped the scouts without the document it will all be in vain.  I fortunately over heard them saying something about dark cousins and something about Palmas.  I am worried that something might have happened there. 



  I think either Dust Orc Magicians or Dust Orc Warrior Commanders may have the documents.  Can you find them for me? There are 3 bundles."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20257"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"26"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"176;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"If it is an important document then I must find it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"3"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"152"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Phew~I am glad to meet someone who is so nice and helpful."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am sorry, but you might be on your own."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm...I understand. I'll do it myself."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Finding Lost Documents"
			Desc2	=	"Ah~ you found them.  Thank you.  Let me see.  These are the right ones.  Phew~ now I can give them my report. 





  I will not head to Palmas...huh?...what...Str...an...ge?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20257"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"92"
					Exp	=	"4642"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"3"
					ItemIdx	=	"152"
				}
			}
		}
	}
}
