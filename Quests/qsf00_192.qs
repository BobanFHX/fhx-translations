<Root>
{
	<Quest>
	{
		Desc	=	"Capture 10 Dust Hobgoblin"
		GiveUp	=	"1"
		Id	=	"192"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Gorge Destroyer"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Phew~ 5 were attacked today, did you hear about it? What? You don't know about it? Now I see that you are not from around here.  Then of course you don't know.  You know at least that there are militants around here? Do you know the reason for these armies in the first place? 



   They are here because of the menace.  We call them the Dust Hobgoblin tribe.  They don't like anyone entering their land. 





  Because of that in the beginning we had to suffer a lot.  They were pretty tough and the empire didn't send any help, so what could we do?  The only thing we could do is to fight them ourselves.  So that is why we had to gather our own militants.  Do you understand? Even still they continue to attack our town.  That is why 5 of us were lost.  Sigh~ so I thought I can't let this go on like this, what do you think, do you want to join me? 



  Since they killed 5 of us, I think it is only fitting that we double their treachery.  Can you go ahead and kill 10 Dust Hobgoblin?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20112"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmm..revenge this might make me feel good."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"10"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"705"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"The feeling will be mutual."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Revenge will breed more revenge."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Don't make me laugh, if you're going to say things like that then just go on."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Gorge Destroyer"
			Desc2	=	"Did you destroy them?  Really?  I wonder if they know that I am getting my revenge."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20112"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"87"
					Exp	=	"3072"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
