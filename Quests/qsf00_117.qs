<Root>
{
	<Quest>
	{
		Desc	=	"Collect 6 shadow Orc Thief's heads."
		GiveUp	=	"1"
		Id	=	"117"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Shadow Orc Thief's Neck"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Why do I keep thinking about 15 years ago? Ahh I just cant forget them.  It's so unfair and pathetic.  I will not forget every single Dark Orc... 





 until my dying day.  Ahhh.  Hey - the road you're taking is very dangerous, you can take an easier road.  All you have to do is bring 6 Dark Orc Thief heads to me. I want them as my dinner."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20238"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"15"
					}
				}
				<Branch0>
				{
					Desc	=	"You must have a sad story."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"6"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"104"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Ahh honey Rolel. Today your image is especially bothering me."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ahh~ you're too noisy. You're not the only one with problems."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Heh ~sniff~"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Shadow Orc Thief's Neck"
			Desc2	=	"Hee who are these you are the ones who give me pain every night ha ha ha. I'll be hunting you every day of my life."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20238"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"64"
					Exp	=	"1359"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"6"
					ItemIdx	=	"104"
				}
			}
		}
	}
}
