<Root>
{
	<Quest>
	{
		Desc	=	"Capture contaminated Nieas and find 7 water spirit."
		GiveUp	=	"1"
		Id	=	"36"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Water spirit"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"To tell you the truth, I use to work here as a medicine man long ago.  I used to do a different type of work in Aegis Fortress until my daughter was ill with an unusual illness. When I came here for a cure, the power of the water was already gone.  I am also in dispair, however all is not lost. 





 Although it is cursed, the Nieas is a spirit.  When you catch them they make the water clean. 





  If you have any talent do you want to go to Kinen Spring and bring some water with cursed Nieas spirit? About 7 would do, if you could obtain it not only your friend can be healed, but my daughter as well."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20216"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"7"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"35;"
					}
				}
				<Branch0>
				{
					Desc	=	"Really? I'll try it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"7"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"35"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I wish you luck."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Sorry, I must go now"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm you just want to go?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Water spirit"
			Desc2	=	"Oh  you've done well.  You brought them in an animal skin, this might make it difficult for me to make some medicine.  For now this place would do. 





  Hmm I still need a medicine bottle to mix some medicine."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20216"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"42"
					Exp	=	"389"
					Fame	=	"0"
					GP	=	"6"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"7"
					ItemIdx	=	"35"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
