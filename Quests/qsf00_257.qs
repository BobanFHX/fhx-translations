<Root>

{

	<Quest>

	{

		Desc	=	"Töte 20 Oger Saboteur"

		GiveUp	=	"1"

		Id	=	"257"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Kopfgeldjagd (6)"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Würdest Du dich an den Oger Saboteur versuchen, die noch niemand erfolgreich bekämpft hat? Es gab schon einige Kopfgeldjäger vor Dir, aber die waren ohne Erfolg. 

			

			Ich wollte schon solange warten, bis die kaiserliche Armee kommt, aber Du siehst aus als ob Du es schaffen könntest. Wie wär's?"

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20287"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"256;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"58"

					}

				}

				<Branch0>

				{

					Desc	=	"Lass es mich versuchen."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Sei vorsichtig. Sie sind wirklich stark.."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10000"

							KillCnt0	=	"20"

							KillCnt1	=	"0"

							KillCnt2	=	"0"

							KillCnt3	=	"0"

							NPCIdx0	=	"1500"

							NPCIdx1	=	"-1"

							NPCIdx2	=	"-1"

							NPCIdx3	=	"-1"

							SvrEventType	=	"0"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Ich gebe auf."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Verstehe.. Leben ist wichtiger als Geld."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Kopfgeldjagd (6)"

			Desc2	=	"Toll ... Du bist so Großartig! Du kannst Dich selbst zum Ritter ernennen."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20287"

				}

				<CondNode1>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"0"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"32"

					Exp	=	"30721"

					Fame	=	"0"

					GP	=	"3"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"101045"

					SItem1	=	"101046"

					SItem2	=	"101047"

					SItem3	=	"101049"

				}

				<YActNode1>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

