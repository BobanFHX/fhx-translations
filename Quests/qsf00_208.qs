<Root>
{
	<Quest>
	{
		Desc	=	"Töte die Abtrünnigen um Edwards Tagebuch zu bekommen !"
		GiveUp	=	"1"
		Id	=	"208"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der Verräter Andoras (3)"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ah.. Edward.. warum hat er uns verraten.. Der Graf liebte ihn so sehr aber er.. ah.. Edward.. Edward..warum..?


Vielleicht erfahren wir mehr über seinen Grund warum er uns verraten hat, wenn wir sein Tagebuch lesen. Er erzählte mir, er könne alles seinem Tagebuch erzählen. Allerdings, ist es nicht in Edwards Haus. Vielleicht haben die Abtrünnigen sein Buch als Beute mitgenommen.

Ich kann Dir nicht genau sagen, welcher Abtrünniger das Buch geklaut hat. Versuche es zu finden, damit wir Licht in das Dunkel bringen und Edwards tun verstehen."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20276"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"207;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"33"
					}
				}
				<Branch0>
				{
					Desc	=	"Ich werde es finden."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"179"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Das muss einen Grund haben.."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Das ist zu schwer."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Was redest Du? Bist Du verrückt?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der Verräter Andoras (3)"
			Desc2	=	"Oh gut Du hast es gefunden. Mal sehen ob wir den Grund für Edwards handeln finden..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20276"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"12"
					Exp	=	"5572"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"179"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
