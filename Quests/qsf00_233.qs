<Root>
{
	<Quest>
	{
		Desc	=	"Töte den Bösen Sumpfschleim und sammele den Saft dieses Schleims."
		GiveUp	=	"1"
		Id	=	"233"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Ians Vorschlag"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Der Grund, warum dies die beste Seide ist, weil es zur Zeit keine Bessere gibt. Ich versuche eine bessere Seide herzustellen.



Während Du das Spinnennetz besorgt hast, habe ich mir eine neue Theorie für bessere Seide ausgedacht . Ich bitte Dich noch einmal um einen Gefallen, wenn Du mir den 5 Schleimsäfte des Bösen Sumpfschleim bringst, gebe ich Dir eine gute Belohnung, die Dir sehr nützlich sein kann."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20283"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"232;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"43"
					}
				}
				<Branch0>
				{
					Desc	=	"Das ist interessant."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"5"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"198"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du wirst es nicht bedauern, das garantiere ich."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Ich brauche nichts."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Das würdest Du nicht sagen, wenn Du wüsstest was das für ein Gegenstand ist."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Ians Vorschlag "
			Desc2	=	"Danke für den Saft des Schleims. Ich beginne sofort mit den Nachforschungen. Hier Deine Belohnung."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20283"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"43"
					Exp	=	"10858"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169010"
					SItem1	=	"169011"
					SItem2	=	"169012"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"5"
					ItemIdx	=	"198"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
