<Root>
{
	<Quest>
	{
		Desc	=	"경비대장의 포고령을 집시촌 촌장에게 보고하라."
		GiveUp	=	"1"
		Id	=	"312"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"경비대장의 포고령"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"잔악한 비적단 무리의 소탕은 반드시 이루어져야 하네. 현재까지 보고된 바에 의하면 마왕의 부활과 관련된 움직임이 이디오스 곳곳에서 발생하고 있으며, 혼란한 정국을 틈타 지금까지 평화롭게 지내던 종족마저도 약탈과 노략질을 반복하고 있는 실정이네.
로그황궁으로부터 멀리 떨어져있는 거인의 영토까지는 황제의 지도력이 미치기도 힘들며, 원정대를 파견할 만한 병력도 사실 부족한 실정이네.. 그런 이유로 집시촌 촌장에게 전권을 일임하여, 로그황궁 경비대의 자금으로 집시 홉 고블린에 대한 현상포고문을 무기한 게제할테니, 이 사실을 집시촌 촌장 프리먼 듀에게 보고해주게. 여행경비가 필요한자, 현상금 사냥꾼, 일확천금을 꿈꾸는 모든 이들이 비적단 소탕에 동참할 것이네."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20304"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"55"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"311;"
					}
				}
				<Branch0>
				{
					Desc	=	"넵. 경비대장님. 즉시 시행하겠습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"즉시 떠나게나. 비적단의 횡포를 더이상 묵과할 수 만은 없네. 서두르게"
						}
						<YActNode1>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"243"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"마법통신병에게 전문을 보내시죠. 다시 거인의 땅까지...못가요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"어허.. 입으로 일을 처리할텐가. 
이런 중차대한 임무는 발로 직접 뛰어야 하는 법이거늘..."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"경비대장의 포고령"
			Desc2	=	"역시나..실질적인 병력지원은 없는 것인가.. 하지만, 무기한의 현상금 포고령은 효력이 있을듯 하구만.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20299"
				}
				<CondNode1>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"243"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"12462"
					Fame	=	"0"
					GP	=	"31"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"243"
				}
			}
		}
	}
}
