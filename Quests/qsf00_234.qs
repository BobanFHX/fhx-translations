<Root>
{
	<Quest>
	{
		Desc	=	"Töte 35 Echsenmensch-Krieger und 35 Echsenmensch-Anführer."
		GiveUp	=	"1"
		Id	=	"234"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Die letzte Aufgabe"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Ich habe alle Materialien gesammelt, die ich aus den stillen Sümpfen brauche, aber ich weiß nicht wie ich nach Hause komme. Es wurden immer mehr Echsenmenschen im Norden gesehen.



So komme ich nicht sicher nach Hause. Würdest Du bitte 35 Echsenmensch-Krieger und 35 Echsenmensch-Anführer töten, damit ich sicher heimkehren kann?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20283"
					}
					<CondNode1>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"233;"
					}
					<CondNode2>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"44"
					}
				}
				<Branch0>
				{
					Desc	=	"Das ist leicht."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Danke, vielen Dank... das Du meine Aufgabe annimmst..."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"35"
							KillCnt1	=	"35"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"1180"
							NPCIdx1	=	"1210"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Das ist zu schwer."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Verstehe... seufz, wie soll ich nur nach Hause kommen.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Die letzte Aufgabe"
			Desc2	=	"Jetzt kann ich sicher nach Hause reisen. Dank das Du die Aufgaben für mich erledigt hast."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20283"
				}
				<CondNode1>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"70"
					Exp	=	"11569"
					Fame	=	"0"
					GP	=	"4"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
