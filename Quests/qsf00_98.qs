<Root>
{
	<Quest>
	{
		Desc	=	"You must deliver remote talismans to James Madio at Black Bridge."
		GiveUp	=	"1"
		Id	=	"98"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Talisman Delivery"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"As for me, I can’t go anywhere with this because my shift isn’t over yet.  I was wondering 





 if you would go to James Madio (Guard Captain) who lives north of here across the Black Bridge.  You will find him there. Could you give this remote talisman to him?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20187"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"97;"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, I’ll do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"91"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Heh heh, if I see you at the tavern, I’ll buy you a drink."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I’ll take care of something else first and then I’ll come back."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hmm, when you finish it, come quickly to me."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Talisman Delivery"
			Desc2	=	"What? Somebody handled orcs? Hmm, this must be the remote talisman. 



 I don’t know who would waste their energy to make it vanish. Lately lots of weird things have been happening.  I think something is about to happen.  Maybe I am reading too much into normal small events - I am not really sure anymore."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"91"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"15"
					Exp	=	"220"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"91"
				}
			}
		}
	}
}
