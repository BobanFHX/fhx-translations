<Root>
{
	<Quest>
	{
		Desc	=	"크리스티 메이커에게 촌장 프리먼 듀의 편지를 전하시오."
		GiveUp	=	"1"
		Id	=	"305"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"기사도 정신"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"뭣이.. 크리스티에게서 물건을 가지러 오던 짐마차꾼 모리스가 습격당했다고? 허허.. 불과 얼마전 모리스의 형인 리커드가 비적단에게 희생되었는데.. 심슨가문에 불행이 또 다시 찾아오다니.. 이런..이런..

그나저나, 크리스티가 원군을 부탁했다는게 사실인가? 음.. 크리스티도 집시촌의 상황을 잘 알고 있을텐데..

그런데. 자네.. 들고있는 무기도 그렇고 차림새가 평범해 보이지는 않구만.. 어디.보자...

자네.. 실례가 되지 않는다면 대답 하나만 해주겠나?

기사도 정신이란 무엇이라 생각하나?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20299"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"51"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"304;"
					}
				}
				<Branch0>
				{
					Desc	=	"약한자를 보호하고, 악한자의 불의의 행동에 맞서 싸울수 있는 정신이라 생각합니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"역시, 내 침침하기만 한 늙은눈이 사람은 잘 알아보는것 같구만.."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"기사도 정신.. 그런건 전 모르겠는데요."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"으음.. 안타까운 일이야.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"기사도 정신"
			Desc2	=	"아직까지 이 세계는 구원받을 만한 가치가 있는것 같구만...."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"1"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"1"
				<CondNode0>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20299"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"20"
					Exp	=	"2442"
					Fame	=	"0"
					GP	=	"6"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
			}
		}
	}
}
