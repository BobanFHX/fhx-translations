<Root>
{
	<Quest>
	{
		Desc	=	"McGulials will need 9 Grey Wolf tails."
		GiveUp	=	"1"
		Id	=	"13"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Grey Wolf tails"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Oh no, I almost finished it... I still need more materials. Darn it. Hey, you! If you are on your way out, would you do me a favour? If you go northwest from here, you'll find Nymph Lake. In the middle of the lake, there's a strange land formation with wolves that have a special fur that shines like no other. 



 I use their tails to make the highest quality bags around. I can't use any other part but the tail. But beware, these wolves are quite dangerous. With my skill, I can't catch them anymore... So do you think you can get 9 tails for me? I'll reward you with one of my finest bags."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20204"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"3"
					}
				}
				<Branch0>
				{
					Desc	=	"I definitely want that bag. I'll do it."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"My bags are not only my biggest pride, but they're the highest quality around. Guaranteed."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"9"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"12"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I won't be heading out anytime soon."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Don't I look desperate enough?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Grey Wolf Tails"
			Desc2	=	"Wow, you brought the highest quality tails. Apparently you have good eyes for quality. Take this, you will like it."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20204"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"35"
					Exp	=	"195"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"169000"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"9"
					ItemIdx	=	"12"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
