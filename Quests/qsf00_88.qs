<Root>
{
	<Quest>
	{
		Desc	=	"Collect 4 small Fog Scorpion’s tails."
		GiveUp	=	"1"
		Id	=	"88"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Scorpion’s Tail"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hello? I wish that Arketlav’s blessings will be with you. Truth is I was passing through here and I made a mistake.  I got bitten by a  Fog Scorpion.  I wasn’t paying attention and I got into this mess. 





 I hate to bother you, but my body is tingling all over.  I need to get antidote, and to make that I need small scorpion’s tails. 





 Fortunately there are plenty of Small Fog Scorpions around.  Would you get me some of those tails? Just think of it as saving a life."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20233"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"13"
					}
				}
				<Branch0>
				{
					Desc	=	"Wait here, I'll be right back."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"4"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"79"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I pray for your safety."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I think I need to go, I have a long journey ahead."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Mmm I can't hold on."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Scorpion's Tail"
			Desc2	=	"Oh you came just in time. If you were a little longer the poison would have killed me for sure. 



 You came just in time to save my life."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20233"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"59"
					Exp	=	"1085"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"4"
					ItemIdx	=	"79"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
