<Root>

{

	<Quest>

	{

		Desc	=	"Töte 20 Wilde Trollkrieger "

		GiveUp	=	"1"

		Id	=	"253"

		NumOfQUNode	=	"2"

		Repeat	=	"0"

		Title	=	"Kopfgeldjagd (2)"

		<QUNode0>

		{

			ChildNodeType	=	"2"

			Desc	=	"Gefällt es Dir ein Kopfgeldjäger zu sein? Wie wäre es mit einer Jagd auf Wilde Trollkrieger? Die greifen auch Hadun an, zusammen mit den Streunenden Trolls verursachen sie mir Kopfschmerzen.

			Wenn Du 20 von ihnen tötest, bekommst du wieder eine Belohnung."

			Id	=	"0"

			PreQUId	=	"255"

			<BranchNode>

			{

				NumOfBranch	=	"2"

				<MainTrigger>

				{

					NextNoQUId	=	"255"

					NextYesQUId	=	"255"

					NumOfCondNode	=	"3"

					NumOfNActNode	=	"0"

					NumOfYActNode	=	"0"

					<CondNode0>

					{

						CondNodeType	=	"10000"

						NPCIdx	=	"20287"

					}

					<CondNode1>

					{

						CondNodeType	=	"20006"

						ORQuestIDs	=	"252;"

					}

					<CondNode2>

					{

						CondNodeType	=	"20000"

						MaxVal	=	"99"

						MinVal	=	"53"

					}

				}

				<Branch0>

				{

					Desc	=	"Ich werde es versuchen."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"1"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"2"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Sei vorsichtig, sie sind noch stärker."

						}

						<YActNode1>

						{

							ActionNodeType	=	"10000"

							KillCnt0	=	"20"

							KillCnt1	=	"0"

							KillCnt2	=	"0"

							KillCnt3	=	"0"

							NPCIdx0	=	"1375"

							NPCIdx1	=	"-1"

							NPCIdx2	=	"-1"

							NPCIdx3	=	"-1"

							SvrEventType	=	"0"

						}

					}

				}

				<Branch1>

				{

					Desc	=	"Das ist zu schwer."

					<Trigger>

					{

						NextNoQUId	=	"255"

						NextYesQUId	=	"255"

						NumOfCondNode	=	"0"

						NumOfNActNode	=	"0"

						NumOfYActNode	=	"1"

						<YActNode0>

						{

							ActionNodeType	=	"20000"

							Desc	=	"Ist vielleicht auch besser sich mit Gegnern anzulegen die man schafft."

						}

					}

				}

			}

		}

		<QUNode1>

		{

			ChildNodeType	=	"4"

			Desc	=	"Kopfgeldjagd (2)"

			Desc2	=	"Oh ... genau. Super das waren die Wilden Trollkrieger! Du bist wirklich sehr gut."

			Id	=	"1"

			PreQUId	=	"0"

			<RewardNode>

			{

				NextNoQUId	=	"255"

				NextYesQUId	=	"255"

				NumOfCondNode	=	"2"

				NumOfNActNode	=	"0"

				NumOfYActNode	=	"2"

				<CondNode0>

				{

					CondNodeType	=	"10000"

					NPCIdx	=	"20287"

				}

				<CondNode1>

				{

					CondNodeType	=	"1"

					SvrEventType	=	"0"

				}

				<YActNode0>

				{

					ActionNodeType	=	"10001"

					CP	=	"54"

					Exp	=	"20809"

					Fame	=	"0"

					GP	=	"2"

					NItem0	=	"-1"

					NItem1	=	"-1"

					NItem2	=	"-1"

					NItem3	=	"-1"

					PP	=	"0"

					SItem0	=	"-1"

					SItem1	=	"-1"

					SItem2	=	"-1"

					SItem3	=	"-1"

				}

				<YActNode1>

				{

					ActionNodeType	=	"2"

					Reserved	=	"255"

				}

			}

		}

	}

}

