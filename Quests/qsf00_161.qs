<Root>
{
	<Quest>
	{
		Desc	=	"Go to the Miner town of Dust Canyon and request for a militiaman."
		GiveUp	=	"1"
		Id	=	"161"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Request for help."
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"First Dust Gorge is wide, but the mining towns are a bit small.  That is why even Rog is not willing to send any help.  They have alot of wealth from all the minerals we are sending them.  There's a saying that goes, those who want water will look for a well.  Our militant army is made up different volunteers from different tribes.  This militant army is easy to assemble and easy to maintain, and they can be at places in a moments notice. 





 If you are going near the mining towns, can you bring this letter to one of my relatives?  He is my nephew and he is the security captain in one of the mining towns.  I am thinking that if he gets this letter he will assemble a militant group and come and help me."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"24"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20250"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"160;"
					}
				}
				<Branch0>
				{
					Desc	=	"You mean the mining towns?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"138"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"That's right the mining towns."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I am not headed that way."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"You need to briefly stop by it, can you do it?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Request for a help"
			Desc2	=	"Hmmm, I didn't know Mario had that happen to him.  I guess I need to quickly assemble the militant army and send them.  Hmm, a monster that looks like a wolf, but walks like a man.  I never heard anything like that before.  Heh heh heh... 



 however I am worried because we do not know the enemy."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"138"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20198"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"767"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"138"
				}
			}
		}
	}
}
