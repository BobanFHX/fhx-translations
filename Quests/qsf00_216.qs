<Root>
{
	<Quest>
	{
		Desc	=	"Töte die Kleinen Walzenskorpione und sammele deren Schale!"
		GiveUp	=	"1"
		Id	=	"216"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Der Skorpion"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Die Andora Gegend hat gute Tiere zum ausstopfen. Eines Tages reiste ich in den nördlichen Wald und dort sah ich kleine Walzenskorpion. Diese lassen sich gut ausstopfen.



Wenn Du Arbeit brauchst, bringe mir bitte eine Schale. Ich verspreche Dir, dass Du es nicht bereuen wirst. Ich zahle gutes Geld für gute Ware."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20264"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"35"
					}
				}
				<Branch0>
				{
					Desc	=	"Das hört sich gut an."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"183"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Das ist es! Lange Reisen kosten viel Geld!"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Nee keine Lust."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Du brauchst kein Geld mehr?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Der Skorpion"
			Desc2	=	"Oh! Eine schöne Schale; das ist mehr als ich erwartet habe. Hahaha."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20264"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"69"
					Exp	=	"5373"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"183"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
