<Root>
{
	<Quest>
	{
		Desc	=	"Trevor Mount is asking you to kill 7 black bear."
		GiveUp	=	"1"
		Id	=	"41"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Brutal Bear"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I don't understand it, why aren't the fortress doing anything about those Brutal Bears?  Huh? Who are you? I don't know who you are, but it would be a good idea not to go any further.  Those black bears are making journey diffiult for any passer by. Three of my companions have already been injured by them. 





 If you don't want to get hurt, I suggest you stay right here.  What? From looks of your weapons you aren't an ordinary person.  Still...hmmm...if you're willing to get rid of those bears, then I will give you some money for your troubles.  Can you kill 7 black bears so that we can pass through?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"8"
					}
					<CondNode1>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20219"
					}
				}
				<Branch0>
				{
					Desc	=	"Sure, just wait here"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"7"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"160"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I know I can trust you"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I really don't want to"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Man...how much longer do I have to wait here until I can pass through?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Brutal bear"
			Desc2	=	"Wow, you killed them so quickly. Your skill is unmatched.  I wish I could use you as my body guard. Ha ha!"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20219"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"47"
					Exp	=	"548"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
