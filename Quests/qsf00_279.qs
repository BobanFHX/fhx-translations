<Root>
{
	<Quest>
	{
		Desc	=	"생명호수 식인 악어 20마리를 잡아 원혼의 한을 달래주시오."
		GiveUp	=	"1"
		Id	=	"279"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"원혼구혼"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"흑흑.. 거기 지나가시는 분 잠시만... 

몇일전 저의 정인이 저기 다리건너 악어들에게 당하셨어요. 그이와 전 이제 곧 결혼할 사이였는데, 강물에 떠내려간 저의 모자를 주워주시러 가셨다가 악어들에게.. 흑흑

그이의 원혼이 몇일동안 꿈 속에서 나타나시는걸 보면 제가 악어를 사냥해서 한을 달래드리고 싶지만, 연약한 여자의 몸으로 도저히 악어들을 어떻게 할 수가 없네요. 제발 저의 부탁을 거절하지 말아주세요..

생명호수 식인 악어 20마리 정도면 그이도 편히 하늘나라로 가실 수 있을것 같아요."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20289"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"29"
					}
				}
				<Branch0>
				{
					Desc	=	"한갓 미물이 감히.. 잠시만 기다리세요. 제가 처리해 드리죠."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"20"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"875"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"정말 고맙습니다. 그리고 악어를 죽이고나서 자세히 살펴주세요."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"식..식인악어.. 아! 전 급히 바쁜일이 있어서.."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"네.. 어쩔수 없죠. 여기서 저 식인악어들을 그저 이를 갈며 지켜보고만 있어야 겠군요. 흑흑"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"원혼구혼"
			Desc2	=	"정말 감사합니다. 그런데.."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20289"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"4636"
					Fame	=	"0"
					GP	=	"55"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
