<Root>
{
	<Quest>
	{
		Desc	=	"비늘리자드맨의 껍질 10개, 다크슬라임의 진액 10개를 모아오시오."
		GiveUp	=	"1"
		Id	=	"293"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"습지 생태파악"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"저는 왕립도서관에서 파견된 탐험대원입니다. 우리 왕립도서관은 요근래 고요의늪에서 녹색연기가 피어오르고, 서식하는 생물들이 난폭해 지고 있다는 보고를 받았습니다. 관장님께서는 보다 자세한 생태보고서를 원하시지만, 탐험대를 파견할 만한 여력이 없어서, 저 혼자 이렇게 조사를 하고있는 상황입니다. 그러나, 혼자서는 손이 열개라도 힘들군요. 일단, 조사한 바에 의하면.. 비늘리자드맨의 껍질이 변색이 되어 황녹색으로 보이더군요. 그리고, 다크슬라임은 어딘지 모르게 움직임이 둔탁한것이..딱딱해 진것 같아요. 

용사님께서 비늘리자드맨의 껍질 10개와 다크슬라임의 진액 10개를 구해주세요. 약간의 단서만 구할 수 있다면 간단한 실험으로 녀석들의 생태를 파악 할 수 있습니다. 수고비는 제대로 드리겠습니다."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20296"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"38"
					}
				}
				<Branch0>
				{
					Desc	=	"요즘 여행경비가 바닥이었는데..좋습니다."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"7"
							ItemCnt1	=	"4"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"223"
							ItemIdx1	=	"224"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"그럼. 부탁드리겠습니다."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"팔마스 공작님의 살해와 관련해서 2개의 기사단이 조사중인것으로 알고있는데. 그쪽에 맡기시죠?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"갈길이 바빠… 그럼 이만.."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"습지 생태파악"
			Desc2	=	"고맙습니다. 수고비는 충분히 챙겨드릴께요. 이래보여도 왕실도서관은 급료가 좋다구요."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"4"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20296"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"30"
					Exp	=	"4605"
					Fame	=	"0"
					GP	=	"20"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"223"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"10"
					ItemIdx	=	"224"
				}
				<YActNode3>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
