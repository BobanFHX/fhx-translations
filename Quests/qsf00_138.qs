<Root>
{
	<Quest>
	{
		Desc	=	"You must steal summoner's spell from sealed dark magician."
		GiveUp	=	"1"
		Id	=	"138"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Sealing of dark magician"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I saw a document once, I think dark forces will be revived.  Dark forces for 1000 years were always looking to take Idios. 





 I don't know if you ever heard, but 1000 years ago Beljev and his evil army came and tried to overtake us. Purikan's army and Arketlav and Daijaru gods who came to his aids to drove them back. 





 After the battle, Beljev was driven back, but they continued to take over. They were spoiled every time. Cain was the last grand master. After 50 years, I think things are stirring up again. According to the sealed document, A dark magician was going to move a summoning spell to the Giant territory. 





 I think right now the magician may be moving from deadman's holy land to north of Calm forest.  I think he might be moving very quietly. Whether summoning or if summoned at the hero's land, it can be either strong grand master ever worse, it can be Beljev. 





 Go and gather your friends and capture him and get rid of him.  Take summoner spell, so that we do not have to suffer tragedy 1000 years ago."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"4"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20188"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"20"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"137;"
					}
					<CondNode3>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"I understand, I'll go right away."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Then...I guess you have lot of work to do."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"0"
							ItemCnt2	=	"0"
							ItemCnt3	=	"0"
							ItemIdx0	=	"120"
							ItemIdx1	=	"-1"
							ItemIdx2	=	"-1"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"Can I do it later?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"This is a golden opportunity."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Sealed dark magician"
			Desc2	=	"Is this the summer spell? This must have been a tough task...you are in a bad shape...ha ha. Since we took this away I must now go and report this to Rog right away."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"3"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20188"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"91"
					Exp	=	"2208"
					Fame	=	"0"
					GP	=	"1"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"165003"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"120"
				}
				<YActNode2>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
