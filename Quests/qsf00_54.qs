<Root>
{
	<Quest>
	{
		Desc	=	"You must report to Kevin Dillon about your assessment."
		GiveUp	=	"1"
		Id	=	"54"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Report to Kevin Dillon"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hmm...I think I need to be responsible for you until the end. Gorge Orcs are causing so much trouble that my commander personally had to step in. 





   man...am I that unworthy? I am 10 year veteran in Rog's Imperial Army.  Here, this is your assesement report, show this to my commander and ask for another assignment."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20182"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"8"
					}
					<CondNode2>
					{
						CondNodeType	=	"20006"
						ORQuestIDs	=	"53;"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes, I understand"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10002"
							ItemCnt	=	"1"
							ItemIdx	=	"48"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Now go. If you're late he will get mad."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I think I need to quit."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Why all of a sudden?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Report to Kevin Dillon"
			Desc2	=	"What? You are new recruit? Oh, maybe that's not the case. You did a great job."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"20005"
					ItemCnt	=	"1"
					ItemIdx	=	"48"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20196"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"0"
					Exp	=	"96"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"48"
				}
			}
		}
	}
}
