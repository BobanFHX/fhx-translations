<Root>
{
	<Quest>
	{
		Desc	=	"Capture 20 Vicious Sunrise Shaikins."
		GiveUp	=	"1"
		Id	=	"182"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"[Group] Shaikin's Pain"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"I am Erois and I am a spiritualist.  I know its a bit of an odd job. I can't control spirits, I can however communicate with them. 





 Lately I have been suffering from nightmares.  The nightmare is always the same, it starts out at Sunrise Lake and I am lost there.  I don't know why I have these nightmares.  I need to find out the reason for the nightmares. 





  For some unknown reason when the Shaikin came to Sunrise Lake they were suffering and they became very aggressive. They wanted someone to kill them.  They didn't want to suffer anymore.  I beg you, can you fulfill their wish by killing 20 vicious Shaikins?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"3"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20260"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"23"
					}
					<CondNode2>
					{
						CondNodeType	=	"20004"
						Group	=	"1"
					}
				}
				<Branch0>
				{
					Desc	=	"Yes of course"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"20"
							KillCnt1	=	"0"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"645"
							NPCIdx1	=	"-1"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Sniff, sniff, sniff' poor spirits."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"I don't believe you."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Why do you doubt me?"
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Shaikin's Pain"
			Desc2	=	"Ah~ I think their pain is now slowly going away.  Thank you very much, now I can peacefully sleep"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20260"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"11"
					Exp	=	"3692"
					Fame	=	"0"
					GP	=	"2"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"170008"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
