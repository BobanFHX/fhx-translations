<Root>
{
	<Quest>
	{
		Desc	=	"You must capture Small Zombie Dogs and Deformed Zombies to get 3 grave robber¡¯s tools."
		GiveUp	=	"1"
		Id	=	"96"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Grave Robber¡¯s Tools"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"Hey you - hey you¡¦shish quiet can you talk to me quietly?  You must be quiet! Over there I see something walking around recklessly.  I have been grave robbing for over 20 years, but I never really seen them before.  Have you heard of zombies? 





 See them?  They are zombies.  Ive heard others tell stories about zombies for 20 years, but I never thought Id see the day that I would witness a real one.  There are so many of them moving about.  This must be end of the world! What? What is that sly look?  Yea I am a thief - I rob the dead. Never seen a robber before? Do you think I want to live like this? Hah lets not talk about this anymore. While I was catching some sleep by a grave, I heard a noise.  At first I thought it was guard patrols.  When I hid, I saw a guy who was saying something in front of me. 





 Then all of the sudden they came out of the ground!  Man I was scared. (shivering) Oooh... my whole body is having the shivers.  Well, thats not the issue, but the problem is that I left my tools there. I need those tools to make a living. I am so used to those tools that, it will take me a while to break-in new ones. 





 Can you find those tools for me? That Zombie Dog has my bags around his neck, and those Deformed Zombies have my shovel and hammer. What? You cant help because I am a robber? Well, I was going to quit this job anyway.  Do you even understand my situation? Have you ever gone hungry for three days because you didnt even have a piece of bread? How can you judge me when you never even felt that before?"
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20235"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"14"
					}
				}
				<Branch0>
				{
					Desc	=	"Well, I¡¯ll help you because of your current situation."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"10000"
							ItemCnt0	=	"1"
							ItemCnt1	=	"1"
							ItemCnt2	=	"1"
							ItemCnt3	=	"0"
							ItemIdx0	=	"87"
							ItemIdx1	=	"88"
							ItemIdx2	=	"89"
							ItemIdx3	=	"-1"
							SvrEventType	=	"1"
						}
						<YActNode1>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Hah I don¡¯t care if you just leave."
						}
					}
				}
				<Branch1>
				{
					Desc	=	"How dare a thief ask me for a favour?"
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"I see that warriors are stubborn."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Grave Robber¡¯s Tools"
			Desc2	=	"Hee hee hee you found them all.  Let me see shovel, hammer hee hee hee its all here.  Thank you very much. 





 Problem is how can I get out when those things are walking around?"
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"5"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"1"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20235"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"61"
					Exp	=	"1217"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"87"
				}
				<YActNode2>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"88"
				}
				<YActNode3>
				{
					ActionNodeType	=	"10003"
					ItemCnt	=	"1"
					ItemIdx	=	"89"
				}
				<YActNode4>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
