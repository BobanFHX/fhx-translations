<Root>
{
	<Quest>
	{
		Desc	=	"You must capture 5 brown bear from Windy canyon and 5 black bears from Windy canyon."
		GiveUp	=	"1"
		Id	=	"143"
		NumOfQUNode	=	"2"
		Repeat	=	"0"
		Title	=	"Canyon Bear Hunt"
		<QUNode0>
		{
			ChildNodeType	=	"2"
			Desc	=	"For the most part bears in this town are gentle.  They were much different from bears from different regions.  These guys didn't eat too much meat, but instead they ate lot of honey and fish.  For some strange reason, all the bees left and catching fish became much more difficult.  Bears became much more aggressive now that they lost their food supplies. 





   They started out attacking few people here and there, but finally they attacked and killed Hugesons, the Midland's count's gardener.  The count was furious after that. 





   The count sent out official dispatch to kill all the bears in the summer windy canyon.  Anyone who capture 5 of each brown and black bears from the windy canyon, there will be a reward."
			Id	=	"0"
			PreQUId	=	"255"
			<BranchNode>
			{
				NumOfBranch	=	"2"
				<MainTrigger>
				{
					NextNoQUId	=	"255"
					NextYesQUId	=	"255"
					NumOfCondNode	=	"2"
					NumOfNActNode	=	"0"
					NumOfYActNode	=	"0"
					<CondNode0>
					{
						CondNodeType	=	"10000"
						NPCIdx	=	"20243"
					}
					<CondNode1>
					{
						CondNodeType	=	"20000"
						MaxVal	=	"99"
						MinVal	=	"21"
					}
				}
				<Branch0>
				{
					Desc	=	"Hmmm...really?  That's not bad."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"1"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"2"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Really?  If it is ok go ahead and try."
						}
						<YActNode1>
						{
							ActionNodeType	=	"10000"
							KillCnt0	=	"5"
							KillCnt1	=	"5"
							KillCnt2	=	"0"
							KillCnt3	=	"0"
							NPCIdx0	=	"570"
							NPCIdx1	=	"575"
							NPCIdx2	=	"-1"
							NPCIdx3	=	"-1"
							SvrEventType	=	"0"
						}
					}
				}
				<Branch1>
				{
					Desc	=	"What's the reward...hmmm."
					<Trigger>
					{
						NextNoQUId	=	"255"
						NextYesQUId	=	"255"
						NumOfCondNode	=	"0"
						NumOfNActNode	=	"0"
						NumOfYActNode	=	"1"
						<YActNode0>
						{
							ActionNodeType	=	"20000"
							Desc	=	"Midland's count is very generous."
						}
					}
				}
			}
		}
		<QUNode1>
		{
			ChildNodeType	=	"4"
			Desc	=	"Uprising of bears from the canyon."
			Desc2	=	"I have heard rumors about you.  I heard you are one tough solider? Well done, you must kill all of them, not one must survive..."
			Id	=	"1"
			PreQUId	=	"0"
			<RewardNode>
			{
				NextNoQUId	=	"255"
				NextYesQUId	=	"255"
				NumOfCondNode	=	"2"
				NumOfNActNode	=	"0"
				NumOfYActNode	=	"2"
				<CondNode0>
				{
					CondNodeType	=	"1"
					SvrEventType	=	"0"
				}
				<CondNode1>
				{
					CondNodeType	=	"10000"
					NPCIdx	=	"20243"
				}
				<YActNode0>
				{
					ActionNodeType	=	"10001"
					CP	=	"79"
					Exp	=	"2408"
					Fame	=	"0"
					GP	=	"0"
					NItem0	=	"-1"
					NItem1	=	"-1"
					NItem2	=	"-1"
					NItem3	=	"-1"
					PP	=	"0"
					SItem0	=	"-1"
					SItem1	=	"-1"
					SItem2	=	"-1"
					SItem3	=	"-1"
				}
				<YActNode1>
				{
					ActionNodeType	=	"2"
					Reserved	=	"255"
				}
			}
		}
	}
}
