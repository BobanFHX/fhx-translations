# Purpose
In this repository we collect files containing text that form part of the FHX client. This will enable new translations of text in Korean and German files, as well as improvements on existing English translations.

# Structure
Most, if not all, files should be able to be opened in notepad. More details are sometimes provided by READMEs in the subdirectories.

## Quests
These are QuestScript files which should be opened by notepad or a similar text editor. There is one file per quest.
